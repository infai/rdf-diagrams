# Triples

![image](./exports/images/triples-blank.svg)

## Valid `label`

- `a`
- [IRI](./iri.md)
- [Blank Node](blank-node.md)

## Valid `containees`

- [Property](./property.md)
- [Properties](./properties.md)

## Example

{{#template templates/example.md file=triples}}
