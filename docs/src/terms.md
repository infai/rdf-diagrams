# Terms

![image](./exports/images/terms.svg)

## Valid `containees` when subject of a link

- [IRI](./iri.md)
- [Blank Node](blank-node.md)
- [Terms](./terms.md)
- [Triples](./triples.md)
- [Collection](./collection.md)

### Example

{{#template templates/example.md file=terms-subject}}

## Valid `containees` when object of a link

- [IRI](./iri.md)
- [Blank Node](blank-node.md)
- [Terms](./terms.md)
- [Triples](./triples.md)
- [Collection](./collection.md)
- [Literal](./literal.md)

### Example

{{#template templates/example.md file=terms-object}}
