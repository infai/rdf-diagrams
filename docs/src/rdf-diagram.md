# RDF-Diagram

The RDF-Diagram Elements are

Nodes:

- [IRI](./iri.md)
- [Blank Node](blank-node.md)
- [Literal](./literal.md)
- [Property](./property.md)
- [Turtle](./turtle.md)

Containers:

- [Terms](./terms.md)
- [Triples](./triples.md)
- [Properties](./properties.md)
- [Collection](./collection.md)

Edge:

- [Link](./link.md)
