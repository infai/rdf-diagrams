# Introduction

The aim of the RDF-Diagram-Framework is to provide reusable building blocks for a compact, human-readable, standardized visual representation of [RDF graphs](https://www.w3.org/TR/rdf11-primer/).

To achieve this, the RDF-Diagram-Framework defines a [model for diagrams](./diagram.md), which serves as the syntax for RDF-Diagrams. Following this, it delineates [semantics](./rdf-diagram.md) grounded in this syntax, bridging the gap to the RDF data model. All concepts of [RDF 1.1](https://www.w3.org/TR/rdf11-concepts/) can be expressed with an RDF-Diagram, except for RDF Datasets.

The Syntax for the labels is equivalent to Turtle. Read the [Turtle specifcation](https://www.w3.org/TR/turtle/) for a deeper understanding.

## Example

This image is taken from the RDF-Primer:

![image](./assets/example-graph-iris.png)

Here is the equivalent RDF-Diagram:

![image](./exports/images/introduction-mona-lisa.svg)

This translates to RDF serialized as Turtle:

```turtle
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX schema: <http://schema.org/>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX dbp: <http://dbpedia.org/resource/>

<http://example.org/bob#me> a foaf:Person;
  foaf:topic_interest wd:Q12418;
  schema:birthDate "1990-07-04"^^xsd:date;
  foaf:knows <http://example.org/alice#me>.

<http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619>
  dcterms:subject wd:Q12418.

wd:Q12418
  dcterms:creator dbp:Leonardo_da_Vinci;
  dcterms:title "Mona Lisa".
```
