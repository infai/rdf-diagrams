# IRIs

According to the [turtle specification](https://www.w3.org/TR/turtle/#sec-iri) IRIs may be written as relative or absolute IRIs or prefixed names.

## Absolute IRI

{{#template templates/example.md file=iri-absolute}}

## Relative IRI

### Turtle Syntax

{{#template templates/example.md file=iri-relative-base}}

### SPARQL Syntax

{{#template templates/example.md file=iri-relative-base-sparql}}

## Prefixed Name

### Turtle Syntax

{{#template templates/example.md file=iri-prefix}}

### SPARQL Syntax

{{#template templates/example.md file=iri-prefix-sparql}}
