# Collection

![image](./exports/images/collection.svg)

## Valid `label`

- [Blank Node](blank-node.md)

## Valid `containees`

- [IRI](./iri.md)
- [Blank Node](blank-node.md)
- [Literal](./literal.md)
- [Terms](./terms.md)
- [Triples](./triples.md)
- [Collection](./collection.md)

### Example

{{#template templates/example.md file=collection-example}}

This translates to:

{{#template templates/example.md file=collection-example-expanded}}
