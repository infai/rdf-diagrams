# Properties

![image](./exports/images/properties-blank.svg)

## Valid `label`

- `a`
- [IRI](./iri.md)

## Valid `containee`

- [IRI](./iri.md)
- [Blank Node](blank-node.md)
- [Terms](./terms.md)
- [Literal](./literal.md)
- [Triples](./triples.md)
- [Collection](./collection.md)

## Example

{{#template templates/example.md file=properties}}
