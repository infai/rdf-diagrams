# Link

{{#template templates/example.md file=link}}

## Valid `subjects`

- [IRI](./iri.md)
- [Blank Node](blank-node.md)
- [Terms](./terms.md)
- [Triples](./triples.md)

## Valid `objects`

- [Allowed subjects](./link.md#allowed-subjects)
- [Literal](./literal.md)
