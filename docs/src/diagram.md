# Syntax

## Elements

![image](./exports/images/elements.svg)

## Rules

1. A container can contain `node`, `named container` and `anon container`.
2. A `edge` can link `node`, `named container` and `anon container`.

## Layout

![image](./exports/images/layout.svg)
