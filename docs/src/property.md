# Property

![image](./exports/images/property.svg)

## Valid `predicate`

- `a`
- [IRI](./iri.md)

## Valid `object`

- [IRI](./iri.md)
- [Blank Node](blank-node.md)
- [Literal](./literal.md)
