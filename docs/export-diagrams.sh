#!/bin/bash

diagrams_dir=./diagrams
export_dir=./src/exports

rm -rf "$export_dir"
mkdir -p "$export_dir"/{turtle,ntriple,images}

printf '\nExporting diagrams to RDF:\n\n'
for file in "$diagrams_dir"/*.drawio; do
    printf 'Converting %s to Turtle\n' "$file"
    rdf-diagram-cli -i "$file" -o "$export_dir"/turtle/"$(basename "$file" .drawio)".ttl
    printf 'Exporting %s to NTriples\n' "$file"
    rdf-diagram-cli -i "$file" -o "$export_dir"/ntriple/"$(basename "$file" .drawio)".nt
done

printf '\nExporting diagrams to SVG:\n\n'
drawio -x "$diagrams_dir" -f svg -o "$export_dir"/images

printf '\nSorting NTriple files...'
for file in "$export_dir"/ntriple/*; do
    sort "$file" -o "$file"
done
printf ' done.\n'
