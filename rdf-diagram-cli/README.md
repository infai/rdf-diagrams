Command-line interface for converting between RDF-Diagrams, drawio diagrams and RDF serializations.

A component of the [RDF-Diagram-Framework](https://gitlab.com/infai/rdf-diagram-framework).

## Install

```sh
git clone git@gitlab.com:infai/rdf-diagram-framework.git
cd rdf-diagram-framework
cargo install --path=rdf-diagram-cli
```

## Usage

Check the [manpage](https://gitlab.com/infai/rdf-diagram-framework/-/blob/main/rdf-diagram-cli/man/rdf-diagram-cli.md?ref_type=heads) or

```sh
rdf-diagram-cli -h
```
