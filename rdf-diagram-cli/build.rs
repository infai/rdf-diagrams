include!("src/cli.rs");

use anyhow::Error;
use clap::CommandFactory;
use clap_mangen::Man;

/// Create manpage
fn main() -> Result<(), Error> {
    let path = std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("man");
    let binary_name = "rdf-diagram-cli";
    let mut cmd = Cli::command();
    cmd.set_bin_name(binary_name);
    let man = Man::new(cmd);
    let mut contents: Vec<u8> = Default::default();
    man.render(&mut contents)?;
    std::fs::create_dir_all(&path)?;
    let man_file_path = path.join(format!("{}.1", binary_name));
    std::fs::write(man_file_path, &contents)?;
    Ok(())
}
