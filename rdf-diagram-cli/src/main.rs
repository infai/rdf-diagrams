use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};

use anyhow::Error;
use clap::Parser;

mod cli;

use cli::{Cli, Serialization};
use rdf_diagram_lib::formats::drawio::DrawioDiagram;
use rdf_diagram_lib::formats::rdf_diagram::RdfConfig;
use rdf_diagram_lib::formats::serialize_params::Mode;
use rdf_diagram_lib::formats::Deserialize;
use rdf_diagram_lib::Diagram;

fn main() -> Result<(), Error> {
    let cli = <Cli as Parser>::parse();

    let mut reader: Box<dyn BufRead> = match cli.input_drawio_file {
        Some(path) => {
            let file = File::open(path)?;
            Box::new(BufReader::new(file))
        }
        None => Box::new(BufReader::new(std::io::stdin())),
    };

    let diagram = Diagram::from_reader::<DrawioDiagram, _>(&mut reader)?;

    let mut write: Box<dyn Write> = match &cli.output.output {
        Some(path) => {
            let file = File::create(path)?;
            Box::new(BufWriter::new(file))
        }
        None => Box::new(BufWriter::new(std::io::stdout())),
    };

    let extension = cli
        .output
        .output
        .and_then(|path| path.extension()?.to_str().map(String::from));

    match (cli.output.serialization, extension.as_deref()) {
        (Some(Serialization::Turtle), _) => {
            let config = RdfConfig::Turtle {
                mode: Mode::Stream,
                indentation: 2,
            };
            diagram.serialize_with_config(&config, &mut write)?
        }
        (Some(Serialization::TurtlePretty), _) | (None, Some("ttl")) => {
            let config = RdfConfig::Turtle {
                mode: Mode::Pretty,
                indentation: 2,
            };
            diagram.serialize_with_config(&config, &mut write)?
        }
        (Some(Serialization::NTriple), _) | (None, Some("nt")) => {
            let config = RdfConfig::NTriple;
            diagram.serialize_with_config(&config, &mut write)?
        }
        (Some(Serialization::Drawio), _)
        | (None, Some("drawio"))
        | (None, Some("xml")) => {
            let mx_file = DrawioDiagram::try_from(&diagram)?;
            write!(write, "{}", mx_file)?;
        }
        (None, None) => {
            return Err(anyhow::Error::msg(
                "If no path with a valid extension is given, the \
                 serialization has to be specified",
            ));
        }
        (None, Some(extension)) => {
            return Err(anyhow::Error::msg(format!(
                "Unsupported file extension '{}'.",
                extension
            )));
        }
    };

    Ok(())
}
