use std::path::PathBuf;

use clap::builder::styling::{AnsiColor, Effects};
use clap::builder::Styles;
use clap::{Args, Parser, ValueEnum};

fn my_styles() -> Styles {
    Styles::styled()
        .header(AnsiColor::Green.on_default() | Effects::BOLD)
        .usage(AnsiColor::Green.on_default() | Effects::BOLD)
        .literal(AnsiColor::Cyan.on_default() | Effects::BOLD)
        .placeholder(AnsiColor::Cyan.on_default())
}

#[derive(Parser)]
#[command(author, version, about, long_about = None, styles(my_styles()))]
pub(crate) struct Cli {
    /// Path to a draw.io file; default to stdio if not provided
    #[arg(short, long, value_name = "PATH")]
    pub input_drawio_file: Option<PathBuf>,
    #[clap(flatten)]
    pub output: Output,
}

#[derive(Args)]
#[group(required = true)]
pub(crate) struct Output {
    /// Path for the output file; defaults to stdout if not provided.
    #[arg(short, long, value_name = "PATH")]
    pub output: Option<PathBuf>,
    /// Output serialization format; inferred from file extension
    /// if output path is given.
    #[arg(short, long, value_enum, verbatim_doc_comment)]
    pub serialization: Option<Serialization>,
}

#[derive(Clone, ValueEnum)]
pub(crate) enum Serialization {
    Turtle,
    TurtlePretty,
    NTriple,
    Drawio,
}
