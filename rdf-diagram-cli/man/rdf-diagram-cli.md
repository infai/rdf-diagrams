```man
rdf‐diagram‐cli(1)                      General Commands Manual                      rdf‐diagram‐cli(1)

NAME
       rdf-diagram-cli

SYNOPSIS
       rdf-diagram-cli    [-i|--input-drawio-file]   [-o|--output]   [-s|--serialization]   [-h|--help]
       [-V|--version]

DESCRIPTION
OPTIONS
       -i, --input-drawio-file=PATH
              Path to a draw.io file; default to stdio if not provided

       -o, --output=PATH
              Path for the output file; defaults to stdout if not provided

       -s, --serialization=SERIALIZATION
              Output serialization format; inferred from file extension if output path is given.

              [possible values: turtle, turtle-pretty, n-triple, drawio]

       -h, --help
              Print help

       -V, --version
              Print version

VERSION
       v0.1.0

                                         rdf‐diagram‐cli 0.1.0                       rdf‐diagram‐cli(1)
```
