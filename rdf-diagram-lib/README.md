<!-- cargo-sync-rdme title [[ -->
# rdf-diagram-lib
<!-- cargo-sync-rdme ]] -->
<!-- cargo-sync-rdme badge [[ -->
[![crates.io](https://img.shields.io/crates/v/rdf-diagram-lib.svg?logo=rust&style=flat-square)](https://crates.io/crates/rdf-diagram-lib)
[![docs.rs](https://img.shields.io/docsrs/rdf-diagram-lib.svg?logo=docs.rs&style=flat-square)](https://docs.rs/rdf-diagram-lib)
[![Rust: ^1.80](https://img.shields.io/badge/rust-^1.80-93450a.svg?logo=rust&style=flat-square)](https://doc.rust-lang.org/cargo/reference/manifest.html#the-rust-version-field)
<!-- cargo-sync-rdme ]] -->
<!-- cargo-sync-rdme rustdoc [[ -->
A Rust library for parsing DrawIO diagrams following the [RDF-Diagram Specification](https://infai.gitlab.io/rdf-diagram-framework/docs/), converting it to RDF serializations. A component of the [RDF-Diagram-Framework](https://gitlab.com/infai/rdf-diagram-framework).

### Features

* Convert DrawIO diagrams to RDF serializations (N-Triple, Turtle)
* Support for both native **Rust** and **WebAssembly** targets
* [Autocompletion](https://docs.rs/rdf-diagram-lib/0.1.0/rdf_diagram_lib/autocomplete/struct.RdfAutoComplete.html) for RDF-Diagrams
* [Error](https://docs.rs/rdf-diagram-lib/0.1.0/rdf_diagram_lib/enum.Error.html) handling with element-specific context

### Usage

Install via cargo:

````bash
cargo install rdf-diagram-lib
````

#### Simple Conversion

Convert a DrawIO diagram to Turtle format with a single function call:

````rust
use rdf_diagram_lib::{drawio_to_turtle, Result};

let xml = include_str!("../tests/rdf-diagram/iri-relative-base.drawio");
let turtle = drawio_to_turtle(&xml)?;
assert_eq!(
    turtle,
    "\n<http://base.org#subject>\n    <http://base.org#predicate> <http://base.org#object>.\n"
);
````

#### Advanced Usage

For more control over the conversion process:

````rust
use rdf_diagram_lib::{Diagram, Result};
use rdf_diagram_lib::formats::{Deserialize, Serialize};
use rdf_diagram_lib::formats::rdf_diagram::RdfConfig;
use rdf_diagram_lib::formats::drawio::DrawioDiagram;

// Parse DrawIO XML
let xml = include_str!("../tests/rdf-diagram/iri-relative-base.drawio");
let diagram = Diagram::from_str::<DrawioDiagram>(&xml)?;

// Configure output format
let config = RdfConfig::NTriple;

// Generate N-Triple serialization
let n_triple = diagram.stringify_with_config(&config)?;
assert_eq!(
    n_triple,
    "<http://base.org#subject> <http://base.org#predicate> <http://base.org#object>.\n"
);
````
<!-- cargo-sync-rdme ]] -->
