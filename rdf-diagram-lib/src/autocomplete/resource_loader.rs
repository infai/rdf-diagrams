use std::fmt::Debug;
use std::str::FromStr;

use anyhow::anyhow;
use ehttp::{Headers, Request};
use sophia::api::graph::CollectibleGraph;
use sophia::api::parser::TripleParser;
use sophia::api::source::TripleSource;
use sophia::iri::Iri;
use sophia::turtle::parser::{nt, turtle};
use sophia::xml::parser;
use strum::IntoEnumIterator;
use strum_macros::{EnumIter, EnumString, IntoStaticStr};
use url::Url;

use crate::Error;

#[derive(Debug, strum_macros::Display, EnumString, EnumIter, IntoStaticStr)]
enum RdfContentType {
    #[strum(serialize = "application/n-triples")]
    NTriples,
    #[strum(serialize = "application/rdf+xml")]
    RdfXml,
    #[strum(serialize = "text/turtle")]
    Turtle,
}

impl RdfContentType {
    fn try_from(value: &str) -> Result<Self, Error> {
        value
            .split(';')
            .map(str::trim)
            .flat_map(RdfContentType::from_str)
            .next()
            .ok_or(anyhow!("Did not return RDF serialization").into())
    }

    fn _headers() -> Headers {
        Headers::new(&[(
            "Accept",
            &RdfContentType::iter()
                .map(|content_type| content_type.to_string())
                .reduce(|header_value, content_type| {
                    header_value + ", " + &content_type
                })
                .unwrap(),
        )])
    }
}

pub async fn get_resource<G>(iri: &Url) -> Result<G, Error>
where
    G: CollectibleGraph,
{
    let request = Request {
        // todo: https://github.com/dbpedia/archivo/issues/50
        // headers: RdfContentType::headers(),
        ..Request::get(proxied(iri)?)
    };

    let response = ehttp::fetch_async(request)
        .await
        .map_err(|err| anyhow!(err))?;

    let content_type = response
        .content_type()
        .and_then(|content_type| RdfContentType::try_from(content_type).ok())
        .ok_or(anyhow!("Failed to find a valid RDF content type"))?;

    let content = response.text().unwrap_or_default();

    extract_graph(content, iri, &content_type)
}

fn proxied(iri: &Url) -> Result<Url, Error> {
    Ok(Url::parse(&format!(
        "https://archivo.dbpedia.org/download?o={}&f=nt",
        iri
    ))?)
}

fn extract_graph<G>(
    content: &str,
    base: &Url,
    content_type: &RdfContentType,
) -> Result<G, Error>
where
    G: CollectibleGraph,
{
    let base = Iri::new(base.to_string())?;
    let graph: G = match content_type {
        RdfContentType::Turtle => turtle::TurtleParser { base: Some(base) }
            .parse_str(content)
            .collect_triples()
            .map_err(|err| anyhow!(err.unwrap_source_error()))?,
        RdfContentType::NTriples => nt::NTriplesParser {}
            .parse_str(content)
            .collect_triples()
            .map_err(|err| anyhow!(err.unwrap_source_error()))?,
        RdfContentType::RdfXml => parser::RdfXmlParser { base: Some(base) }
            .parse_str(content)
            .collect_triples()
            .map_err(|err| anyhow!(err.unwrap_source_error()))?,
    };
    Ok(graph)
}

#[cfg(test)]
mod tests {

    use rstest::rstest;
    use sophia::api::graph::Graph;
    use sophia::inmem::graph::FastGraph;

    use super::*;
    #[cfg(target_arch = "wasm32")]
    use crate::wasm;

    #[rstest]
    #[case("http://xmlns.com/foaf/0.1/")]
    #[case("http://www.w3.org/2002/07/owl#")]
    #[cfg_attr(target_arch = "wasm32", wasm::test)]
    async fn test_get_resource(#[case] iri: &str) {
        let iri = Url::parse(iri).unwrap();
        let graph: FastGraph = get_resource(&iri).await.unwrap();
        let last_iri = graph.iris().last().unwrap().ok();
        assert!(last_iri.is_some());
    }
}
