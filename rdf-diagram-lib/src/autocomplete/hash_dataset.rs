use std::collections::{HashMap, HashSet};

use itertools::Itertools;
use sophia::api::dataset::Dataset;
use sophia::api::graph::Graph;
use sophia::api::term::{IriRef, SimpleTerm};
use sophia::inmem::graph::FastGraph;
use url::Url;

use crate::autocomplete::resource_loader::get_resource;
use crate::diagram::formats::rdf_diagram::Namespace;
use crate::{Error, RdfDiagram, Result};

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
struct GraphName(Option<Url>);

impl<'dataset> From<GraphName> for Option<SimpleTerm<'dataset>> {
    fn from(value: GraphName) -> Option<SimpleTerm<'dataset>> {
        value.0.map(|x| {
            SimpleTerm::Iri(IriRef::new_unchecked(x.as_str().to_owned().into()))
        })
    }
}

impl From<Namespace> for GraphName {
    fn from(value: Namespace) -> Self {
        GraphName(Some(Url::parse(&value).unwrap()))
    }
}

#[derive(Debug, Default)]
pub struct CompletionDataset {
    graphs: HashMap<GraphName, FastGraph>,
}

// Does not fail when a resource could not be fetched
impl CompletionDataset {
    pub async fn update(&mut self, rdf_diagram: &RdfDiagram) -> Result<()> {
        let graphs_current: HashSet<GraphName> =
            self.graphs.keys().cloned().collect();
        let graphs_update: HashSet<GraphName> =
            rdf_diagram.ns_names().map_into().collect();

        self.graphs.retain(|k, _| graphs_update.contains(k));

        for graph_name in graphs_update.difference(&graphs_current) {
            if let Some(iri) = graph_name.0.as_ref() {
                if let Ok(graph) = get_resource(iri).await {
                    self.graphs.insert(graph_name.to_owned(), graph);
                }
            }
        }

        self.graphs.insert(GraphName(None), rdf_diagram.try_into()?);

        Ok(())
    }
}

impl Dataset for CompletionDataset {
    type Error = Error;
    type Quad<'dataset>
        = (Option<SimpleTerm<'dataset>>, [SimpleTerm<'dataset>; 3])
    where
        Self: 'dataset;

    fn quads(&self) -> sophia::api::dataset::DQuadSource<Self> {
        let quads = self
            .graphs
            .iter()
            .flat_map(|(graph_name, graph)| {
                graph.triples().map_ok(|triple| {
                    (graph_name.clone().into(), triple.map(ToOwned::to_owned))
                })
            })
            .map(|result| result.map_err(Into::into));
        Box::new(quads)
    }
}
