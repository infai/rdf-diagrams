use std::hash::{DefaultHasher, Hash, Hasher};

use chrono::Utc;
use euclid::default::Rect;
use ordered_float::OrderedFloat;
use quick_xml::escape::escape;

use super::{
    Cell, DrawioDiagram, MxCell, MxGeometry, MxGraphModel, Root, UserObject,
};
use crate::diagram::formats::drawio;
use crate::diagram::{Container, Diagram, DiagramNode, Edge, Id};
use crate::graph::{Graph, IdTrait, Link};
use crate::{Error, Result};

static ROOT_NODE_ID: &str = "0";
static DEFAULT_LAYER_ID: &str = "1";

#[rustfmt::skip]
static NODE_STYLE: &str = "points=[];\
html=1;\
shadow=1;\
perimeterSpacing=3;\
spacingLeft=10;\
spacingRight=10;\
allowArrows=1;\
rotatable=0;\
overflow=hidden;\
whiteSpace=wrap;\
fontSize=14;";
#[rustfmt::skip]
static CONTAINER_STYLE: &str = "swimlane;\
collapsible=1;\
childLayout=stackLayout;\
swimlaneFillColor=default;\
resizeParent=1;\
resizeLast=0;\
resizeParentMax=0;\
marginBottom=10;\
marginTop=10;\
marginRight=10;\
marginLeft=10;\
spacingLeft=5;\
spacingRight=5;\
fontStyle=0;";
static NAMED_CONTAINER_STYLE: &str = "startSize=30;";
static ANON_CONTAINER_STYLE: &str = "startSize=0;";
#[rustfmt::skip]
static EDGE_STYLE: &str = "html=1;\
edgeStyle=elbowEdgeStyle;\
rounded=0;\
orthogonalLoop=1;\
jettySize=auto;\
endArrow=block;\
endFill=1;\
endSize=5;\
spacingBottom=20;\
shadow=1;\
fontSize=14;\
labelBackgroundColor=none;";

impl TryFrom<&Diagram> for DrawioDiagram {
    type Error = Error;

    fn try_from(diagram: &Diagram) -> Result<Self> {
        let mut drawio = DrawioDiagram::default();
        let set_parent = |mut cell: Cell| -> Result<Cell> {
            let id: Id = cell.id().into();
            let mut parents = diagram.nodes_iter().filter(|node| {
                node.containee_ids_iter().any(|containee| containee == &id)
            });
            let parent = parents
                .next()
                .map(DiagramNode::id)
                .map(Into::into)
                .unwrap_or(DEFAULT_LAYER_ID.to_owned());
            if parents.next().is_some() {
                panic!("More than one parent");
            }
            cell.mx_cell_mut().parent = parent;
            Ok(cell)
        };
        let mut nodes: Vec<Cell> = diagram
            .nodes_iter()
            .map(Into::into)
            .map(set_parent)
            .collect::<Result<Vec<_>>>()?;
        let edges = diagram.edges_iter().map(Into::into);
        nodes.extend(edges);
        drawio.diagram.mx_graph_model.root.cells.extend(nodes);
        drawio.generate_etag();
        Ok(drawio)
    }
}

impl DrawioDiagram {
    fn generate_etag(&mut self) {
        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        self.etag = hasher.finish().to_string();
    }
}

// Assumes that `style` includes `html=1`
fn escape_label(str: &str) -> String {
    escape(str).replace('\n', "<br>")
}

impl Default for DrawioDiagram {
    fn default() -> Self {
        DrawioDiagram {
            diagram: drawio::Diagram {
                mx_graph_model: MxGraphModel {
                    root: Root {
                        cells: vec![
                            Cell::new_root(),
                            Cell::new_default_layer(),
                        ],
                    },
                },
            },
            host: "Electron".to_owned(),
            modified: Utc::now(),
            agent: "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 \
                    (KHTML, like Gecko) draw.io/23.0.2 Chrome/120.0.6099.109 \
                    Electron/28.1.0 Safari/537.36"
                .to_string(),
            version: "23.0.2".to_owned(),
            etag: String::new(),
        }
    }
}

impl Cell {
    fn new_root() -> Cell {
        let mx_cell = MxCell {
            id: ROOT_NODE_ID.to_owned(),
            ..Default::default()
        };
        Cell::MxCell(mx_cell)
    }

    fn new_default_layer() -> Cell {
        let mx_cell = MxCell {
            id: DEFAULT_LAYER_ID.to_owned(),
            parent: ROOT_NODE_ID.to_owned(),
            ..Default::default()
        };
        Cell::MxCell(mx_cell)
    }
}

impl From<&DiagramNode> for Cell {
    fn from(node: &DiagramNode) -> Self {
        let (id, label, style, geometry) = match node {
            DiagramNode::AnonContainer {
                id,
                geometry,
                containees_orientation,
                ..
            } => (
                id,
                "",
                [
                    CONTAINER_STYLE,
                    ANON_CONTAINER_STYLE,
                    // Must be last because fontSize must be last
                    NODE_STYLE,
                    &format!("horizontalStack={};", containees_orientation),
                ]
                .concat(),
                geometry,
            ),
            DiagramNode::Container {
                id,
                geometry,
                label,
                orientation,
                containees_orientation,
                ..
            } => (
                id,
                label.as_str(),
                [
                    CONTAINER_STYLE,
                    NAMED_CONTAINER_STYLE,
                    // Must be last because fontSize must be last
                    NODE_STYLE,
                    &format!("horizontal={};", orientation),
                    &format!("horizontalStack={};", containees_orientation),
                ]
                .concat(),
                geometry,
            ),
            DiagramNode::Node {
                id,
                geometry,
                label,
            } => (id, label.as_str(), NODE_STYLE.to_string(), geometry),
        };

        Cell::UserObject(UserObject {
            id: id.to_string(),
            label: escape_label(label),
            mx_cell: MxCell {
                id: "".to_string(),
                value: "".to_string(),
                style,
                parent: "".to_string(),
                source: "".to_string(),
                target: "".to_string(),
                is_node: true,
                is_edge: false,
                geometry: Some(geometry.into()),
            },
        })
    }
}

impl<L> From<&Edge<L>> for Cell
where
    L: ToString,
{
    fn from(edge: &Edge<L>) -> Self {
        Cell::UserObject(UserObject {
            id: edge.id().to_string(),
            label: escape_label(&edge.label.to_string()),
            mx_cell: MxCell {
                id: "".to_string(),
                value: "".to_string(),
                style: EDGE_STYLE.to_owned(),
                parent: DEFAULT_LAYER_ID.to_string(),
                source: edge.source().to_string(),
                target: edge.target().to_string(),
                is_node: false,
                is_edge: true,
                geometry: Some(MxGeometry::new_edge()),
            },
        })
    }
}

impl From<&Rect<i32>> for MxGeometry {
    fn from(rect: &Rect<i32>) -> Self {
        let x = if rect.origin.x == 0 {
            None
        } else {
            Some(OrderedFloat::from(rect.origin.x))
        };
        let y = if rect.origin.y == 0 {
            None
        } else {
            Some(OrderedFloat::from(rect.origin.y))
        };
        MxGeometry {
            x,
            y,
            width: Some(OrderedFloat::from(rect.size.width)),
            height: Some(OrderedFloat::from(rect.size.height)),
            as_field: "geometry".to_owned(),
            relative: false,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::PathBuf;

    use pretty_assertions::assert_eq;
    use rstest::rstest;

    use super::*;

    #[rstest]
    fn from(#[files("tests/**/*.drawio")] path: PathBuf) -> anyhow::Result<()> {
        use crate::diagram::formats::Deserialize;

        let xml = fs::read_to_string(path)?;
        let expected = DrawioDiagram::from_str::<DrawioDiagram>(&xml)?;
        let diagram = Diagram::from_str::<DrawioDiagram>(&xml)?;
        let actual: DrawioDiagram = (&diagram).try_into()?;
        assert_eq!(expected, actual);
        Ok(())
    }
}
