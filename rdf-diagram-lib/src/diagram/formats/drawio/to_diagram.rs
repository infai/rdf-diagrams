use std::collections::HashSet;

use anyhow::anyhow;
use euclid::default::{Point2D, Rect, Size2D};
use ordered_float::OrderedFloat;

use super::{Cell, CellKind, DrawioDiagram, MxCell, MxGeometry, UserObject};
use crate::diagram::{Containers, Diagram, DiagramNode, Edge, Id};
use crate::graph::{Graph, HashMapGraph};
use crate::Error;

impl TryFrom<DrawioDiagram> for Diagram {
    type Error = Error;

    fn try_from(drawio_diagram: DrawioDiagram) -> crate::Result<Diagram> {
        let mut diagram: Diagram = HashMapGraph::new();
        add_nodes(&mut diagram, &drawio_diagram)?;
        add_children(&mut diagram, &drawio_diagram)?;
        add_edges(&mut diagram, &drawio_diagram)?;
        Ok(diagram)
    }
}

impl DrawioDiagram {
    fn cells(&self) -> impl Iterator<Item = &Cell> {
        self.diagram.mx_graph_model.root.cells.iter()
    }

    fn cells_filter_by_kind(
        &self,
        kind: CellKind,
    ) -> impl Iterator<Item = &Cell> {
        self.cells().filter(move |cell| cell.kind() == kind)
    }

    fn layers(&self) -> impl Iterator<Item = &Cell> {
        self.cells_filter_by_kind(CellKind::Layer)
    }

    fn nodes(&self) -> impl Iterator<Item = &Cell> {
        self.cells_filter_by_kind(CellKind::Node)
    }

    fn edges(&self) -> impl Iterator<Item = &Cell> {
        self.cells_filter_by_kind(CellKind::Edge)
    }

    /// All cells whose parent is a container
    fn containees(&self) -> impl Iterator<Item = &Cell> {
        let layer_ids: HashSet<String> = self.layers().map(Cell::id).collect();
        self.nodes()
            .filter(move |node| !layer_ids.contains(node.parent()))
    }
}

fn add_nodes(
    diagram: &mut Diagram,
    drawio_diagram: &DrawioDiagram,
) -> Result<(), Error> {
    let nodes: Result<Vec<DiagramNode>, Error> =
        drawio_diagram.nodes().map(|node| node.try_into()).collect();
    diagram.add_nodes(nodes?);
    Ok(())
}

fn add_children(
    diagram: &mut Diagram,
    mx_file: &DrawioDiagram,
) -> Result<(), Error> {
    for node in mx_file.containees() {
        let parent_id: Id = node.parent().into();
        let child_id: Id = node.id().into();
        diagram.add_containee(&parent_id, child_id)?;
    }
    Ok(())
}

fn add_edges(
    diagram: &mut Diagram,
    drawio_diagram: &DrawioDiagram,
) -> Result<(), Error> {
    drawio_diagram
        .edges()
        .map(Into::into)
        .try_for_each(|edge: Edge<String>| diagram.add_edge(edge))?;
    Ok(())
}

impl Cell {
    fn label(&self) -> String {
        let label = match self {
            Cell::UserObject(UserObject { label, .. })
            | Cell::MxCell(MxCell { value: label, .. }) => label,
        };
        let text = if self.is_html_encoded() {
            html2text::from_read(label.as_bytes(), usize::MAX)
        } else {
            label.clone()
        };
        text.trim_end_matches('\n').to_string()
    }
}

impl TryFrom<&Cell> for DiagramNode {
    type Error = Error;

    fn try_from(cell: &Cell) -> Result<Self, Error> {
        let id: Id = cell.id().into();
        let geometry: Rect<i32> = cell
            .mx_geometry()
            .ok_or_else(|| Error::ElementError {
                id: id.to_owned(),
                source: anyhow!("Missing mxGeometry"),
            })?
            .try_into()?;
        let label = cell.label();
        let orientation = cell.orientation();
        let containees_orientation = cell.containees_orientation();

        let diagram_node = match (cell.is_container(), cell.is_anon_container())
        {
            (true, true) => DiagramNode::AnonContainer {
                id,
                geometry,
                containees_orientation,
                containees: Vec::new(),
            },
            (true, false) => DiagramNode::Container {
                id,
                geometry,
                label,
                orientation,
                containees_orientation,
                containees: Vec::new(),
            },
            (false, _) => DiagramNode::Node {
                id,
                label,
                geometry,
            },
        };
        Ok(diagram_node)
    }
}

impl From<&Cell> for Edge<String> {
    fn from(cell: &Cell) -> Self {
        Self::new(
            cell.id().into(),
            cell.label(),
            cell.source().into(),
            cell.target().into(),
        )
    }
}

impl TryFrom<&MxGeometry> for Rect<i32> {
    type Error = Error;

    fn try_from(mx_geometry: &MxGeometry) -> Result<Self, Error> {
        match (
            mx_geometry.x,
            mx_geometry.y,
            mx_geometry.width,
            mx_geometry.height,
        ) {
            (x, y, Some(width), Some(height)) => {
                let x = x.unwrap_or(OrderedFloat(0.0));
                let y = y.unwrap_or(OrderedFloat(0.0));
                Ok(Rect::new(
                    Point2D::new(x.into_inner() as i32, y.into_inner() as i32),
                    Size2D::new(
                        width.into_inner() as i32,
                        height.into_inner() as i32,
                    ),
                ))
            }
            _ => Err(Error::Other(anyhow!("Invalid geometry"))),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::PathBuf;

    use insta;
    use rstest::rstest;

    use super::*;
    use crate::diagram::formats::Deserialize;
    use crate::tests::set_snapshot_path;

    #[rstest]
    fn to(#[files("tests/**/*.drawio")] path: PathBuf) -> anyhow::Result<()> {
        let xml = fs::read_to_string(&path)?;
        let diagram = Diagram::from_str::<DrawioDiagram>(&xml)?;
        set_snapshot_path!(path);
        insta::assert_debug_snapshot!(diagram);
        Ok(())
    }
}
