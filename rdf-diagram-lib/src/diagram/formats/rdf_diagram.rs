mod from_diagram;
mod label_parser;
mod serialize;
mod to_rdf_graph;

use core::fmt::Debug;
use std::borrow::Borrow;
use std::fmt::Display;
use std::hash::Hash;
use std::ops::Deref;

use hashlink::LinkedHashMap;
use itertools::Either;
use strum_macros::EnumDiscriminants;
use url::{ParseError, Url};
use uuid::Uuid;
use wasm_bindgen::prelude::wasm_bindgen;

pub use self::serialize::RdfConfig;
use crate::diagram::{Container, Edge, Id};
use crate::graph::{Graph, HashMapGraph, IdTrait};
use crate::{Error, IntoElementError};

#[derive(thiserror::Error, Debug, PartialEq, Eq)]
pub enum RdfError {
    #[error("base can be defined only one time")]
    DuplicateBase,
    #[error("base is not a valid IRI")]
    InvalidBase { source: ParseError },
    #[error("{0} is defined")]
    MissingPrefix(String),
    #[error("no valid RDF container type: {0}")]
    AssignRdfContainer(String),
    #[error(
        "{text} could not be parsed by tree-sitter: {abstract_syntax_tree}"
    )]
    ParseLabel {
        text: String,
        abstract_syntax_tree: String,
    },
    #[error("predicate must be \"a\" or an IRI")]
    InvalidPredicate,
    #[error("{target} cannot be linked")]
    InvalidLinkTarget { target: String },
}

impl RdfError {
    fn into_error_result<T>(
        result: Result<T, Self>,
        id: Id,
    ) -> Result<Vec<T>, Error> {
        result
            .map(|item| vec![item])
            .map_err(|error| error.into_element_error(&id))
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Base(pub String);

impl Display for Base {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<String> for Base {
    fn from(string: String) -> Self {
        Base(string)
    }
}

impl From<&str> for Base {
    fn from(string: &str) -> Self {
        Base(string.to_owned())
    }
}

impl Deref for Base {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Borrow<str> for Base {
    fn borrow(&self) -> &str {
        &self.0
    }
}

pub type Prefix = String;
pub type Namespace = String;
pub type Namespaces = LinkedHashMap<Prefix, Namespace>;

#[wasm_bindgen]
#[derive(Debug, Default)]
pub struct RdfDiagram {
    base: Option<Base>,
    namespaces: Namespaces,
    inner: HashMapGraph<Id, RdfNode, RdfEdge>,
}

impl RdfDiagram {
    pub fn base(&self) -> Option<Base> {
        self.base.to_owned()
    }

    pub fn namespaces(&self) -> &Namespaces {
        &self.namespaces
    }

    pub fn prefixes(&self) -> impl Iterator<Item = Prefix> + '_ {
        self.namespaces().keys().cloned()
    }

    pub fn ns_names(&self) -> impl Iterator<Item = Namespace> + '_ {
        self.namespaces().values().cloned()
    }

    fn new() -> Self {
        Self::default()
    }

    fn add_node(&mut self, node: impl Into<RdfNode>) -> Result<(), RdfError> {
        let rdf_node = node.into();
        if let RdfNode::Node {
            label:
                RdfLabel::TurtleDoc {
                    ref base,
                    ref namespaces,
                    ..
                },
            ..
        } = rdf_node
        {
            base.to_owned().map(|base| self.set_base(base));
            namespaces.iter().for_each(|(prefix, ns_name)| {
                self.add_namespace(prefix.to_owned(), ns_name.to_owned())
            })
        };
        self.inner.add_node(rdf_node);
        Ok(())
    }

    fn set_base(&mut self, base: Base) -> Result<(), RdfError> {
        if self.base.is_some() {
            Err(RdfError::DuplicateBase)
        } else {
            Url::parse(&base.to_string())
                .map_err(|source| RdfError::InvalidBase { source })?;
            self.base = Some(base);
            Ok(())
        }
    }

    fn get_namespace(&self, ns: &str) -> Option<&str> {
        self.namespaces.get(ns).map(String::as_str)
    }

    fn add_namespace(&mut self, prefix: String, ns_name: String) {
        self.namespaces.insert(prefix, ns_name);
    }
}

#[derive(Debug, PartialEq)]
enum RdfNode {
    Container { id: Id, container: RdfContainer },
    Node { id: Id, label: RdfLabel },
}

impl RdfNode {
    fn get_turtle_directives(&self) -> Vec<&str> {
        match self {
            RdfNode::Node {
                label: RdfLabel::TurtleDoc { directives, .. },
                ..
            } => directives.iter().map(String::as_str).collect(),
            _ => Vec::new(),
        }
    }

    fn get_turtle_triples(&self) -> Vec<&str> {
        match self {
            RdfNode::Node {
                label: RdfLabel::TurtleDoc { triples, .. },
                ..
            } => triples.iter().map(String::as_str).collect(),
            _ => Vec::new(),
        }
    }
}

impl IdTrait<Id> for RdfNode {
    fn id(&self) -> &Id {
        match self {
            Self::Container { id, .. } | Self::Node { id, .. } => id,
        }
    }
}

#[derive(Debug, PartialEq, EnumDiscriminants)]
#[strum_discriminants(derive(Hash), name(DisRdfContainer))]
enum RdfContainer {
    Terms {
        containees: Vec<Id>,
    },
    Properties {
        label: RdfLabel,
        containees: Vec<Id>,
    },
    Triples {
        label: RdfLabel,
        containees: Vec<Id>,
    },
    Collection {
        label: RdfLabel,
        containees: Vec<(RdfLabel, Id)>,
    },
}

impl Container for RdfContainer {
    type Id = Id;

    fn containee_ids_iter(&self) -> impl Iterator<Item = &Id> {
        match self {
            RdfContainer::Terms { containees, .. }
            | RdfContainer::Properties { containees, .. }
            | RdfContainer::Triples { containees, .. } => {
                Either::Left(containees.iter())
            }
            RdfContainer::Collection { containees, .. } => {
                Either::Right(containees.iter().map(|(_, containee)| containee))
            }
        }
    }

    fn add_containee(&mut self, containee: impl Into<Id>) {
        match self {
            RdfContainer::Terms { containees, .. }
            | RdfContainer::Properties { containees, .. }
            | RdfContainer::Triples { containees, .. } => {
                containees.push(containee.into())
            }
            RdfContainer::Collection { containees, .. } => containees
                .push((RdfLabel::new_anon_blank_node(), containee.into())),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
enum RdfLabel {
    Term(Term),
    Property {
        predicate: Iri,
        objects: Vec<Term>,
    },
    TurtleDoc {
        base: Option<Base>,
        namespaces: LinkedHashMap<String, String>,
        directives: Vec<String>,
        triples: Vec<String>,
    },
}

impl RdfLabel {
    fn new_anon_blank_node() -> Self {
        let bnode_id = Uuid::new_v4().to_string();
        RdfLabel::Term(Term::BlankNode(BlankNode::Anon { bnode_id }))
    }
}

#[derive(Debug, Clone, PartialEq)]
enum Term {
    Iri(Iri),
    BlankNode(BlankNode),
    Literal(Literal),
}

#[derive(Debug, Clone, PartialEq)]
enum Iri {
    IriReference { iri: String },
    PrefixedName { pn_prefix: String, pn_local: String },
}

#[derive(Debug, Clone, PartialEq)]
enum BlankNode {
    Anon { bnode_id: String },
    BlankNodeLabel { bnode_id: String },
}

#[derive(Debug, Clone, PartialEq)]
enum Literal {
    Rdf {
        lexical_form: String,
        lang_tag: Option<String>,
        datatype: Option<Iri>,
    },
    Numeric(NumericLiteral),
    Boolean {
        lexical_form: String,
    },
}

#[derive(Debug, Clone, PartialEq)]
enum NumericLiteral {
    Decimal { lexical_form: String },
    Double { lexical_form: String },
    Integer { lexical_form: String },
}

type RdfEdge = Edge<RdfLabel>;

#[cfg(test)]
mod tests {
    use std::fs;
    use std::fs::read_to_string;
    use std::path::PathBuf;

    use anyhow::anyhow;
    use pretty_assertions::assert_eq;
    use rstest::rstest;
    use sophia::api::source::TripleSource;
    use sophia::inmem::graph::FastGraph;
    use sophia::isomorphism::isomorphic_graphs;
    use sophia::turtle::parser::turtle;

    use super::*;
    use crate::diagram::formats::drawio::DrawioDiagram;
    use crate::diagram::formats::rdf_diagram::serialize::RdfConfig;
    use crate::diagram::formats::{Deserialize, Serialize};

    #[rstest]
    fn test(
        #[files("tests/rdf-diagram/*.drawio")]
        #[files("tests/completion/*.drawio")]
        path: PathBuf,
    ) -> Result<(), Error> {
        use sophia::turtle::parser::turtle::parse_str;

        let ttl_file = path.with_extension("ttl");
        let actual_turtle = {
            let xml = fs::read_to_string(&path)?;
            RdfDiagram::from_str::<DrawioDiagram>(&xml)?
                .stringify_with_config(&RdfConfig::default())?
        };
        let expected_turtle = read_to_string(&ttl_file)
            .unwrap_or_else(|_| panic!("{}", ttl_file.display()));

        let actual_graph: FastGraph =
            parse_str(&actual_turtle).collect_triples().unwrap();

        let expected_graph: FastGraph = turtle::parse_str(&expected_turtle)
            .collect_triples()
            .unwrap();

        match isomorphic_graphs(&actual_graph, &expected_graph) {
            Ok(true) => Ok(()),
            Ok(false) => {
                assert_eq!(
                    actual_turtle, expected_turtle,
                    "The generated graph string does not match the contents \
                     of the expected TTL file"
                );
                Ok(())
            }

            Err(e) => {
                Err(anyhow!("Error checking graph isomorphism: {:?}", e).into())
            }
        }
    }
}
