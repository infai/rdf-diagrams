use std::fmt::Display;
use std::io::Write;

use sophia::api::prefix::Prefix;
use sophia::api::serializer::{Stringifier, TripleSerializer};
use sophia::api::source::StreamError;
use sophia::api::MownStr;
use sophia::inmem::graph::FastGraph;
use sophia::inmem::index::TermIndexFullError;
use sophia::iri::Iri;
use sophia::turtle::serializer::nt::NtSerializer;
use sophia::turtle::serializer::turtle::{TurtleConfig, TurtleSerializer};

use super::RdfDiagram;
use crate::diagram::formats::serialize_params::{Indentation, Mode};
use crate::diagram::formats::{Config, Serialize};
use crate::{Error, Result};

/// Configuration for serializing to RDF.
pub enum RdfConfig {
    Turtle {
        mode: Mode,
        indentation: Indentation,
    },
    NTriple,
}

impl Config for RdfConfig {
    type Format = RdfDiagram;
}

impl Default for RdfConfig {
    fn default() -> Self {
        Self::Turtle {
            mode: Mode::Pretty,
            indentation: 4,
        }
    }
}

impl Serialize for RdfDiagram {
    type Config = RdfConfig;

    fn serialize_with_config<W: Write>(
        &self,
        config: &Self::Config,
        writer: &mut W,
    ) -> Result<()> {
        let graph: FastGraph = self.try_into()?;
        match config {
            RdfConfig::Turtle { mode, indentation } => {
                let sophia_config =
                    self.make_sophia_turtle_config(mode, *indentation);
                TurtleSerializer::new_with_config(writer, sophia_config)
                    .serialize_graph(&graph)?;
            }
            RdfConfig::NTriple => {
                NtSerializer::new(writer).serialize_graph(&graph)?;
            }
        }
        Ok(())
    }

    fn stringify_with_config(&self, config: &Self::Config) -> Result<String> {
        let mut writer = Vec::new();
        self.serialize_with_config(config, &mut writer)?;
        Ok(String::from_utf8(writer).expect("Sophia serializes to UTF-8"))
    }
}

impl From<StreamError<TermIndexFullError, std::io::Error>> for Error {
    fn from(error: StreamError<TermIndexFullError, std::io::Error>) -> Self {
        Error::Other(error.unwrap_sink_error().into())
    }
}

impl Display for RdfDiagram {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let graph: FastGraph = self.try_into().map_err(|_| std::fmt::Error)?;
        let config = self.make_sophia_turtle_config(&Mode::Pretty, 4);
        let turtle = TurtleSerializer::new_stringifier_with_config(config)
            .serialize_graph(&graph)
            .map_err(|_| std::fmt::Error)?
            .to_string();
        write!(f, "{}", turtle)
    }
}

impl RdfDiagram {
    fn make_sophia_turtle_config(
        &self,
        mode: &Mode,
        indentation: usize,
    ) -> TurtleConfig {
        let prefix_map: Vec<(Prefix<MownStr>, Iri<MownStr>)> = self
            .namespaces()
            .iter()
            .map(|(prefix, ns_name)| {
                (
                    Prefix::new_unchecked(prefix.clone().into()),
                    Iri::new_unchecked(ns_name.clone().into()),
                )
            })
            .collect();
        TurtleConfig::new()
            .with_pretty(matches!(mode, Mode::Pretty))
            .with_indentation(" ".repeat(indentation))
            .with_prefix_map(&prefix_map[..])
    }
}
