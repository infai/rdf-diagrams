use itertools::Itertools;
#[cfg(not(target_arch = "wasm32"))]
use tree_sitter::{Parser, Query, QueryCursor, Tree, TreeCursor};
#[cfg(target_arch = "wasm32")]
use tree_sitter_c2rust::{Parser, Query, QueryCursor, Tree, TreeCursor};
use uuid::Uuid;

use super::{
    BlankNode, Iri, Literal, NumericLiteral, RdfError, RdfLabel, Term,
};

const RDF_TYPE_REF: &str = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";
const RDF_TYPE: &str = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
const PANIC: &str = "internal invariant violated: expected to be a successful \
                     operation due to prior parse with treesitter";

pub struct LabelParser<'parser> {
    tree: Tree,
    str: &'parser str,
}

impl<'parser> LabelParser<'parser> {
    pub(crate) fn try_parse(str: &'parser str) -> Result<RdfLabel, RdfError> {
        let str = match str {
            "a" => RDF_TYPE_REF,
            _ if str.is_empty() => "[]",
            _ => str,
        };
        let mut parser = Parser::new();
        parser
            .set_language(&tree_sitter_rdf_diagram::language())
            .expect(PANIC);
        let tree = parser.parse(str, None).expect(PANIC);
        let root_node = tree.root_node();

        if root_node.has_error() {
            return Err(RdfError::ParseLabel {
                text: str.to_owned(),
                abstract_syntax_tree: tree.root_node().to_sexp(),
            });
        };

        Ok(Self { tree, str }.parse())
    }

    fn parse(&self) -> RdfLabel {
        let cursor = self.tree.walk();
        self.rdf_label(cursor)
    }

    fn bytes(&self) -> &'parser [u8] {
        self.str.as_bytes()
    }

    fn cursor_as_string(&self, cursor: &TreeCursor) -> String {
        cursor
            .node()
            .utf8_text(self.bytes())
            .expect(PANIC)
            .to_owned()
    }

    fn query(
        &self,
        query: &str,
        cursor: &TreeCursor<'parser>,
    ) -> Vec<TreeCursor<'parser>> {
        let query = Query::new(&tree_sitter_rdf_diagram::language(), query)
            .expect(PANIC);
        QueryCursor::new()
            .matches(&query, cursor.node(), self.bytes())
            .flat_map(|match_| match_.captures.iter())
            .map(|capture| capture.node.walk())
            .collect()
    }

    fn query_strings(&self, query: &str, cursor: &TreeCursor) -> Vec<String> {
        self.query(query, cursor)
            .iter()
            .map(|cursor| self.cursor_as_string(cursor))
            .collect()
    }

    fn query_str_option(
        &self,
        query: &str,
        cursor: &TreeCursor,
    ) -> Option<String> {
        self.query_strings(query, cursor).first().cloned()
    }

    fn query_str(&self, query: &str, cursor: &TreeCursor) -> String {
        self.query_str_option(query, cursor).expect(PANIC)
    }

    fn rdf_label(&self, mut cursor: TreeCursor) -> RdfLabel {
        cursor.goto_first_child();
        let rdf_label = match cursor.node().kind() {
            "term" => self.term(cursor).into(),
            "property" => self.property(cursor),
            "turtle_doc" => self.turtle_doc(cursor),
            _ => panic!("{}", PANIC),
        };
        rdf_label
    }

    fn term(&self, mut cursor: TreeCursor) -> Term {
        cursor.goto_first_child();
        match cursor.node().kind() {
            "iri" => self.iri(cursor).into(),
            "blank_node" => self.blank_node(cursor).into(),
            "literal" => self.literal(cursor).into(),
            _ => panic!("{}", PANIC),
        }
    }

    fn iri(&self, mut cursor: TreeCursor) -> Iri {
        cursor.goto_first_child();
        match cursor.node().kind() {
            "iri_reference" => Iri::IriReference {
                iri: self
                    .query_str_option(
                        "(iri_reference (lexical_form)@0)",
                        &cursor,
                    )
                    // In case of <> there wont be a lexical_form, so return an empty String
                    .unwrap_or_default(),
            },
            "prefixed_name" => Iri::PrefixedName {
                pn_prefix: self
                    .query_str_option("(pn_prefix) @0", &cursor)
                    .unwrap_or_default(),
                pn_local: self.query_str("(pn_local) @0", &cursor),
            },
            "a" => Iri::IriReference {
                iri: RDF_TYPE.to_owned(),
            },
            _ => panic!("{}", PANIC),
        }
    }

    fn blank_node(&self, mut cursor: TreeCursor) -> BlankNode {
        cursor.goto_first_child();
        match cursor.node().kind() {
            "anon" => BlankNode::Anon {
                bnode_id: Uuid::new_v4().to_string(),
            },
            "blank_node_label" => BlankNode::BlankNodeLabel {
                bnode_id: self.query_str("(blank_node_label (id)@0)", &cursor),
            },
            _ => panic!("{}", PANIC),
        }
    }

    fn literal(&self, mut cursor: TreeCursor) -> Literal {
        cursor.goto_first_child();
        match cursor.node().kind() {
            "rdf_literal" => Literal::Rdf {
                lexical_form: self.query_str("(lexical_form)@0", &cursor),
                lang_tag: self.query_str_option("(lang_tag)@0", &cursor),
                datatype: self
                    .query("datatype: (iri)@0", &cursor.clone())
                    .first()
                    .map(|cursor| self.iri(cursor.clone())),
            },
            "numeric_literal" => {
                cursor.goto_first_child();
                match cursor.node().kind() {
                    "decimal" => NumericLiteral::Decimal {
                        lexical_form: self.cursor_as_string(&cursor),
                    },
                    "double" => NumericLiteral::Double {
                        lexical_form: self.cursor_as_string(&cursor),
                    },
                    "integer" => NumericLiteral::Integer {
                        lexical_form: self.cursor_as_string(&cursor),
                    },
                    _ => panic!("{}", PANIC),
                }
                .into()
            }
            "boolean_literal" => Literal::Boolean {
                lexical_form: self.cursor_as_string(&cursor),
            },
            _ => panic!(),
        }
    }

    fn property(&self, mut cursor: TreeCursor) -> RdfLabel {
        let terms = self
            .query("(term)@0", &cursor)
            .into_iter()
            .map(|cursor| self.term(cursor))
            .collect();
        cursor.goto_first_child();
        cursor.goto_first_child();
        RdfLabel::Property {
            predicate: self.iri(cursor),
            objects: terms,
        }
    }

    fn turtle_doc(&self, cursor: TreeCursor) -> RdfLabel {
        let triples = self.query_strings("(statement (triples))@0", &cursor);
        let directives = self.query_strings("(directive)@0", &cursor);
        let base = self
            .query_str_option("(base (_ (lexical_form)@0))", &cursor)
            .map(Into::into);
        let namespaces = self
            .query_strings(
                "(prefix_id (namespace)@0 (_ (lexical_form)@1))",
                &cursor,
            )
            .into_iter()
            .tuples()
            .map(|(prefix, ns_name)| {
                (prefix[0..prefix.len() - 1].to_owned(), ns_name)
            })
            .collect();
        RdfLabel::TurtleDoc {
            base,
            namespaces,
            directives,
            triples,
        }
    }
}

impl From<Term> for RdfLabel {
    fn from(value: Term) -> Self {
        RdfLabel::Term(value)
    }
}

impl From<Iri> for Term {
    fn from(value: Iri) -> Self {
        Term::Iri(value)
    }
}

impl From<BlankNode> for Term {
    fn from(value: BlankNode) -> Self {
        Term::BlankNode(value)
    }
}

impl From<Literal> for Term {
    fn from(value: Literal) -> Self {
        Term::Literal(value)
    }
}

impl From<NumericLiteral> for Literal {
    fn from(numeric_literal: NumericLiteral) -> Self {
        Literal::Numeric(numeric_literal)
    }
}

#[cfg(test)]
mod tests {
    use hashlink::LinkedHashMap;
    use matches::assert_matches;
    use pretty_assertions::assert_eq;
    use rstest::rstest;

    use super::*;
    use crate::diagram::formats::rdf_diagram::{Base, RdfLabel, Term};

    fn test_rdf_label(input: &str, expected: RdfLabel) {
        let actual: RdfLabel = LabelParser::try_parse(input).unwrap();
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_term_iri_reference_absolute() {
        let input = "<http://absolute_iri>";
        let expected = RdfLabel::Term(Term::Iri(Iri::IriReference {
            iri: "http://absolute_iri".to_owned(),
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_iri_reference_relative() {
        let input = "<relative_iri>";
        let expected = RdfLabel::Term(Term::Iri(Iri::IriReference {
            iri: "relative_iri".to_owned(),
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_iri_reference_empty() {
        let input = "<>";
        let expected =
            RdfLabel::Term(Term::Iri(Iri::IriReference { iri: "".to_owned() }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_prefixed_name() {
        let input = "namespace:local";
        let expected = RdfLabel::Term(Term::Iri(Iri::PrefixedName {
            pn_prefix: "namespace".to_owned(),
            pn_local: "local".to_owned(),
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_prefixed_name_empty_prefix() {
        let input = ":local";
        let expected = RdfLabel::Term(Term::Iri(Iri::PrefixedName {
            pn_prefix: "".to_owned(),
            pn_local: "local".to_owned(),
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_blank_node_label() {
        let input = "_:alice";
        let expected =
            RdfLabel::Term(Term::BlankNode(BlankNode::BlankNodeLabel {
                bnode_id: "alice".to_owned(),
            }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_blank_node_anon() {
        let input = "[]";
        let actual = LabelParser::try_parse(input).unwrap();
        assert_matches!(
            actual,
            RdfLabel::Term(Term::BlankNode(BlankNode::Anon { ref bnode_id })) if !bnode_id.is_empty());
    }

    #[test]
    fn test_term_literal_rdf_single_quote() {
        let input = "'string'";
        let expected = RdfLabel::Term(Term::Literal(Literal::Rdf {
            lexical_form: "string".to_owned(),
            lang_tag: None,
            datatype: None,
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_literal_rdf_double_quote() {
        let input = "\"string\"";
        let expected = RdfLabel::Term(Term::Literal(Literal::Rdf {
            lexical_form: "string".to_owned(),
            lang_tag: None,
            datatype: None,
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_literal_rdf_lang_tag() {
        let input = "'string'@fr-be";
        let expected = RdfLabel::Term(Term::Literal(Literal::Rdf {
            lexical_form: "string".to_owned(),
            lang_tag: Some("fr-be".to_owned()),
            datatype: None,
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_literal_rdf_datatype_prefixed_name() {
        let input = "'string'^^xsd:String";
        let expected = RdfLabel::Term(Term::Literal(Literal::Rdf {
            lexical_form: "string".to_owned(),
            lang_tag: None,
            datatype: Some(Iri::PrefixedName {
                pn_prefix: "xsd".to_owned(),
                pn_local: "String".to_owned(),
            }),
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_literal_rdf_datatype_iri_reference() {
        let input = "'string'^^<http://example.org/foo>";
        let expected = RdfLabel::Term(Term::Literal(Literal::Rdf {
            lexical_form: "string".to_owned(),
            lang_tag: None,
            datatype: Some(Iri::IriReference {
                iri: "http://example.org/foo".to_owned(),
            }),
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_literal_rdf_with_new_lines() {
        let input = "'''This is a multi-line\nliteral with many quotes \
                     (\"\"\"\")\nand up to two sequential apostrophes ('').'''";
        let expected = RdfLabel::Term(Term::Literal(Literal::Rdf {
            lexical_form: "This is a multi-line\nliteral with many quotes \
                           (\"\"\"\")\nand up to two sequential apostrophes \
                           ('')."
                .to_owned(),
            lang_tag: None,
            datatype: None,
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_literal_numeric_integer() {
        let input = "2";
        let expected = RdfLabel::Term(Term::Literal(Literal::Numeric(
            NumericLiteral::Integer {
                lexical_form: "2".to_owned(),
            },
        )));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_literal_numeric_decimal() {
        let input = "4.002602";
        let expected = RdfLabel::Term(Term::Literal(Literal::Numeric(
            NumericLiteral::Decimal {
                lexical_form: "4.002602".to_owned(),
            },
        )));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_literal_numeric_double() {
        let input = "1.663E-4";
        let expected = RdfLabel::Term(Term::Literal(Literal::Numeric(
            NumericLiteral::Double {
                lexical_form: "1.663E-4".to_owned(),
            },
        )));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_term_literal_boolean_false() {
        let input = "false";
        let expected = RdfLabel::Term(Term::Literal(Literal::Boolean {
            lexical_form: "false".to_owned(),
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_property_single_object() {
        let input = "<p> <o>";
        let expected = RdfLabel::Property {
            predicate: Iri::IriReference {
                iri: "p".to_owned(),
            },
            objects: vec![Term::Iri(Iri::IriReference {
                iri: "o".to_owned(),
            })],
        };
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_property_a() {
        let input = "a <o>";
        let expected = RdfLabel::Property {
            predicate: Iri::IriReference {
                iri: "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
                    .to_owned(),
            },
            objects: vec![Term::Iri(Iri::IriReference {
                iri: "o".to_owned(),
            })],
        };
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_property_object_list() {
        let input = "<p> <o1>, <o2>";
        let expected = RdfLabel::Property {
            predicate: Iri::IriReference {
                iri: "p".to_owned(),
            },
            objects: vec![
                Term::Iri(Iri::IriReference {
                    iri: "o1".to_owned(),
                }),
                Term::Iri(Iri::IriReference {
                    iri: "o2".to_owned(),
                }),
            ],
        };
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_statement_triple() {
        let input = "<s> <p> <o> .\n<s> <p> <o> .\n";
        let expected = RdfLabel::TurtleDoc {
            base: None,
            namespaces: LinkedHashMap::new(),
            directives: Vec::new(),
            triples: input.lines().map(str::to_owned).collect(),
        };
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_statement_directive_base() {
        let input = "@base <http://one.example/> .\n\
                           BASE <http://one.example/>\n\
                           BAse <http://one.example/>\n";
        let expected = RdfLabel::TurtleDoc {
            base: Some(Base("http://one.example/".to_owned())),
            namespaces: LinkedHashMap::new(),
            directives: input.lines().map(str::to_owned).collect(),
            triples: Vec::new(),
        };
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_statement_directive_prefix() {
        let input = "@prefix p1: <http://one.example/> .\n\
                           PREFIX p2: <http://two.example/>\n\
                           PREfix p3: <http://three.example/>\n";
        let mut namespaces = LinkedHashMap::new();
        namespaces.insert("p1".to_owned(), "http://one.example/".to_owned());
        namespaces.insert("p2".to_owned(), "http://two.example/".to_owned());
        namespaces.insert("p3".to_owned(), "http://three.example/".to_owned());
        let expected = RdfLabel::TurtleDoc {
            base: None,
            namespaces,
            directives: input.lines().map(str::to_owned).collect(),
            triples: Vec::new(),
        };
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_a() {
        let input = "a";
        let expected = RdfLabel::Term(Term::Iri(Iri::IriReference {
            iri: RDF_TYPE.to_owned(),
        }));
        test_rdf_label(input, expected);
    }

    #[test]
    fn test_empty() {
        let input = "";
        let actual = LabelParser::try_parse(input).unwrap();
        assert_matches!(
            actual,
            RdfLabel::Term(Term::BlankNode(BlankNode::Anon { ref bnode_id })) if !bnode_id.is_empty());
    }

    #[rstest]
    #[case(
        "<p> <o",
        RdfError::ParseLabel {
            text: input.to_owned(),
            abstract_syntax_tree: "(rdf_diagram (property (predicate (iri (iri_reference (lexical_form)))) (term (iri (iri_reference (lexical_form) (MISSING \">\"))))))".to_owned()
        }
    )]
    fn test_rdf_label_fail(#[case] input: &str, #[case] expected: RdfError) {
        let actual = LabelParser::try_parse(input).unwrap_err();
        assert_eq!(actual, expected);
    }
}
