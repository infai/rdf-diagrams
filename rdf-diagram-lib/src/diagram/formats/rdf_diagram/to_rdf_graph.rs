use itertools::Itertools;
use sophia::api::graph::MutableGraph;
use sophia::api::ns::{rdf, xsd};
use sophia::api::source::TripleSource;
use sophia::api::term::{BnodeId, IriRef, LanguageTag, SimpleTerm, Term as _};
use sophia::inmem::graph::FastGraph;
use sophia::iri::is_relative_iri_ref;
use sophia::turtle::parser::turtle;

use super::{
    BlankNode, Iri, Literal, NumericLiteral, RdfContainer, RdfDiagram, RdfEdge,
    RdfError, RdfLabel, RdfNode, Term,
};
use crate::diagram::Id;
use crate::graph::{Graph, IdTrait, Link};
use crate::{Error, IntoElementError};

pub type RefTriple<'g> = [SimpleTerm<'g>; 3];

type RefProperty<'g> = [SimpleTerm<'g>; 2];

impl TryFrom<&RdfDiagram> for FastGraph {
    type Error = Error;

    fn try_from(diagram: &RdfDiagram) -> Result<Self, Self::Error> {
        let mut graph = FastGraph::default();
        let mut add_turtle_to_graph =
            |statement: &str, id: &Id| -> Result<(), Self::Error> {
                turtle::parse_str(statement)
                    .add_to_graph(&mut graph)
                    .map_err(|error| error.into_element_error(id))?;
                Ok(())
            };

        let mut directives = Vec::new();
        diagram.inner.nodes_iter().try_for_each(|node| {
            node.get_turtle_directives()
                .iter()
                .try_for_each(|&directive| {
                    directives.push(directive);
                    // Just for checking if adding the directive causes an error
                    add_turtle_to_graph(directive, node.id())
                })
        })?;
        diagram.inner.nodes_iter().try_for_each(|node| {
            node.get_turtle_triples().iter().try_for_each(|&triple| {
                // Directives are prepended so that relative and prefixed IRI's in a statement can
                // be parsed
                let statements =
                    format!("{}\n{}", directives.join("\n"), triple);
                add_turtle_to_graph(&statements, node.id())
            })
        })?;

        let node_ids = diagram.inner.nodes_iter().map(IdTrait::id);
        let edge_ids = diagram.inner.edges_iter().map(IdTrait::id);
        let triples = [
            diagram.try_as_triples_for_nodes(node_ids)?,
            diagram.try_as_triples_for_edges(edge_ids)?,
        ]
        .concat();

        for [s, p, o] in triples {
            // todo: Check if still infallible on sophia 0.8
            graph.insert(&s, &p, &o).unwrap();
        }
        Ok(graph)
    }
}

impl TryFrom<RdfDiagram> for FastGraph {
    type Error = Error;

    fn try_from(rdf_diagram: RdfDiagram) -> Result<Self, Self::Error> {
        (&rdf_diagram).try_into()
    }
}

impl RdfDiagram {
    fn try_transform_nodes<'g, F, T>(
        &'g self,
        ids: impl IntoIterator<Item = &'g Id>,
        transform_fn: F,
    ) -> Result<Vec<T>, Error>
    where
        F: Fn(&'g RdfNode) -> Result<Vec<T>, Error>,
    {
        self.inner
            .get_nodes(ids)
            .map(&transform_fn)
            .flatten_ok()
            .collect()
    }

    fn try_as_terms_for_node(&self, id: &Id) -> Result<Vec<SimpleTerm>, Error> {
        if let Some(node) = self.inner.get_node(id) {
            node.try_as_terms(self)
        } else {
            panic!()
        }
    }

    fn try_as_terms_for_nodes<'g>(
        &'g self,
        ids: impl IntoIterator<Item = &'g Id> + 'g,
    ) -> Result<Vec<SimpleTerm<'g>>, Error> {
        self.try_transform_nodes(ids, |node| node.try_as_terms(self))
    }

    fn try_as_properties_for_nodes<'g>(
        &'g self,
        ids: impl IntoIterator<Item = &'g Id> + 'g,
    ) -> Result<Vec<RefProperty<'g>>, Error> {
        self.try_transform_nodes(ids, |node| node.try_as_properties(self))
    }

    fn try_as_triples_for_nodes<'g>(
        &'g self,
        ids: impl IntoIterator<Item = &'g Id> + 'g,
    ) -> Result<Vec<RefTriple<'g>>, Error> {
        self.try_transform_nodes(ids, |node| node.try_as_triples(self))
    }

    fn try_as_triples_for_edges<'g>(
        &'g self,
        ids: impl IntoIterator<Item = &'g Id> + 'g,
    ) -> Result<Vec<RefTriple<'g>>, Error> {
        self.inner
            .get_edges(ids)
            .map(|edge| edge.try_as_triples(self))
            .flatten_ok()
            .collect()
    }
}

impl<'g> RdfNode {
    pub fn try_as_terms(
        &'g self,
        diagram: &'g RdfDiagram,
    ) -> Result<Vec<SimpleTerm<'g>>, Error> {
        let id = self.id();
        match self {
            Self::Container { container, .. } => {
                container.try_as_terms(id, diagram)
            }
            Self::Node { label, .. } => RdfError::into_error_result(
                label.try_as_term(diagram),
                id.to_owned(),
            ),
        }
    }

    pub fn try_as_properties(
        &'g self,
        diagram: &'g RdfDiagram,
    ) -> Result<Vec<RefProperty<'g>>, Error> {
        let id = self.id();
        match self {
            Self::Container { container, .. } => {
                container.try_as_properties(id, diagram)
            }
            Self::Node { label, .. } => label
                .try_as_properties(diagram)
                .map_err(|error| error.into_element_error(id)),
        }
    }

    pub fn try_as_triples(
        &'g self,
        diagram: &'g RdfDiagram,
    ) -> Result<Vec<RefTriple<'g>>, Error> {
        let id = self.id();
        match self {
            RdfNode::Container { container, .. } => {
                container.try_as_triples(id, diagram)
            }
            RdfNode::Node { .. } => Ok(Vec::new()),
        }
    }
}

impl<'g> RdfContainer {
    fn try_as_terms(
        &'g self,
        id: &Id,
        diagram: &'g RdfDiagram,
    ) -> Result<Vec<SimpleTerm<'g>>, Error> {
        match self {
            Self::Terms { containees, .. } => {
                diagram.try_as_terms_for_nodes(containees.iter())
            }
            Self::Properties { .. } => RdfError::into_error_result(
                Err(RdfError::InvalidLinkTarget {
                    target: "properties container".to_owned(),
                }),
                id.to_owned(),
            ),
            Self::Triples { label, .. } => RdfError::into_error_result(
                label.try_as_term(diagram),
                id.to_owned(),
            ),
            Self::Collection { label, .. } => RdfError::into_error_result(
                label.try_as_term(diagram),
                id.to_owned(),
            ),
        }
    }

    fn try_as_properties(
        &'g self,
        id: &Id,
        diagram: &'g RdfDiagram,
    ) -> Result<Vec<RefProperty<'g>>, Error> {
        match self {
            Self::Terms { .. }
            | Self::Triples { .. }
            | Self::Collection { .. } => {
                panic!("Shoud be caught during assignment of containers types")
            }
            Self::Properties { label, containees } => {
                let predicate = label
                    .try_as_term(diagram)
                    .map_err(|error| error.into_element_error(id))?;
                let properties = diagram
                    .try_as_terms_for_nodes(containees.iter())?
                    .into_iter()
                    .map(|object| [predicate.to_owned(), object])
                    .collect();
                Ok(properties)
            }
        }
    }

    pub fn try_as_triples(
        &'g self,
        id: &Id,
        diagram: &'g RdfDiagram,
    ) -> Result<Vec<RefTriple<'g>>, Error> {
        match self {
            RdfContainer::Terms { .. } | RdfContainer::Properties { .. } => {
                Ok(Vec::new())
            }
            Self::Triples { label, containees } => {
                let subject = label
                    .try_as_term(diagram)
                    .map_err(|error| error.into_element_error(id))?;
                let triples = diagram
                    .try_as_properties_for_nodes(containees.iter())?
                    .into_iter()
                    .map(|property| {
                        [
                            subject.to_owned(),
                            property[0].to_owned(),
                            property[1].to_owned(),
                        ]
                    })
                    .collect();
                Ok(triples)
            }
            Self::Collection { containees, .. } => {
                Self::build_collection_triples(containees, diagram)
            }
        }
    }

    fn build_collection_triples(
        containees: &'g Vec<(RdfLabel, Id)>,
        diagram: &'g RdfDiagram,
    ) -> Result<Vec<RefTriple<'g>>, Error> {
        let mut terms = Vec::new();
        for (blank_node, id) in containees {
            let blank_node_term = blank_node
                .try_as_term(diagram)
                .map_err(|error| error.into_element_error(id))?;
            let item_terms = diagram.try_as_terms_for_node(id)?;
            terms.push((blank_node_term, item_terms));
        }

        let rest_nodes: Vec<_> = terms
            .iter()
            .cloned()
            .map(|(blank_node, _)| blank_node)
            .chain(std::iter::once(rdf::nil.into_term()))
            .collect();

        let mut triples = Vec::new();
        for (blank_node_term, item_terms) in terms {
            item_terms.iter().for_each(|item_term| {
                triples.push([
                    blank_node_term.to_owned(),
                    rdf::first.into_term(),
                    item_term.to_owned(),
                ])
            })
        }
        for rest_node_pair in rest_nodes.windows(2) {
            triples.push([
                rest_node_pair[0].to_owned(),
                rdf::rest.into_term(),
                rest_node_pair[1].to_owned(),
            ]);
        }

        Ok(triples)
    }
}

impl<'g> RdfLabel {
    fn try_as_term(
        &'g self,
        diagram: &'g RdfDiagram,
    ) -> Result<SimpleTerm<'g>, RdfError> {
        match self {
            Self::Property { .. } | Self::TurtleDoc { .. } => {
                panic!("Use later for turtle as qouted triple")
            }
            Self::Term(term) => term.try_as_term(diagram),
        }
    }

    fn try_as_properties(
        &'g self,
        diagram: &'g RdfDiagram,
    ) -> Result<Vec<RefProperty<'g>>, RdfError> {
        match self {
            Self::Property { predicate, objects } => {
                let predicate_term = predicate.try_as_term(diagram)?;
                objects
                    .iter()
                    .map(|object| object.try_as_term(diagram))
                    .map_ok(|object_term| {
                        [predicate_term.to_owned(), object_term]
                    })
                    .collect()
            }
            Self::Term(_) | Self::TurtleDoc { .. } => {
                panic!("Should never happen")
            }
        }
    }
}

impl<'g> Term {
    fn try_as_term(
        &'g self,
        diagram: &'g RdfDiagram,
    ) -> Result<SimpleTerm<'g>, RdfError> {
        match self {
            Term::Iri(iri) => iri.try_as_term(diagram),
            Term::BlankNode(blank_node) => blank_node.try_as_term(),
            Term::Literal(literal) => literal.try_as_term(diagram),
        }
    }
}

impl<'g> Iri {
    fn try_as_term(
        &'g self,
        diagram: &'g RdfDiagram,
    ) -> Result<SimpleTerm<'g>, RdfError> {
        match self {
            Self::IriReference { iri } => {
                let iri_ref_string =
                    match (is_relative_iri_ref(iri), diagram.base()) {
                        (true, Some(base)) => format!("{}{}", base, iri),
                        _ => iri.to_string(),
                    };
                let iri_ref = IriRef::new_unchecked(iri_ref_string.into());
                Ok(SimpleTerm::Iri(iri_ref))
            }
            Self::PrefixedName {
                pn_prefix,
                pn_local,
            } => {
                if let Some(ns) = diagram.get_namespace(pn_prefix) {
                    let iri_ref = IriRef::new_unchecked(
                        format!("{}{}", ns, pn_local).into(),
                    );
                    Ok(SimpleTerm::Iri(iri_ref))
                } else {
                    Err(RdfError::MissingPrefix(pn_prefix.to_string()))
                }
            }
        }
    }
}

impl BlankNode {
    fn try_as_term(&self) -> Result<SimpleTerm, RdfError> {
        let bnode_id: &str = match self {
            BlankNode::Anon { bnode_id }
            | BlankNode::BlankNodeLabel { bnode_id } => bnode_id,
        };
        Ok(BnodeId::new(bnode_id).unwrap().into_term())
    }
}

impl Literal {
    fn try_as_term<'g>(
        &'g self,
        diagram: &'g RdfDiagram,
    ) -> Result<SimpleTerm<'g>, RdfError> {
        let literal = match self {
            Self::Rdf {
                lexical_form,
                lang_tag: Some(lang_tag),
                ..
            } => SimpleTerm::LiteralLanguage(
                lexical_form.as_str().into(),
                LanguageTag::new_unchecked(lang_tag.as_str().into()),
            ),
            Self::Rdf {
                lexical_form,
                lang_tag: None,
                datatype,
            } => {
                let dt = match datatype.as_ref() {
                    Some(dt_inner) => dt_inner.try_as_term(diagram).map(
                        |simple_term: SimpleTerm| {
                            IriRef::new_unchecked(
                                simple_term.iri().unwrap().to_string().into(),
                            )
                        },
                    )?,
                    None => xsd::string.iri().unwrap(),
                };
                SimpleTerm::LiteralDatatype(lexical_form.as_str().into(), dt)
            }

            Self::Numeric(numeric_literal) => {
                let (lexical_form, dt) = match numeric_literal {
                    NumericLiteral::Decimal { lexical_form } => {
                        (lexical_form, xsd::decimal.iri().unwrap())
                    }
                    NumericLiteral::Double { lexical_form } => {
                        (lexical_form, xsd::double.iri().unwrap())
                    }
                    NumericLiteral::Integer { lexical_form } => {
                        (lexical_form, xsd::integer.iri().unwrap())
                    }
                };
                SimpleTerm::LiteralDatatype(lexical_form.as_str().into(), dt)
            }

            Self::Boolean { lexical_form } => SimpleTerm::LiteralDatatype(
                lexical_form.as_str().into(),
                xsd::boolean.iri().unwrap(),
            ),
        };

        Ok(literal)
    }
}

impl<'g> RdfEdge {
    pub fn try_as_triples(
        &'g self,
        diagram: &'g RdfDiagram,
    ) -> Result<Vec<RefTriple<'g>>, Error> {
        let subjects = diagram.try_as_terms_for_node(self.source())?;
        let predicate = self
            .label
            .try_as_term(diagram)
            .map_err(|error| error.into_element_error(self.id()))?;
        let objects = diagram.try_as_terms_for_node(self.target())?;
        let triples = subjects
            .iter()
            .cartesian_product(objects.iter())
            .map(|(subject, object)| {
                [subject.to_owned(), predicate.to_owned(), object.to_owned()]
            })
            .collect();
        Ok(triples)
    }
}
