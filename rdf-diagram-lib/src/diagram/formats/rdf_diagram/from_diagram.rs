use std::collections::{HashSet, VecDeque};

use super::label_parser::LabelParser;
use super::{
    DisRdfContainer, RdfContainer, RdfDiagram, RdfEdge, RdfError, RdfLabel,
    RdfNode, Term,
};
use crate::diagram::{Container, Diagram, DiagramNode, Edge, Id, Label};
use crate::graph::{Graph, IdTrait, Link};
use crate::{Error, IntoElementError};

impl TryFrom<&Diagram> for RdfDiagram {
    type Error = Error;

    fn try_from(diagram: &Diagram) -> Result<Self, Self::Error> {
        let mut rdf_diagram = RdfDiagram::new();
        let mut nodes: VecDeque<_> = diagram.nodes_iter().collect();

        while let Some(node) = nodes.pop_front() {
            // None if any containee of RdfNode is not yet added to rdf_diagram
            let containees: Option<Vec<&RdfNode>> = node
                .containee_ids_iter()
                .map(|id| rdf_diagram.inner.get_node(id))
                .collect();
            match containees {
                Some(containees) => node
                    .label()
                    .map(String::as_str)
                    .map(LabelParser::try_parse)
                    .transpose()
                    .and_then(|rdf_label| {
                        RdfNode::try_from_diagram_node(
                            node, rdf_label, containees,
                        )
                    })
                    .and_then(|node| rdf_diagram.add_node(node))
                    .map_err(|error| error.into_element_error(node.id()))?,
                None => {
                    nodes.push_back(node);
                }
            };
        }

        for edge in diagram.edges_iter() {
            let rdf_edge: RdfEdge =
                edge.try_into().map_err(|error: RdfError| {
                    error.into_element_error(edge.id())
                })?;
            rdf_diagram
                .inner
                .add_edge(rdf_edge)
                .map_err(|error| error.into_element_error(edge.id()))?;
        }

        Ok(rdf_diagram)
    }
}

impl TryFrom<Diagram> for RdfDiagram {
    type Error = Error;

    fn try_from(diagram: Diagram) -> Result<Self, Self::Error> {
        (&diagram).try_into()
    }
}

impl RdfNode {
    fn try_from_diagram_node(
        node: &DiagramNode,
        rdf_label: Option<RdfLabel>,
        containee_nodes: Vec<&RdfNode>,
    ) -> Result<RdfNode, RdfError> {
        let rdf_node = match node {
            DiagramNode::AnonContainer {
                id,
                containees: containees_ids,
                ..
            } => Self::Container {
                id: id.clone(),
                container: RdfContainer::try_from_container(
                    rdf_label,
                    containees_ids,
                    containee_nodes,
                )?,
            },
            DiagramNode::Container {
                id,
                containees: containees_ids,
                ..
            } => Self::Container {
                id: id.clone(),
                container: RdfContainer::try_from_container(
                    rdf_label,
                    containees_ids,
                    containee_nodes,
                )?,
            },
            DiagramNode::Node { id, .. } => Self::Node {
                id: id.clone(),
                label: rdf_label.expect("DiagramNode always has a label"),
            },
        };
        Ok(rdf_node)
    }

    fn valid_containers(&self) -> HashSet<DisRdfContainer> {
        match self {
            Self::Container { container, .. } => container.valid_containers(),
            Self::Node { label, .. } => label.valid_containers(),
        }
    }
}

impl RdfContainer {
    /// # Panics
    ///
    /// Panics if the valid_container_for_label_and_containees function has a bug.
    fn try_from_container(
        label: Option<RdfLabel>,
        containees: &[Id],
        containee_nodes: Vec<&RdfNode>,
    ) -> Result<Self, RdfError> {
        let valid_containers = Self::valid_container_for_label_and_containees(
            label.as_ref(),
            containee_nodes.clone(),
        );
        let valid_container = match valid_containers.len() {
            0 => Err(RdfError::AssignRdfContainer(format!(
                "label: {:?}, containees: {:?}",
                &label, &containee_nodes
            ))),
            1 => Ok(valid_containers.iter().next().unwrap()),
            _ => panic!(),
        }?;

        let containees = containees.to_owned();
        let container = match (valid_container, label) {
            (DisRdfContainer::Terms, None) => Self::Terms { containees },
            (DisRdfContainer::Terms, Some(_)) => panic!(),
            (DisRdfContainer::Properties, Some(label)) => {
                Self::Properties { label, containees }
            }
            (DisRdfContainer::Properties, None) => panic!(),
            (DisRdfContainer::Triples, Some(label)) => {
                Self::Triples { label, containees }
            }
            (DisRdfContainer::Triples, None) => panic!(),
            (DisRdfContainer::Collection, Some(label)) => Self::Collection {
                label: label.clone(),
                containees: Self::convert_containees_for_collection(
                    label, containees,
                ),
            },
            (DisRdfContainer::Collection, None) => panic!(),
        };
        Ok(container)
    }

    /// The set of containers, which can contain self
    fn valid_containers(&self) -> HashSet<DisRdfContainer> {
        let valid_containers = match self {
            Self::Properties { .. } => vec![DisRdfContainer::Triples],
            Self::Terms { .. }
            | Self::Triples { .. }
            | Self::Collection { .. } => {
                vec![
                    DisRdfContainer::Terms,
                    DisRdfContainer::Properties,
                    DisRdfContainer::Collection,
                ]
            }
        };
        valid_containers.into_iter().collect()
    }

    fn valid_container_for_label_and_containees(
        label: Option<&RdfLabel>,
        containee_nodes: Vec<&RdfNode>,
    ) -> HashSet<DisRdfContainer> {
        let valid_containers_for_containees =
            Self::valid_containers_for_containees(containee_nodes);
        let valid_containers_for_label =
            RdfLabel::valid_containers_for_label(label);
        valid_containers_for_containees
            .intersection(&valid_containers_for_label)
            .cloned()
            .collect()
    }

    fn valid_containers_for_containees(
        containees: Vec<&RdfNode>,
    ) -> HashSet<DisRdfContainer> {
        containees
            .into_iter()
            .map(RdfNode::valid_containers)
            .reduce(|acc, set| acc.intersection(&set).cloned().collect())
            .unwrap_or_default()
    }

    fn convert_containees_for_collection(
        label: RdfLabel,
        containees: Vec<Id>,
    ) -> Vec<(RdfLabel, Id)> {
        let mut result = Vec::with_capacity(containees.len());
        let mut containees_iter = containees.into_iter();
        if let Some(containee) = containees_iter.next() {
            result.push((label, containee));
            for containee in containees_iter {
                let blank_node = RdfLabel::new_anon_blank_node();
                result.push((blank_node, containee));
            }
        };
        result
    }
}

impl RdfLabel {
    /// Returns the set of containers which can contain self
    fn valid_containers(&self) -> HashSet<DisRdfContainer> {
        let valid_containers = match self {
            Self::Term(term) => match term {
                Term::Iri(_) | Term::BlankNode(_) | Term::Literal(_) => {
                    vec![
                        DisRdfContainer::Terms,
                        DisRdfContainer::Properties,
                        DisRdfContainer::Collection,
                    ]
                }
            },
            Self::Property { .. } => vec![DisRdfContainer::Triples],
            Self::TurtleDoc { .. } => vec![],
        };
        valid_containers.into_iter().collect()
    }

    /// Returns the set of containers for label
    fn valid_containers_for_label(
        label: Option<&RdfLabel>,
    ) -> HashSet<DisRdfContainer> {
        let valid_containers = match label {
            Some(label) => match label {
                RdfLabel::Term(term) => match term {
                    Term::Iri(_) => vec![
                        DisRdfContainer::Triples,
                        DisRdfContainer::Properties,
                    ],
                    Term::BlankNode(_) => vec![
                        DisRdfContainer::Triples,
                        DisRdfContainer::Collection,
                    ],
                    Term::Literal(_) => vec![],
                },
                RdfLabel::Property { .. } | RdfLabel::TurtleDoc { .. } => {
                    vec![]
                }
            },
            None => vec![DisRdfContainer::Terms],
        };
        valid_containers.into_iter().collect()
    }
}

impl TryInto<RdfEdge> for &Edge<String> {
    type Error = RdfError;

    fn try_into(self) -> Result<RdfEdge, Self::Error> {
        let rdf_label = LabelParser::try_parse(self.label.as_str())?;
        if !matches!(rdf_label, RdfLabel::Term(Term::Iri(_))) {
            return Err(RdfError::InvalidPredicate);
        }
        Ok(RdfEdge::new(
            self.id().clone(),
            rdf_label,
            self.source().clone(),
            self.target().clone(),
        ))
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::PathBuf;

    use insta;
    use rstest::rstest;

    use super::*;
    use crate::diagram::formats::Deserialize;
    use crate::formats::drawio::DrawioDiagram;
    use crate::tests::set_snapshot_path;

    #[rstest]
    fn from(
        #[files("tests/rdf-diagram/*.drawio")] path: PathBuf,
    ) -> anyhow::Result<()> {
        let xml = fs::read_to_string(&path)?;
        let rdf_diagram: RdfDiagram =
            Diagram::from_str::<DrawioDiagram>(&xml)?.try_into()?;
        // FastGraph has a random ordering
        set_snapshot_path!(path);
        insta::with_settings!({filters => vec![(
            r#"bnode_id: "[0-9a-fA-F-]{36}""#,
            "bnode_id: \"UUID PLACEHOLDER\""
        )]},
            {insta::assert_debug_snapshot!(rdf_diagram);
        });
        Ok(())
    }
}
