use std::fmt::{Display, Formatter};
use std::hash::Hash;
use std::io::{BufReader, Read};

use chrono::{DateTime, SecondsFormat, Utc};
use ordered_float::OrderedFloat;
use quick_xml::de::{from_reader, from_str};
use quick_xml::se::Serializer;
use serde::Deserializer;

use super::serialize_params::Indentation;
use super::{Config, Deserialize, Serialize};
use crate::diagram::Orientation;
use crate::Error;

mod from_diagram;
mod to_diagram;

/// A draw.io diagram.
#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[serde(rename = "mxfile")]
pub struct DrawioDiagram {
    diagram: Diagram,
    #[serde(rename = "@host")]
    host: String,
    #[serde(
        rename = "@modified",
        deserialize_with = "date_time_from_str",
        serialize_with = "date_time_to_str"
    )]
    modified: DateTime<Utc>,
    #[serde(rename = "@agent")]
    agent: String,
    #[serde(rename = "@version")]
    version: String,
    #[serde(rename = "@etag")]
    etag: String,
}

impl Deserialize for DrawioDiagram {
    fn from_reader<DrawioDiagram, B: Read>(reader: B) -> Result<Self, Error> {
        let buf_reader = BufReader::new(reader);
        Ok(from_reader(buf_reader)?)
    }

    fn from_str<DrawioDiagram>(str: &str) -> Result<Self, Error> {
        Ok(from_str(str)?)
    }
}

impl Serialize for DrawioDiagram {
    type Config = DrawioConfig;

    fn stringify_with_config(
        &self,
        config: &Self::Config,
    ) -> Result<String, Error> {
        let mut writer = String::new();
        let mut serializer = Serializer::new(&mut writer);
        serializer.indent(' ', config.indentation);
        serde::Serialize::serialize(self, serializer)?;
        Ok(writer)
    }
}

impl Display for DrawioDiagram {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let string = self.stringify().map_err(|_| std::fmt::Error)?;
        writeln!(f, "{}", &string)?;
        Ok(())
    }
}

impl Hash for DrawioDiagram {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.diagram.hash(state);
        self.host.hash(state);
        self.agent.hash(state);
        self.version.hash(state);
    }
}

impl PartialEq for DrawioDiagram {
    fn eq(&self, other: &Self) -> bool {
        self.diagram == other.diagram
            && self.host == other.host
            && self.agent == other.agent
            && self.version == other.version
    }
}

/// Configuration for serializing to a [draw.io diagram](DrawioDiagram).
pub struct DrawioConfig {
    pub indentation: Indentation,
}

impl Config for DrawioConfig {
    type Format = DrawioDiagram;
}

impl Default for DrawioConfig {
    fn default() -> Self {
        DrawioConfig { indentation: 4 }
    }
}

#[derive(Debug, serde::Deserialize, Hash, serde::Serialize, PartialEq, Eq)]
struct Diagram {
    #[serde(rename = "mxGraphModel")]
    mx_graph_model: MxGraphModel,
}

#[derive(Debug, serde::Deserialize, Hash, serde::Serialize, PartialEq, Eq)]
struct MxGraphModel {
    root: Root,
}

#[derive(Debug, serde::Deserialize, Hash, serde::Serialize, PartialEq, Eq)]
struct Root {
    #[serde(rename = "$value")]
    cells: Vec<Cell>,
}

#[derive(Debug, serde::Deserialize, Hash, serde::Serialize, PartialEq, Eq)]
enum Cell {
    UserObject(UserObject),
    #[serde(rename = "mxCell")]
    MxCell(MxCell),
}

#[derive(PartialEq, Eq)]
enum CellKind {
    Node,
    Edge,
    RootNode,
    Layer,
}

#[derive(Debug, serde::Deserialize, Hash, serde::Serialize, PartialEq, Eq)]
struct UserObject {
    #[serde(rename = "@id")]
    id: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(rename = "@label", default)]
    label: String,
    #[serde(rename = "mxCell")]
    mx_cell: MxCell,
}

#[derive(
    Debug, Default, serde::Deserialize, Hash, serde::Serialize, PartialEq, Eq,
)]
struct MxCell {
    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(rename = "@id", default)]
    id: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(rename = "@value", default)]
    value: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(rename = "@style", default)]
    style: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(rename = "@parent", default)]
    parent: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(rename = "@source", default)]
    source: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(rename = "@target", default)]
    target: String,
    #[serde(serialize_with = "serialize_bool")]
    #[serde(deserialize_with = "deserialize_bool")]
    #[serde(skip_serializing_if = "is_false")]
    #[serde(rename = "@vertex", default)]
    is_node: bool,
    #[serde(serialize_with = "serialize_bool")]
    #[serde(deserialize_with = "deserialize_bool")]
    #[serde(skip_serializing_if = "is_false")]
    #[serde(rename = "@edge", default)]
    is_edge: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "mxGeometry")]
    geometry: Option<MxGeometry>,
}

#[derive(
    Debug, Default, serde::Deserialize, Hash, serde::Serialize, PartialEq, Eq,
)]
struct MxGeometry {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "@x", default)]
    x: Option<OrderedFloat<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "@y", default)]
    y: Option<OrderedFloat<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "@width")]
    width: Option<OrderedFloat<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "@height")]
    height: Option<OrderedFloat<f64>>,
    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(rename = "@as")]
    as_field: String,
    #[serde(serialize_with = "serialize_bool")]
    #[serde(deserialize_with = "deserialize_bool")]
    #[serde(skip_serializing_if = "is_false")]
    #[serde(rename = "@relative", default)]
    relative: bool,
}

fn serialize_bool<S>(value: &bool, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    let s = if *value { "1" } else { "0" };
    serializer.serialize_str(s)
}

fn deserialize_bool<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let s: String = serde::Deserialize::deserialize(deserializer)?;
    match s.as_str() {
        "1" => Ok(true),
        "0" => Ok(false),
        _ => Err(serde::de::Error::unknown_variant(&s, &["1", "0"])),
    }
}

fn is_false(val: &bool) -> bool {
    !*val
}

fn date_time_from_str<'de, D>(
    deserializer: D,
) -> Result<DateTime<Utc>, D::Error>
where
    D: Deserializer<'de>,
{
    let s = <String as serde::Deserialize>::deserialize(deserializer)?;
    DateTime::parse_from_rfc3339(&s)
        .map_err(|_| {
            serde::de::Error::invalid_value(
                serde::de::Unexpected::Str(&s),
                &"an RFC 3339 formatted string",
            )
        })
        .map(|dt| dt.with_timezone(&Utc))
}

fn date_time_to_str<S>(
    date: &DateTime<Utc>,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    let s = date.to_rfc3339_opts(SecondsFormat::Millis, true);
    serializer.serialize_str(&s)
}

impl Cell {
    fn id(&self) -> String {
        match self {
            Cell::MxCell(MxCell { id, .. })
            | Cell::UserObject(UserObject { id, .. }) => id.to_owned(),
        }
    }

    fn mx_cell(&self) -> &MxCell {
        match self {
            Cell::MxCell(mx_cell)
            | Cell::UserObject(UserObject { mx_cell, .. }) => mx_cell,
        }
    }

    fn mx_cell_mut(&mut self) -> &mut MxCell {
        match self {
            Cell::MxCell(mx_cell)
            | Cell::UserObject(UserObject { mx_cell, .. }) => mx_cell,
        }
    }

    fn style(&self) -> &str {
        &self.mx_cell().style
    }

    fn parent(&self) -> &str {
        &self.mx_cell().parent
    }

    fn source(&self) -> &str {
        &self.mx_cell().source
    }

    fn target(&self) -> &str {
        &self.mx_cell().target
    }

    fn mx_geometry(&self) -> Option<&MxGeometry> {
        self.mx_cell().geometry.as_ref()
    }

    fn kind(&self) -> CellKind {
        if self.parent().is_empty() {
            CellKind::RootNode
        } else if self.parent() == "0" {
            CellKind::Layer
        } else if self.mx_cell().is_edge {
            CellKind::Edge
        } else {
            CellKind::Node
        }
    }

    fn is_container(&self) -> bool {
        self.style().contains("swimlane")
    }

    fn is_anon_container(&self) -> bool {
        self.style().contains("swimlane")
            && self.style().contains("startSize=0")
    }

    fn is_html_encoded(&self) -> bool {
        self.style().contains("html=1")
    }

    fn orientation(&self) -> Orientation {
        match self.style().contains("horizontal=1") {
            true => Orientation::Horizontal,
            false => Orientation::Vertical,
        }
    }

    fn containees_orientation(&self) -> Orientation {
        match self.style().contains("horizontalStack=1") {
            true => Orientation::Horizontal,
            false => Orientation::Vertical,
        }
    }
}

impl MxGeometry {
    fn new_edge() -> MxGeometry {
        MxGeometry {
            x: None,
            y: None,
            width: None,
            height: None,
            as_field: "geometry".to_owned(),
            relative: true,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::PathBuf;

    use insta;
    use rstest::rstest;

    use super::*;
    use crate::tests::set_snapshot_path;

    #[rstest]
    fn parse_drawio_xml(
        #[files("tests/**/*.drawio")] path: PathBuf,
    ) -> anyhow::Result<()> {
        let xml = fs::read_to_string(&path)?;
        let diagram = DrawioDiagram::from_str::<DrawioDiagram>(&xml)?;
        // use pretty_assertions::assert_eq;
        // assert_eq!(xml, format!("{}", drawio_diagram));
        set_snapshot_path!(path);
        insta::with_settings!({filters => vec![
            (
            r#"modified=".+?""#,
            "modified=\"DATE PLACEHOLDER\""
            ),
            (
            r#"etag="[0-9]+?""#,
            "etag=\"ETAG PLACEHOLDER\""
            )
        ]},
            {insta::assert_snapshot!(diagram);
        });
        Ok(())
    }
}
