use std::io::{Read, Write};

use super::Diagram;
use crate::{Error, Result};
/// Handles parsing and conversion of draw.io diagrams into and from a [Diagram],
pub mod drawio;
/// A [Diagram] which conforms to the [Specification](https://infai.gitlab.io/rdf-diagram-framework/docs/).
pub mod rdf_diagram;

/// Common configuration options for serialization formats using the [Config] trait.
pub mod serialize_params {
    pub enum Mode {
        Pretty,
        Stream,
    }
    pub type Indentation = usize;
}

/// An associated trait for [formats](self) that can be [serialized](Serialize).
pub trait Config: Default {
    type Format: Serialize;
}

/// Formats that can be deserialized into a [Diagram].
pub trait Deserialize: Sized {
    fn from_str<Format>(str: &str) -> Result<Self>
    where
        Format: TryInto<Diagram, Error = Error> + Deserialize;

    fn from_reader<Format, B: Read>(mut reader: B) -> Result<Self>
    where
        Format: TryInto<Diagram, Error = Error> + Deserialize,
    {
        let mut buffer = String::new();
        reader.read_to_string(&mut buffer)?;
        Self::from_str::<Format>(&buffer)
    }
}

/// Formats that can be serialized.
pub trait Serialize: Sized {
    type Config: Config;

    fn serialize<W: Write>(&self, write: &mut W) -> Result<()> {
        self.serialize_with_config(&Self::Config::default(), write)
    }

    fn serialize_with_config<W: Write>(
        &self,
        config: &Self::Config,
        writer: &mut W,
    ) -> Result<()> {
        writeln!(writer, "{}", self.stringify_with_config(config)?)?;
        Ok(())
    }

    fn stringify(&self) -> Result<String> {
        self.stringify_with_config(&Self::Config::default())
    }

    fn stringify_with_config(&self, config: &Self::Config) -> Result<String>;
}

impl<T> Deserialize for T
where
    T: TryFrom<Diagram, Error = Error>,
{
    fn from_str<Format>(str: &str) -> Result<Self>
    where
        Format: TryInto<Diagram, Error = Error> + Deserialize,
    {
        let input = Format::from_str::<Format>(str)?;

        let diagram: Diagram = input.try_into()?;
        TryFrom::try_from(diagram)
    }
}
