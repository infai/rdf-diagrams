use std::borrow::Borrow;
use std::fmt::{Debug, Display};
use std::hash::Hash;

use hashlink::LinkedHashMap;

pub trait IdTrait<Id> {
    fn id(&self) -> &Id;
}

pub trait Link<Id>: IdTrait<Id> {
    fn source(&self) -> &Id;

    fn target(&self) -> &Id;
}

#[derive(thiserror::Error, Debug)]
pub enum AddEdgeError {
    #[error("Source node '{source_id}' not found for edge '{edge_id}'")]
    SourceNotFound { edge_id: String, source_id: String },
    #[error("Target node '{target_id}' not found for edge '{edge_id}'")]
    TargetNotFound { edge_id: String, target_id: String },
}

pub trait Graph {
    type Id: Display + Debug;
    type N: IdTrait<Self::Id>;
    type E: Link<Self::Id>;

    fn add_edge_unchecked(&mut self, edge: impl Into<Self::E>);

    fn add_edge(
        &mut self,
        edge: impl Into<Self::E>,
    ) -> Result<(), AddEdgeError> {
        let edge = edge.into();
        let id = edge.id();
        let source = edge.source();
        let target = edge.target();

        self.get_node(source).ok_or(AddEdgeError::SourceNotFound {
            edge_id: id.to_string(),
            source_id: source.to_string(),
        })?;
        self.get_node(target).ok_or(AddEdgeError::TargetNotFound {
            edge_id: id.to_string(),
            target_id: source.to_string(),
        })?;

        self.add_edge_unchecked(edge);
        Ok(())
    }

    fn _add_edges(
        &mut self,
        edges: impl IntoIterator<Item = impl Into<Self::E>>,
    ) -> Result<(), AddEdgeError> {
        for edge in edges {
            self.add_edge(edge)?;
        }
        Ok(())
    }

    fn add_node(&mut self, node: impl Into<Self::N>);

    fn add_nodes(
        &mut self,
        nodes: impl IntoIterator<Item = impl Into<Self::N>>,
    ) {
        for node in nodes {
            self.add_node(node);
        }
    }

    fn get_edge(&self, id: impl Borrow<Self::Id>) -> Option<&Self::E>;

    fn get_edges(
        &self,
        ids: impl IntoIterator<Item = impl Borrow<Self::Id>>,
    ) -> impl Iterator<Item = &Self::E> {
        ids.into_iter().flat_map(|id| self.get_edge(id))
    }

    fn get_node(&self, id: impl Borrow<Self::Id>) -> Option<&Self::N>;

    fn get_nodes(
        &self,
        ids: impl IntoIterator<Item = impl Borrow<Self::Id>>,
    ) -> impl Iterator<Item = &Self::N> {
        ids.into_iter().flat_map(|id| self.get_node(id))
    }

    fn get_node_mut(
        &mut self,
        id: impl Borrow<Self::Id>,
    ) -> Option<&mut Self::N>;

    fn edges_iter(&self) -> impl Iterator<Item = &Self::E>;

    fn nodes_iter(&self) -> impl Iterator<Item = &Self::N>;
}

#[derive(Debug, Clone)]
pub struct HashMapGraph<Id, N, E>
where
    Id: Eq + Hash + Clone,
{
    nodes: LinkedHashMap<Id, N>,
    edges: LinkedHashMap<Id, E>,
}

impl<Id, N, E> HashMapGraph<Id, N, E>
where
    Id: Eq + Hash + Clone,
{
    pub fn new() -> Self {
        HashMapGraph {
            nodes: LinkedHashMap::new(),
            edges: LinkedHashMap::new(),
        }
    }
}

impl<Id, N, E> Default for HashMapGraph<Id, N, E>
where
    Id: Eq + Hash + Clone,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<Id, N, E> Graph for HashMapGraph<Id, N, E>
where
    Id: Eq + Hash + Clone + Debug + Display,
    N: IdTrait<Id> + Debug,
    E: Link<Id>,
{
    type E = E;
    type Id = Id;
    type N = N;

    fn add_edge_unchecked(&mut self, edge: impl Into<Self::E>) {
        let edge = edge.into();
        let id = edge.id().clone();
        self.edges.insert(id, edge);
    }

    fn add_node(&mut self, node: impl Into<N>) {
        let node = node.into();
        let id = node.id().clone();
        self.nodes.insert(id, node);
    }

    fn get_edge(&self, id: impl Borrow<Self::Id>) -> Option<&Self::E> {
        self.edges.get(id.borrow())
    }

    fn get_node(&self, id: impl Borrow<Self::Id>) -> Option<&Self::N> {
        self.nodes.get(id.borrow())
    }

    fn get_node_mut(
        &mut self,
        id: impl Borrow<Self::Id>,
    ) -> Option<&mut Self::N> {
        self.nodes.get_mut(id.borrow())
    }

    fn edges_iter(&self) -> impl Iterator<Item = &Self::E> {
        self.edges.values()
    }

    fn nodes_iter(&self) -> impl Iterator<Item = &Self::N> {
        self.nodes.values()
    }
}
