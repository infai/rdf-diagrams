use core::fmt::Debug;
use std::io::Write;

use derive_more::Display;
use euclid::default::Rect;
use itertools::Either;
use strum_macros::EnumString;

use crate::diagram::formats::{Config, Deserialize, Serialize};
use crate::graph::{Graph, HashMapGraph, IdTrait, Link};
use crate::Error;

/// Formats that can be converted from and/or into a [Diagram].
pub mod formats;

#[derive(thiserror::Error, Debug)]
pub(super) enum AddContaineeError {
    #[error(
        "Could not add containee '{container_id}' to not existing container \
         '{container_id}'"
    )]
    ContainerNotFound {
        container_id: String,
        containee_id: String,
    },
    #[error(
        "Could not add not existing containee '{container_id}' to container \
         '{container_id}'"
    )]
    ContaineeNotFound {
        container_id: String,
        containee_id: String,
    },
}

trait Containers: Graph
where
    Self::N: IdTrait<Self::Id> + Container<Id = Self::Id>,
    Self::E: IdTrait<Self::Id>,
{
    fn add_containee(
        &mut self,
        container_id: &Self::Id,
        containee_id: Self::Id,
    ) -> Result<(), AddContaineeError>;
}

impl<G> Containers for G
where
    G: Graph,
    Self::N: IdTrait<Self::Id> + Container<Id = Self::Id>,
    Self::E: IdTrait<Self::Id>,
{
    fn add_containee(
        &mut self,
        container_id: &Self::Id,
        containee_id: Self::Id,
    ) -> Result<(), AddContaineeError> {
        self.get_node(&containee_id).ok_or(
            AddContaineeError::ContaineeNotFound {
                container_id: container_id.to_string(),
                containee_id: containee_id.to_string(),
            },
        )?;
        let container = self.get_node_mut(container_id).ok_or(
            AddContaineeError::ContainerNotFound {
                container_id: container_id.to_string(),
                containee_id: containee_id.to_string(),
            },
        )?;
        container.add_containee(containee_id);
        Ok(())
    }
}

trait Container {
    type Id;

    fn containee_ids_iter(&self) -> impl Iterator<Item = &Self::Id>;

    fn add_containee(&mut self, containee: impl Into<Self::Id>);
}

trait Label {
    type Label;
    fn label(&self) -> Option<&Self::Label>;
}

/// A representation of a
/// [diagram](https://infai.gitlab.io/rdf-diagram-framework/docs/diagram.html#syntax).
///
/// This serves as a intermediate format that all formats can be transformed from and/or into.
pub type Diagram = HashMapGraph<Id, DiagramNode, Edge<String>>;

impl Diagram {
    pub fn serialize_with_config<'a, F, C, W>(
        &'a self,
        config: &C,
        writer: &mut W,
    ) -> crate::Result<()>
    where
        F: Serialize<Config = C> + TryFrom<&'a Diagram, Error = Error>,
        C: Config<Format = F>,
        W: Write,
    {
        let diagram_format: F = self.try_into()?;
        F::serialize_with_config(&diagram_format, config, writer)?;
        Ok(())
    }

    pub fn stringify_with_config<'a, F, C>(
        &'a self,
        config: &C,
    ) -> crate::Result<String>
    where
        F: Serialize<Config = C> + TryFrom<&'a Diagram, Error = Error>,
        C: Config<Format = F> + Default,
    {
        let diagram_format: F = self.try_into()?;
        F::stringify_with_config(&diagram_format, config)
    }
}

impl Deserialize for Diagram {
    fn from_str<Format>(str: &str) -> crate::Result<Self>
    where
        Format: TryInto<Diagram, Error = Error> + Deserialize,
    {
        let input = Format::from_str::<Format>(str)?;
        input.try_into()
    }
}

/// Identifies an element in [Diagram].
#[derive(Debug, Display, Default, Clone, PartialEq, Eq, Hash)]
pub struct Id(String);

impl IdTrait<Id> for Id {
    fn id(&self) -> &Id {
        self
    }
}

impl From<&str> for Id {
    fn from(str: &str) -> Self {
        Id(str.to_owned())
    }
}

impl From<String> for Id {
    fn from(string: String) -> Self {
        Id(string)
    }
}

impl AsRef<str> for Id {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl From<&Id> for String {
    fn from(id: &Id) -> Self {
        id.to_string()
    }
}

#[derive(Clone, Debug)]
pub enum DiagramNode {
    AnonContainer {
        id: Id,
        geometry: Rect<i32>,
        containees_orientation: Orientation,
        containees: Vec<Id>,
    },
    Container {
        id: Id,
        geometry: Rect<i32>,
        label: String,
        orientation: Orientation,
        containees_orientation: Orientation,
        containees: Vec<Id>,
    },
    Node {
        id: Id,
        geometry: Rect<i32>,
        label: String,
    },
}

impl IdTrait<Id> for DiagramNode {
    fn id(&self) -> &Id {
        match self {
            DiagramNode::AnonContainer { id, .. }
            | DiagramNode::Container { id, .. }
            | DiagramNode::Node { id, .. } => id,
        }
    }
}

impl Label for DiagramNode {
    type Label = String;

    fn label(&self) -> Option<&Self::Label> {
        match self {
            DiagramNode::AnonContainer { .. } => None,
            DiagramNode::Container { label, .. }
            | DiagramNode::Node { label, .. } => Some(label),
        }
    }
}

impl Container for DiagramNode {
    type Id = Id;

    fn containee_ids_iter(&self) -> impl Iterator<Item = &Id> {
        match self {
            Self::AnonContainer { containees, .. }
            | Self::Container { containees, .. } => {
                Either::Left(containees.iter())
            }
            Self::Node { .. } => Either::Right(std::iter::empty()),
        }
    }

    fn add_containee(&mut self, containee: impl Into<Id>) {
        match self {
            Self::AnonContainer { containees, .. }
            | Self::Container { containees, .. } => {
                containees.push(containee.into())
            }
            Self::Node { .. } => (),
        }
    }
}

#[derive(Clone, Debug, strum_macros::Display, EnumString)]
pub enum Orientation {
    #[strum(to_string = "1")]
    Horizontal,
    #[strum(to_string = "0")]
    Vertical,
}

#[derive(Debug, Clone)]
pub struct Edge<L> {
    id: Id,
    pub label: L,
    source: Id,
    target: Id,
}

impl<L> Edge<L> {
    pub fn new(id: Id, label: L, source: Id, target: Id) -> Self {
        Self {
            id,
            label,
            source,
            target,
        }
    }
}

impl<L> Link<Id> for Edge<L> {
    fn source(&self) -> &Id {
        &self.source
    }

    fn target(&self) -> &Id {
        &self.target
    }
}

impl<L> IdTrait<Id> for Edge<L> {
    fn id(&self) -> &Id {
        &self.id
    }
}
