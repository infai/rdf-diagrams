//! A Rust library for parsing DrawIO diagrams following the [RDF-Diagram Specification](https://infai.gitlab.io/rdf-diagram-framework/docs/), converting it to RDF serializations. A component of the [RDF-Diagram-Framework](https://gitlab.com/infai/rdf-diagram-framework).
//!
//! ## Features
//!
//! - Convert DrawIO diagrams to RDF serializations (N-Triple, Turtle)
//! - Support for both native **Rust** and **WebAssembly** targets
//! - [Autocompletion](RdfAutoComplete) for RDF-Diagrams
//! - [Error] handling with element-specific context
//!
//! ## Usage
//!
//! Install via cargo:
//!
//! ```bash
//! cargo install rdf-diagram-lib
//! ```
//!
//! ### Simple Conversion
//!
//! Convert a DrawIO diagram to Turtle format with a single function call:
//!
//! ```
//! use rdf_diagram_lib::{drawio_to_turtle, Result};
//!
//! # fn main() -> Result<()> {
//! let xml = include_str!("../tests/rdf-diagram/iri-relative-base.drawio");
//! let turtle = drawio_to_turtle(&xml)?;
//! assert_eq!(
//!     turtle,
//!     "\n<http://base.org#subject>\n    <http://base.org#predicate> <http://base.org#object>.\n"
//! );
//! # Ok(())
//! # }
//! ```
//!
//! ### Advanced Usage
//!
//! For more control over the conversion process:
//!
//! ```
//! use rdf_diagram_lib::{Diagram, Result};
//! use rdf_diagram_lib::formats::{Deserialize, Serialize};
//! use rdf_diagram_lib::formats::rdf_diagram::RdfConfig;
//! use rdf_diagram_lib::formats::drawio::DrawioDiagram;
//!
//! # fn main() -> Result<()> {
//! // Parse DrawIO XML
//! let xml = include_str!("../tests/rdf-diagram/iri-relative-base.drawio");
//! let diagram = Diagram::from_str::<DrawioDiagram>(&xml)?;
//!
//! // Configure output format
//! let config = RdfConfig::NTriple;
//!
//! // Generate N-Triple serialization
//! let n_triple = diagram.stringify_with_config(&config)?;
//! assert_eq!(
//!     n_triple,
//!     "<http://base.org#subject> <http://base.org#predicate> <http://base.org#object>.\n"
//! );
//! # Ok(())
//! # }
//! ```

use quick_xml::DeError;
use wasm_bindgen::prelude::wasm_bindgen;
use wasm_bindgen::JsValue;

use self::diagram::formats::drawio::DrawioDiagram;
use self::diagram::formats::rdf_diagram::RdfDiagram;
use self::diagram::formats::{Deserialize, Serialize};
use self::diagram::AddContaineeError;
use self::graph::AddEdgeError;

mod autocomplete;
mod diagram;
mod graph;

pub use self::autocomplete::RdfAutoComplete;
pub use self::diagram::{formats, Diagram, Id};

/// Return a library specific [Error]
/// <svg><rect width=1 height=1 /></svg>
pub type Result<T> = std::result::Result<T, Error>;

/// Returns optionally the [Id] of the element causing the error.
#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Element {id:?} has error: {source}")]
    ElementError { id: Id, source: anyhow::Error },
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

/// Javascript version of [Error].
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct JsError {
    pub element_id: Option<String>,
    pub message: String,
}

impl From<Error> for JsValue {
    fn from(error: Error) -> Self {
        let js_error = match error {
            Error::ElementError { id, source } => JsError {
                element_id: Some(id.to_string()),
                message: source.to_string(),
            },
            Error::Other(source) => JsError {
                element_id: None,
                message: source.to_string(),
            },
        };
        serde_wasm_bindgen::to_value(&js_error).unwrap()
    }
}

trait IntoElementError {
    fn into_element_error(self, id: &Id) -> Error;
}

impl<E> IntoElementError for E
where
    E: Into<anyhow::Error>,
{
    fn into_element_error(self, id: &Id) -> Error {
        Error::ElementError {
            id: id.to_owned(),
            source: self.into(),
        }
    }
}

macro_rules! impl_from_error {
    ($errtype:path) => {
        impl std::convert::From<$errtype> for crate::Error {
            fn from(e: $errtype) -> crate::Error {
                anyhow::Error::from(e).into()
            }
        }
    };
}
pub(crate) use impl_from_error;
impl_from_error!(std::io::Error);
impl_from_error!(strum::ParseError);
impl_from_error!(sophia::inmem::index::TermIndexFullError);
impl_from_error!(sophia::iri::InvalidIri);
impl_from_error!(url::ParseError);
impl_from_error!(AddEdgeError);
impl_from_error!(AddContaineeError);
impl_from_error!(DeError);

/// Takes a XML serialization of an DrawIO Diagram following the [RDF-Diagram Specification](https://infai.gitlab.io/rdf-diagram-framework/docs/) and returns a String of pretty-printed Turtle.
#[wasm_bindgen]
pub fn drawio_to_turtle(input: &str) -> Result<String> {
    RdfDiagram::from_str::<DrawioDiagram>(input)?.stringify()
}

#[cfg(all(test, target_arch = "wasm32"))]
pub(crate) mod wasm {
    pub use wasm_bindgen_test::wasm_bindgen_test as test;
    wasm_bindgen_test::wasm_bindgen_test_configure!(run_in_browser);
}

#[cfg(test)]
mod tests {
    macro_rules! set_snapshot_path {
        ($path:expr) => {
            let mut settings = insta::Settings::clone_current();
            let tests_path = $path
                .to_str()
                .and_then(|s| s.split("tests/").last())
                .unwrap_or("unknown tests path");
            settings.set_snapshot_suffix(tests_path.to_owned());
            settings.set_input_file($path);
            let _guard = settings.bind_to_scope();
        };
    }
    pub(crate) use set_snapshot_path;
}
