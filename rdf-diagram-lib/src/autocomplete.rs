use itertools::Itertools;
use logos::Logos;
use nucleo_matcher::pattern::{Atom, AtomKind, CaseMatching, Normalization};
use nucleo_matcher::Matcher;
use sophia::api::dataset::Dataset;
use sophia::api::term::Term;
use wasm_bindgen::prelude::wasm_bindgen;

use crate::autocomplete::hash_dataset::CompletionDataset;
use crate::diagram::formats::drawio::DrawioDiagram;
use crate::diagram::formats::rdf_diagram::{Base, RdfDiagram};
use crate::diagram::formats::{Deserialize, Serialize};
use crate::Result;

mod hash_dataset;
mod resource_loader;

/// Provides autocompletions for labels in a [RDF-Diagram](https://infai.gitlab.io/rdf-diagram-framework/docs/rdf-diagram.html).
///
/// When updating the [RdfAutoComplete] struct with a draw.io diagram, it tries to fetch all
/// defined namespaces.
///
/// # Example
///
/// ```
/// use rdf_diagram_lib::RdfAutoComplete;
///
/// # async_std::task::block_on(async {
/// let xml = include_str!("../tests/completion/completion.drawio");
/// let mut rdf_auto_complete = RdfAutoComplete::default();
/// rdf_auto_complete.update_and_export_to_turtle(xml).await?;
///
/// let completions = rdf_auto_complete.complete("<s")?;
/// assert_eq!(completions, vec!["<subject>"]);
/// # Ok::<(), Box<dyn std::error::Error>>(())
/// # });
/// ```
#[wasm_bindgen]
#[derive(Debug, Default)]
pub struct RdfAutoComplete {
    rdf_diagram: RdfDiagram,
    dataset: CompletionDataset,
}

#[derive(Logos, Debug, PartialEq)]
#[logos(skip r"[\s]+")]
enum Trigger {
    #[regex(r"<[^<>]*>")]
    IriRefClosed,
    #[regex(r"<[^>]*")]
    IriRefOpen,
    #[regex(r#"(".*")|('.*')"#)]
    StringClosed,
    #[regex(r#"["'][^"']*"#)]
    StringOpen,
    #[regex(r"[a-zA-Z:]+")]
    PrefixedName,
}

#[wasm_bindgen]
impl RdfAutoComplete {
    pub fn new() -> Self {
        RdfAutoComplete::default()
    }

    /// Update the completion engine with draw.io xml, which should conform to the [RDF-Diagram
    /// Specification](https://infai.gitlab.io/rdf-diagram-framework/docs/rdf-diagram.html).
    /// Returns a Result containing a String with pretty-printed Turtle on success.
    pub async fn update_and_export_to_turtle(
        &mut self,
        rdf_diagram_xml: &str,
    ) -> Result<String> {
        let rdf_diagram =
            RdfDiagram::from_str::<DrawioDiagram>(rdf_diagram_xml)?;
        self.dataset.update(&rdf_diagram).await?;
        self.rdf_diagram = rdf_diagram;
        self.rdf_diagram.stringify()
    }

    /// Returns autocompletion suggestions for the given string.
    pub fn complete(&self, input: &str) -> Result<Vec<String>> {
        let mut lex = Trigger::lexer(input);
        let trigger = lex.next().and_then(|r| r.ok());
        match trigger {
            Some(Trigger::IriRefOpen) => {
                Ok(self.match_input(input, self.iri_refs()?))
            }
            Some(Trigger::PrefixedName) => {
                Ok(self.match_input(input, self.prefixed_names()?))
            }
            Some(Trigger::StringOpen) => {
                Ok(self.match_input(input, self.literals()?))
            }
            None => Ok(self
                .rdf_diagram
                .prefixes()
                .map(|prefix| format!("{prefix}:"))
                .collect()),
            _ => Ok(vec!["Not Matched".to_owned()]),
        }
    }

    /// Returns all IRI references which are:
    /// 1. Relative to Base
    /// 2. Relative to a Prefix
    /// 3. Absolute IRI references, which are not 1. or 2.
    fn iri_refs(&self) -> Result<Vec<String>> {
        let base = self.rdf_diagram.base().unwrap_or(Base(String::default()));
        let iri_refs: Vec<_> = self
            .dataset
            .iris()
            .map_ok(|simple_term| {
                let iri = simple_term.iri().unwrap();
                iri.strip_prefix(&base.to_string())
                    .unwrap_or(&iri)
                    .to_owned()
            })
            .filter_ok(|iri| self.map_to_prefix(iri).is_none())
            .map_ok(|iri| format!("<{}>", iri))
            .try_collect()?;
        Ok(iri_refs.into_iter().unique().collect())
    }

    fn prefixed_names(&self) -> Result<Vec<String>> {
        let mut prefixed_names: Vec<_> = self
            .dataset
            .iris()
            .filter_map_ok(|simple_term| {
                let iri = simple_term.iri().unwrap();
                self.map_to_prefix(iri.as_str())
            })
            .try_collect()?;
        prefixed_names.extend(
            self.rdf_diagram
                .prefixes()
                .map(|prefix| format!("{prefix}:")),
        );
        Ok(prefixed_names)
    }

    fn literals(&self) -> Result<Vec<String>> {
        let literals: Vec<_> = self
            .dataset
            .literals()
            .map_ok(|simple_term| {
                format!("\"{}\"", simple_term.lexical_form().unwrap())
            })
            .try_collect()?;
        Ok(literals)
    }

    /// Returns a PrefixedName if the IRI Reference can be mapped to a prefix
    fn map_to_prefix(&self, str: &str) -> Option<String> {
        for (prefix, namespace) in self.rdf_diagram.namespaces() {
            if let Some(local_part) = str.strip_prefix(namespace) {
                return Some(format!("{}:{}", prefix, local_part));
            }
        }
        None
    }

    fn match_input(&self, input: &str, items: Vec<String>) -> Vec<String> {
        let mut matcher = Matcher::default();
        Atom::new(
            input,
            CaseMatching::Smart,
            Normalization::Smart,
            AtomKind::Fuzzy,
            false,
        )
        .match_list(items, &mut matcher)
        .into_iter()
        .map(|item| item.0.to_owned())
        .unique()
        .collect()
    }
}

#[cfg(test)]
mod tests {

    use pretty_assertions::assert_eq;
    use rstest::{fixture, rstest};

    use super::*;
    #[cfg(target_arch = "wasm32")]
    use crate::wasm;

    #[fixture]
    async fn rdf_diagram_manager() -> Result<RdfAutoComplete> {
        let xml = include_str!("../tests/completion/completion.drawio");
        let mut engine = RdfAutoComplete::default();
        engine.update_and_export_to_turtle(xml).await?;
        Ok(engine)
    }

    #[rstest]
    #[cfg_attr(target_arch = "wasm32", wasm::test)]
    async fn test_iri_refs(
        #[future]
        #[notrace]
        rdf_diagram_manager: Result<RdfAutoComplete>,
    ) {
        let result = rdf_diagram_manager.await.unwrap().iri_refs().unwrap();
        assert_eq!(
            result,
            vec![
                "<subject>",
                "<predicate>",
                "<object>",
                "<http://prefix_unkown#object>"
            ]
        );
    }

    #[rstest]
    #[case("", vec!["prefix_unused:", "prefix_used:"])]
    #[case("<", vec!["<subject>", "<predicate>", "<object>", "<http://prefix_unkown#object>"])]
    #[case("<s", vec!["<subject>"])]
    #[case("prefix_un", vec!["prefix_unused:"])]
    #[case("o", vec!["prefix_used:object"])]
    #[case("\"", vec!["\"String\""])]
    // #[case("\'", vec!["'String'"])]
    #[trace]
    #[cfg_attr(target_arch = "wasm32", wasm::test)]
    async fn test_complete(
        #[future]
        #[notrace]
        rdf_diagram_manager: Result<RdfAutoComplete>,
        #[case] input: &str,
        #[case]
        #[notrace]
        expected: Vec<&str>,
    ) {
        let result =
            rdf_diagram_manager.await.unwrap().complete(input).unwrap();
        assert_eq!(result, expected);
    }

    #[rstest]
    #[case("", None)]
    #[case("<", Some(Trigger::IriRefOpen))]
    #[case("<foo", Some(Trigger::IriRefOpen))]
    #[case("<>", Some(Trigger::IriRefClosed))]
    #[case("<foo>", Some(Trigger::IriRefClosed))]
    #[case("foo", Some(Trigger::PrefixedName))]
    #[case(":", Some(Trigger::PrefixedName))]
    #[case("''", Some(Trigger::StringClosed))]
    #[case("\"\"", Some(Trigger::StringClosed))]
    #[case("'", Some(Trigger::StringOpen))]
    #[case("\"", Some(Trigger::StringOpen))]
    #[case("\"foo", Some(Trigger::StringOpen))]
    #[trace]
    #[cfg_attr(target_arch = "wasm32", wasm::test)]
    fn test_lexer(
        #[case] input: &str,
        #[case]
        #[notrace]
        expected: Option<Trigger>,
    ) {
        let mut lex = Trigger::lexer(input);
        let actual = lex.next().and_then(|r| r.ok());
        assert_eq!(actual, expected);
    }
}
