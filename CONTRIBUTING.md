## Git Workflow

- You should follow the guidelines in the [InfAI Wiki](https://gitlab.com/infai/wiki/-/wikis/Contributing-guide)
- Before commit:
    ```makefile
    just pre-commit-check
    ```

# Project Setup Guide

This is the guide for developing the draw.io plugin.

## Prerequisites

Make sure you meet the following prerequisites before getting started:

- Linux operating system (this guide uses Ubuntu)

## Steps

1. Install Clang and LLVM (if required):

    ```bash
    sudo apt install clang
    sudo apt install llvm

2. Add the Node.js repository and install Node.js:

    ```bash
    curl -fsSL https://deb.nodesource.com/setup_21.x | sudo -E bash -
    sudo apt-get install -y nodejs
    ```

3. Install Rust using rustup:

    ```bash
    curl -proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    ```

4. Install `just`, a task automation tool:

    ```bash
    cargo install just
    ```

5. Start the development environment (in /rdf-diagram-framewok/rdf-diagram-drawio):

    ```bash
    just develop
    ```

Now the project should start in a new browser window
