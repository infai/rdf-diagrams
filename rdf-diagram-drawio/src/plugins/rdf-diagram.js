/* eslint new-cap: "off" */

/**
 * Load Plugin
 *
 * Graph is global object of: src/main/webapp/js/grapheditor/Graph.js
 * ui is EditorUI: src/main/webapp/js/diagramly/EditorUi.js
 *
 * mxGraph specification, see:
 *  - https://jgraph.github.io/mxgraph/docs/js-api/files/index-txt.html
 *  - https://jgraph.github.io/mxgraph/docs/manual.html#3.1.3.1
 *
 * It might help to try/develope straight in the developer console due it does not need to wait for the browser reloads.
 * The `Draw` object is global:
 *      Draw.loadPlugin(function(editorUI) {
 *          var graph = editorUI.editor.graph
 *          var cells = graph.getModel().getChildren(graph.getDefaultParent())
 *          console.log('CELLS', cells)
 *      })
 */
import * as RdfConverter from 'rdf-diagram-lib'

const RdfDiagram = function (ui) {
  const self = this
  this.ui = ui
  this.editorUi = ui
  this.graph = ui.editor.graph
  this.apiUrl =
    urlParams.test === '1' || urlParams.dev === '1'
      ? 'http://localhost:8000/api'
      : 'https://infai.gitlab.io/rdf-diagram-framework/api'
  this.liveRDFCheckEnabled = false
  // this.liveCheckEnabled = (urlParams['test'] == '1' || urlParams['dev'] == '1') ? false : true;
  this.rdfOutputWindow = null

  console.log('aaaaaaa')
  EditorUi.debug('rdf-diaam', [this])

  console.log('aaaaaaa')
  this.initUi()

  self.graph.model.addListener(
    mxEvent.CHANGE,
    mxUtils.bind(self, function (sender, evt) {
      self.initChangeDetections(evt)
    })
  )

  // TODO key handler
  // var editorUiOnKeyDown = ui.onKeyDown;
  // ui.onKeyDown = function(evt) {
  //     editorUiOnKeyDown.apply(this, arguments);
  //     // if (!mxEvent.isConsumed(evt))
  //     // {
  //     //     console.log('LiveDetection.isConsumed?!?');
  //     // }
  // }
}

RdfDiagram.prototype.initUi = function () {
  // mxConstants.SHADOWCOLOR = '#FF6666';
  // mxConstants.SHADOW_OPACITY = 0.75;
  // mxConstants.SHADOW_OFFSET_X = 0;

  this.graph.setAllowDanglingEdges(true)
  this.graph.setDisconnectOnMove(false)
  this.graph.setCloneInvalidEdges(false)

  this.createMenus()

  console.log('lib is there')
  this.initCustomLibrary()
  this.customCellsForShapePicker()
  this.initCellCollapseImage()
  this.initCustomCellStyles()
  this.rdfFormatPanel()
  this.initCellHovers()

  // Add methods that require refreshments here
  // this.update = mxUtils.bind(this, function(sender, evt) {
  //   this.refreshUi();
  // });
  // this.graph.getSelectionModel().addListener(mxEvent.CHANGE, this.update);
  // this.graph.getModel().addListener(mxEvent.CHANGE, this.update);
  // this.graph.addListener(mxEvent.ROOT, this.update);
}

RdfDiagram.prototype.refreshUi = function () {
  if (this.pendingRefresh != null) {
    window.clearTimeout(this.pendingRefresh)
    this.pendingRefresh = null
  }

  this.pendingRefresh = window.setTimeout(
    mxUtils.bind(this, function () {
      // self.createFormatPanel();
      // Ad refresh mthods here
    })
  )
}

RdfDiagram.prototype.createMenus = function () {
  EditorUi.debug('rdf-diagram.createUi', [this])
  const self = this

  mxResources.parse('exportRDF=RDF...')
  const exportAsMenu = this.editorUi.menus.get('exportAs')
  const oldExportAsfunct = exportAsMenu.funct

  this.editorUi.actions.addAction('exportRDF', function () {
    self.exportRDFAction()
  })

  if (urlParams.test === '1' || urlParams.dev === '1') {
    // create ExportAs->Tests2ZIP
    mxResources.parse('exportRDFTestsAsZIP=RDF Tests as ZIP...')
    this.editorUi.actions.addAction('exportRDFTestsAsZIP', function () {
      self._testsuite_exportTestsAsZIP()
    })

    // create Extras->RDF-Testframework
    mxResources.parse('rdfTest=Run RDF Tests...')
    const extrasMenu = this.editorUi.menus.get('extras')
    const oldxFunct = extrasMenu.funct
    extrasMenu.funct = function (menu, parent) {
      oldxFunct.apply(this, arguments)
      self.editorUi.menus.addMenuItem(menu, 'rdfTest', parent)
    }
    this.editorUi.actions.addAction('rdfTest', function () {
      self._testsuite_exec()
    })
  }

  exportAsMenu.funct = function (menu, parent) {
    oldExportAsfunct.apply(this, arguments)
    self.editorUi.menus.addMenuItem(menu, 'exportRDF', parent)
    if (urlParams.test === '1' || urlParams.dev === '1') {
      self.editorUi.menus.addMenuItem(menu, 'exportRDFTestsAsZIP', parent)
    }
  }

  // var footer = this.ui.footerContainer
  // var statusContainer = this.editorUi.statusContainer;

  // var buttonContainer = this.ui.menubar.container.children.item(this.ui.menubar.container.children.length - 1)
  // const barContainer = this.editorUi.buttonContainer
  // const menubarContainer = this.editorUi.menubar.container
  // var drawioStatusContainer = this.editorUi.statusContainer
  // console.log('CONTAINER', {menubarContainer, statusContainer});

  const seperator = document.createElement('div')
  seperator.className = 'getSeparator'

  this.toolbarContainer = document.createElement('div')
  this.toolbarContainer.className = 'geLabel rdfToolbarContainer'
  this.toolbarContainer.style.cssText = 'display:inline-flex'

  this.editorUi.toolbar.container.appendChild(seperator)
  this.editorUi.toolbar.container.appendChild(this.toolbarContainer)
  // menubar.appendChild(this.rdfrdfStatusContainer)

  const liveCheck = this.ui.addCheckbox(
    this.toolbarContainer,
    mxResources.get('live RDF syntax check'),
    !(urlParams.test === '1' || urlParams.dev === '1')
  )
  liveCheck.style.marginTop = '0px'
  mxEvent.addListener(liveCheck, 'change', function (evt) {
    self.liveRDFCheckEnabled = liveCheck.checked
    self.doRDFCheck()
  })

  // TODO open RDF output on click
  const rdfCheckCont = document.createElement('span')
  this.toolbarContainer.appendChild(rdfCheckCont)

  const openConsoleBtn = document.createElement('a')
  openConsoleBtn.href = '#'
  openConsoleBtn.style.color = 'red'
  openConsoleBtn.appendChild(document.createTextNode('Click here to view details.'))
  openConsoleBtn.setAttribute('title', 'Open RDF output console to view details.')

  mxEvent.addListener(openConsoleBtn, 'click', mxUtils.bind(this, function (evt) {
    self.editorUi.actions.get('rdfOutput').funct(evt)
    mxEvent.consume(evt)
  }))

  this.graph.model.addListener('didRDFCheck', mxUtils.bind(self, function (sender, evt) {
    const result = evt.getProperty('result')
    rdfCheckCont.innerText = ''
    if (typeof result === 'object') {
      rdfCheckCont.insertAdjacentHTML('beforeend', ': <span style="color: red">Error.</span> ')
      rdfCheckCont.append(openConsoleBtn)
    } else {
      rdfCheckCont.textContent = ': OK'
    }
  }))

  // style toolbar child
  // var formatContainer = Format.container

  // var ss = ui.getSelectionState();
  // if (!ss.containsLabel && ss.cells.length > 0) {

  // }

  mxResources.parse('rdfOutput=RDF Output')
  const viewMenu = this.editorUi.menus.get('view')
  const oldViewFunct = viewMenu.funct
  viewMenu.funct = function (menu, parent) {
    oldViewFunct.apply(this, arguments)
    self.editorUi.menus.addMenuItems(menu, ['-', 'rdfOutput'], parent)
  }

  const viewPanelsMenu = this.editorUi.menus.get('viewPanels')
  const oldViewPanelsMenu = viewPanelsMenu.funct

  viewPanelsMenu.funct = function (menu, parent) {
    oldViewPanelsMenu.apply(this, arguments)
    self.editorUi.menus.addMenuItems(menu, ['-', 'rdfOutput'], parent)
  }

  const action = this.editorUi.actions.addAction('rdfOutput', mxUtils.bind(this, function () {
    if (this.rdfOutputWindow == null) {
      this.rdfOutputWindow = self.initRdfOutputWindow(
        document.body.offsetWidth - 640,
        60,
        420,
        document.body.offsetHeight - 60
      )

      this.rdfOutputWindow.window.addListener('show', mxUtils.bind(this, function () {
        self.editorUi.fireEvent(new mxEventObject('rdfOutput'))
      }))
      this.rdfOutputWindow.window.addListener('hide', function () {
        self.editorUi.fireEvent(new mxEventObject('rdfOutput'))
      })
      this.rdfOutputWindow.window.setVisible(true)
      self.editorUi.fireEvent(new mxEventObject('rdfOutput'))
    } else {
      this.rdfOutputWindow.window.setVisible(!this.rdfOutputWindow.window.isVisible())
    }
  }), null, null, null)
  action.setToggleAction(true)
  action.setSelectedCallback(mxUtils.bind(this, function () { return this.rdfOutputWindow != null && this.rdfOutputWindow.window.isVisible() }))
}

RdfDiagram.prototype.initCellHovers = function () {
  const self = this

  // Defines the tolerance before removing the icons
  const iconTolerance = 20

  // Shows icons if the mouse is over a cell
  this.graph.addMouseListener({
    currentState: null,
    currentIconSet: null,
    mouseDown: function (sender, me) {
      // Hides icons on mouse down
      if (this.currentState != null) {
        this.dragLeave(me.getEvent(), this.currentState)
        this.currentState = null
      }
    },
    mouseMove: function (sender, me) {
      if (
        this.currentState != null &&
        (me.getState() === this.currentState || me.getState() == null)
      ) {
        const tol = iconTolerance
        const tmp = new mxRectangle(
          me.getGraphX() - tol,
          me.getGraphY() - tol,
          2 * tol,
          2 * tol
        )

        if (mxUtils.intersects(tmp, this.currentState)) {
          return
        }
      }

      let tmp = self.graph.view.getState(me.getCell())

      // Ignores everything but vertices
      // TODO exclude shape=note !!!
      if (
        self.graph.isMouseDown ||
        (tmp != null && !self.graph.getModel().isVertex(tmp.cell))
      ) {
        tmp = null
      }

      if (tmp !== this.currentState) {
        if (this.currentState != null) {
          this.dragLeave(me.getEvent(), this.currentState)
        }

        this.currentState = tmp

        if (this.currentState != null) {
          this.dragEnter(me.getEvent(), this.currentState)
        }
      }
    },
    mouseUp: function (sender, me) { },
    dragEnter: function (evt, state) {
      if (this.currentIconSet == null) {
        this.currentIconSet = new self.mxIconSet(state, self)
      }
    },
    dragLeave: function (evt, state) {
      if (this.currentIconSet != null) {
        this.currentIconSet.destroy()
        this.currentIconSet = null
      }
    }
  })

  // Enables rubberband selection
  // new mxRubberband(this.graph);
}

RdfDiagram.prototype.mxIconSet = function (state, rdfDiagram) {
  this.images = []
  const graph = state.view.graph

  // TODO implement global styles
  const stackStyle =
    'swimlane;html=1;fontStyle=0;childLayout=stackLayout;horizontal=1;startSize=38;horizontalStack=0;resizeParent=1;resizeParentMax=0;resizeLast=0;collapsible=1;marginBottom=0;fontSize=12;points=[[0,0,0,0,0],[0,0,0,0,19],[0.25,0,0,0,0],[0.5,0,0,0,0],[0.75,0,0,0,0],[1,0,0,0,0],[1,0,0,0,19]];swimlaneLine=1;snapToPoint=1;swimlaneFillColor=#FFFFFF;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;'
  const stackPropStyle =
    'text;html=1;strokeColor=none;fillColor=none;align=left;verticalAlign=middle;spacingLeft=4;spacingRight=4;overflow=hidden;points=[[0,0.5],[1,0.5]];portConstraint=eastwest;rotatable=0;connectable=0;'

  const imgToStack = mxUtils.createImage('images/list.svg')
  imgToStack.setAttribute('title', 'Transform into StackNode')
  imgToStack.style.position = 'absolute'
  imgToStack.style.cursor = 'pointer'
  imgToStack.style.width = '16px'
  imgToStack.style.height = '16px'
  imgToStack.style.left = state.x + state.width - 17 + 'px'
  imgToStack.style.top = state.y + state.height - 16 + 'px'

  const imgAddProp = mxUtils.createImage('images/plus.png')
  imgAddProp.setAttribute('title', 'Add property')
  imgAddProp.style.position = 'absolute'
  imgAddProp.style.cursor = 'pointer'
  imgAddProp.style.width = '12px'
  imgAddProp.style.height = '12px'
  imgAddProp.style.left = state.x + 2 + 'px'

  const transformToStack = function (me, evt, cell) {
    // set min width
    if (cell.value == '') {
      cell.geometry.width = 170
    }
    graph.setCellStyle(stackStyle, [cell])

    graph.insertVertex(cell, null, ':p :o', 0, 0, 170, 40, stackPropStyle)

    mxEvent.consume(evt)
    me.destroy()
  }

  const addPropertyToStack = function (me, evt, stack) {
    graph.insertVertex(stack, null, ':p :o', 0, 0, 170, 40, stackPropStyle)

    mxEvent.consume(evt)
    me.destroy()
  }

  mxEvent.addGestureListeners(imgToStack, mxUtils.bind(this, function (evt) {
    transformToStack(this, evt, state.cell)
  }))

  mxEvent.addGestureListeners(imgAddProp, mxUtils.bind(this, function (evt) {
    if (
      state.cell &&
      rdfDiagram.isStackContainer(state.cell)
    ) {
      addPropertyToStack(this, evt, state.cell)
    } else if (
      state.cell.parent &&
      rdfDiagram.isStackContainer(state.cell.parent)
    ) {
      addPropertyToStack(this, evt, state.cell.parent)
    } else {
      console.error('Unbdefined cell/parent to add a property', { state })
    }
  }))

  if (
    state.cell &&
    rdfDiagram.isStackContainer(state.cell)
  ) {
    imgAddProp.style.top = state.cell.y + state.cell.height + 2 + 'px'
    graph.container.appendChild(imgAddProp)
    this.images.push(imgAddProp)
  } else if (
    state.cell.parent &&
    rdfDiagram.isStackContainer(state.cell.parent)
  ) {
    const parentState = graph.view.getState(state.cell.parent)
    imgAddProp.style.top = parentState.y + parentState.height + 2 + 'px'
    state.view.graph.container.appendChild(imgAddProp)
    this.images.push(imgAddProp)
  } else if (
    state.cell &&
    !rdfDiagram.isStackContainer(state.cell) &&
    !rdfDiagram.isStackContainer(state.cell.parent) &&
    !rdfDiagram.isNamespaceContainer(state.cell)
  ) {
    state.view.graph.container.appendChild(imgToStack)
    this.images.push(imgToStack)
  }

  this.destroy = function () {
    if (this.images != null) {
      for (let i = 0; i < this.images.length; i++) {
        const img = this.images[i]
        img.parentNode.removeChild(img)
      }
    }
    this.images = null
  }
}

RdfDiagram.prototype.rdfFormatPanel = function () {
  const originPanel = StyleFormatPanel.prototype.init
  StyleFormatPanel.prototype.init = function () {
    originPanel.apply(this, arguments)

    const rdfPanel = this.createPanel()
    rdfPanel.style.paddingTop = '8px'

    const title = this.createTitle(mxResources.get('RDF'))
    title.style.paddingLeft = '14px'
    title.style.paddingTop = '10px'
    title.style.paddingBottom = '6px'
    rdfPanel.appendChild(title)

    const opacityPanel = this.createRelativeOption(
      mxResources.get('my-2-opacity'),
      mxConstants.STYLE_OPACITY
    )
    opacityPanel.style.paddingTop = '8px'
    opacityPanel.style.paddingBottom = '10px'
    rdfPanel.appendChild(opacityPanel)

    this.container.insertBefore(rdfPanel, this.container.childNodes[1])
  }
}

RdfDiagram.prototype.OLD_createFormatPanel = function () {
  // More than 3 Panels DONT WORK

  // see drawio/drawio.git/src/main/webapp/js/grapheditor/Format.js

  console.log('createFormatPanel', {
    'format:': this.editorUi.format,
    container: this.editorUi.formatContainer
  })

  if (this.editorUi.formatContainer.style.width === '0px') {
    return
  }

  var div = document.createElement('div')
  div.style.whiteSpace = 'nowrap'
  div.style.color = Editor.isDarkMode() ? '#8D8D8D' : '#616161'
  div.style.textAlign = 'left'
  div.style.cursor = 'default'

  // var styleFormatPanel = this.editorUi.format.panels[0]
  // console.log('styleFormatPanel', styleFormatPanel);

  const span = document.createElement('div')
  span.style.width = '100%'
  span.style.marginTop = '0px'
  span.style.fontWeight = 'bold'
  span.style.padding = '10px 0 0 14px'
  mxUtils.write(span, mxResources.get('RDF-STYLES'))

  const ss = this.editorUi.getSelectionState()

  if (!ss.containsLabel && ss.cells.length > 0) {
    this.editorUi.formatContainer.appendChild(span)
  }

  var div = document.createElement('div')
  div.style.whiteSpace = 'nowrap'
  div.style.color = Editor.isDarkMode() ? '#8D8D8D' : '#616161'
  div.style.textAlign = 'left'
  div.style.cursor = 'default'

  const label = document.createElement('div')
  label.className = 'geFormatSection'
  label.style.textAlign = 'center'
  label.style.fontWeight = 'bold'
  label.style.paddingTop = '8px'
  label.style.fontSize = '13px'
  label.style.borderWidth = '0px 0px 1px 1px'
  label.style.borderStyle = 'solid'
  label.style.display = 'inline-block'
  label.style.height = '25px'
  label.style.overflow = 'hidden'
  label.style.width = '100%'

  label.style.borderLeftWidth = '1px'
  label.style.cursor = 'pointer'
  label.style.width =
    ss.cells.length == 0 ? '100%' : ss.containsLabel ? '50%' : '33.3%'

  if (!this.graph.isSelectionEmpty() && !this.graph.isEditing()) {
    console.log('Create My RDF FORMAT PAnel')
    console.log('Format:', this.editorUi.format)

    const format = this.editorUi.format
    // var idx = ;

    // RDF
    const label12 = label.cloneNode(false)
    label12.style.backgroundColor = Format.inactiveTabBackgroundColor
    mxUtils.write(label12, mxResources.get('RDF'))
    div.appendChild(label12)

    const rdfPanel = div.cloneNode(false)
    rdfPanel.style.display = 'none'
    format.panels.push(new TextFormatPanel(format, this.ui, rdfPanel))
    format.container.appendChild(rdfPanel)

    if (ss.cells.length > 0) {
      // addClickHandler(label12, rdfPanel, idx + 1);
    } else {
      label12.style.display = 'none'
    }

    console.log('Format:', this.editorUi.format)
  }
}

RdfDiagram.prototype.initCellCollapseImage = function () {
  // TODO may see here: https://github.com/jgraph/mxgraph-js/blob/master/javascript/examples/hovericons.html
  console.log('TODO: init cell colapse image')
  return
  const self = this

  const getFoldImageOriginal = this.graph.getFoldingImage
  const getFoldingImage = function (state) {
    if (
      state != null &&
      self.graph.foldingEnabled &&
      !self.graph.getModel().isEdge(state.cell)
    ) {
      const isCollapsed = self.graph.isCellCollapsed(state.cell)
      if (self.graph.isCellFoldable(state.cell, !isCollapsed)) {
        return isCollapsed
          ? self.graph.collapsedImage
          : self.graph.expandedImage
      }
    }
    return null
  }
  this.graph.getFoldingImage = function () {
    return null
  }

  // TODO this is still Buggy
  //  - toggle collapse causes bad shapes (shadow border)

  this.graph.addMouseListener({
    currentState: null,
    previousStyle: null,
    mouseDown: function (sender, me) {
      if (this.currentState != null) {
        console.log('Mouse Down', {
          sender,
          me,
          event: me.getEvent(),
          source: me.getSource()
        })
        if (me.getSource().nodeName != 'image') {
          this.dragLeave(me.getEvent(), this.currentState)
          this.currentState = null
        }
      }
    },
    mouseMove: function (sender, me) {
      if (this.currentState != null && me.getState() == this.currentState) {
        return
      }

      let tmp = self.graph.view.getState(me.getCell())

      // Ignores everything but vertices
      if (
        self.graph.isMouseDown ||
        (tmp != null && !self.graph.getModel().isVertex(tmp.cell)) ||
        (tmp != null && !self.graph.isCellFoldable(tmp.cell))
      ) {
        tmp = null
      }

      if (tmp != this.currentState) {
        console.log('Mouse Move', { tmp })
        if (this.currentState != null) {
          this.dragLeave(me.getEvent(), this.currentState)
        }

        this.currentState = tmp

        if (this.currentState != null) {
          this.dragEnter(me.getEvent(), this.currentState)
        }
      }
    },
    mouseUp: function (sender, me) { },
    dragEnter: function (evt, state) {
      if (state != null) {
        console.log('DRAG ENTER ', { evt, state })
        // self.graph.getFoldingImage = getFoldImageOriginal
        self.graph.getFoldingImage = function (state) {
          return getFoldingImage(state)
        }

        // this.previousStyle = state.style;
        // state.style = mxUtils.clone(state.style);
        // updateStyle(state, true);

        // if (state.shape != null) {
        //     state.shape.apply(state);
        //     state.shape.redraw();
        //     state.shape.redrawShape();
        // }

        state.shape.redraw()
        self.graph.cellRenderer.redraw(state)
      }
    },
    dragLeave: function (evt, state) {
      if (state != null) {
        console.log('DRAG LEAVE ', { evt, state })
        self.graph.getFoldingImage = function () {
          return null
        }

        // state.style = this.previousStyle;

        // updateStyle(state, false);
        // if (state.shape != null) {
        //     state.shape.apply(state);
        //     state.shape.redraw();
        //     state.shape.redrawShape();
        // }
        self.graph.cellRenderer.redraw(state)
      }
    }
  })
}

RdfDiagram.prototype.initCustomCellStyles = function () {
  console.log('TODO: init custom cell styles')
  return
  const style = new Object()
  style[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_RECTANGLE
  style[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter
  style[mxConstants.STYLE_ROUNDED] = true
  style[mxConstants.STYLE_FONTCOLOR] = 'red'
  style.width = '100%'
  style.overflow = 'hidden'
  style.textOverflow = 'ellipsis'
  this.graph.getStylesheet().putCellStyle('myStyleTest', style)
}

RdfDiagram.prototype._testsuite_exec = async function () {
  const self = this
  const graph = this.editorUi.editor.graph
  let ti = 0

  // See `new mxWindow()` to create own window!
  if (!mxLog.isVisible()) {
    mxLog.show()
    mxLog.window.div.style.width = '600px'
    mxLog.window.div.style.height = '320px'
    mxLog.window.div.style.left = `${document.body.clientWidth - 1.5 * 600}px`
    mxLog.window.div.style.top = '150px'
    mxLog.window.div.firstChild.style.width = '600px'
    mxLog.window.div.firstChild.style.height = '320px'
    mxLog.window.div.firstChild.querySelector('div.mxWindowPane').style.height =
      '289px'
    // mxLog.window.div.firstChild.querySelector(".mxWindowTitle").prepend("RDF Diagram Framework Test - ")
  } else {
    mxLog.window.fit()
  }

  const testContainers = this._testsuite_getContainers()
  if (!testContainers) {
    alert('No testcontainer found!')
    return
  }
  const namespace = this.getNamespaceContainer()
  if (!namespace) console.warn('No namespace found!')

  const w3ctestUrl = graph
    .getLabel(namespace)
    .replace(/\n/g, '')
    .replace(/.*@prefix w3ctest: <(.*)>/gm, '$1')
  // console.log('Testcontainers', testContainers)
  // console.log('Namespace', namespace);

  this.editorUi.spinner.spin(
    document.body,
    mxResources.get('Running RDF tests')
  )

  function execTest (testContainer) {
    const name = graph
      .getLabel(testContainer)
      .replace(/\n/g, '')
      .replace(/.*name:[\s\"']*([0-9a-zA-Z:\/.-_\s]*).*/gm, '$1')
    const action = graph
      .getLabel(testContainer)
      .replace(/\n/g, '')
      .replace(/.*action:[\s\"']*([0-9a-zA-Z:\/.-_]*).*/gm, '$1')

    mxLog.debug(
      `Test ${1 + ti}/${testContainers.length}: ${name} (${testContainer.id
      }), action: ${action} ...`
    )

    // TODO try/catch

    if (action.search('[a-z]:' > -1)) {
      const prfx = action.replace(/([a-z]):.*/, '$1')
      // var regNmsp = new RegExp(`.*@prefix ${prfx}: <(.*)>`, 'g')
      const namespaceUrl = graph
        .getLabel(namespace)
        .replace(/\n/g, '')
        .replace(RegExp(`.*@prefix\\s+${prfx}:\\s*<(.*)>`, 'g'), '$1')
      const actionUrl = action.replace(RegExp(`${prfx}:`), namespaceUrl)
      // console.log({name, prfx, namespaceUrl, actionUrl});
    }

    // TODO test if testContainer has children
    graph.selectCells(true, true, testContainer)
    graph.addSelectionCell(namespace)

    return new Promise(async (resolve, reject) => {
      // TODO send RDF to conversion API and test against test-data
      // var tdata = await (await fetch(`${w3ctestUrl}${actionUrl}`)).text()
      // Dummy test
      setTimeout(() => {
        mxLog.debug('...OK')
        resolve(true)
      }, 1000)
    })
  }

  mxLog.debug('Testsuite | RDF Diagram Framework\n======================\n')

  for (const testContainer of testContainers) {
    await execTest(testContainer)
    ti++
  }

  this.editorUi.spinner.stop()
  mxLog.debug('======================\nResult: ...')
}

RdfDiagram.prototype._testsuite_exportTestsAsZIP = async function () {
  const self = this
  const graph = this.editorUi.editor.graph

  if (
    typeof JSZip === 'undefined' &&
    !this.loadingExtensions &&
    !this.isOffline(true)
  ) {
    alert('JSZip lib not found!')
    // TODO may try to load extensions
    // see drawio/drawio.git/src/main/webapp/js/diagramly/EditorUi.js #9306
    return
  }

  const zip = new JSZip()

  const testContainers = this._testsuite_getContainers()
  if (!testContainers) {
    alert('No testcontainer found!')
    return
  }
  const namespace = this.getNamespaceContainer()
  if (!namespace) console.warn('No namespace found!')

  function execTest (testContainer) {
    const name = graph
      .getLabel(testContainer)
      .replace(/\n/g, '')
      .replace(/.*name:[\s\"']*([0-9a-zA-Z:\/.-_\s]*).*/gm, '$1')
    // var action = graph.getLabel(testContainer).replace(/\n/g, "").replace(/.*action:[\s\"']*([0-9a-zA-Z:\/.-_]*).*/gm, "$1")

    graph.selectCells(true, true, testContainer)
    graph.addSelectionCell(namespace)

    const notCompacted = true
    const xmlData = self.getXml()

    const filename = name.replace(/[^a-z0-9]/gi, '_')
    zip.file(`${filename}.xml`, xmlData)
  }

  this.editorUi.spinner.spin(document.body, mxResources.get('exporting'))
  try {
    for (const testContainer of testContainers) {
      await execTest(testContainer)
    }

    zip.generateAsync({ type: 'base64' }).then(function (content) {
      self.editorUi.spinner.stop()
      self.editorUi.saveData(
        'RdfDiagram-Tests.zip',
        'zip',
        content,
        'application/zip',
        true
      )
    })
  } catch (error) {
    console.log(error)
  } finally {
    this.editorUi.spinner.stop()
  }
}

RdfDiagram.prototype._testsuite_getContainers = function () {
  const graph = this.editorUi.editor.graph
  const cells = graph.getModel().getChildren(graph.getDefaultParent())
  const testContainers = cells.filter((cell) =>
    cell.getAttribute('testcontainer')
  )
  return testContainers
}

RdfDiagram.prototype.initCustomLibrary = function () {
  console.log('lib is there')
  const self = this
  // see also Sidebar.js for manual implementations

  // Todo: Use static var
  const path =
    urlParams.test == '1' || urlParams.dev == '1'
      ? 'plugins/rdf-diagram/library.xml'
      : 'plugins/rdf-diagram/library.xml'

  fetch(path)
    .then((response) => response.text())
    .then((response) => {
      try {
        self.editorUi.loadLibrary(
          new LocalLibrary(self.editorUi, response, 'RDF-Diagram')
        )
        console.log('lib is there')
      } catch (e) {
        console.error('e', e)
        self.editorUi.handleError(e, mxResources.get('errorLoadingFile'))
      }
    })
    .catch((e) => {
      console.log(e)
    })
}

RdfDiagram.prototype.isCurrentCellStyle = function (cell, shape, value) {
  return mxUtils.getValue(this.graph.getCurrentCellStyle(cell), shape, null) == value
}

RdfDiagram.prototype.isNamespaceContainer = function (cell) {
  return this.isCurrentCellStyle(cell, 'shape', 'note')
}

RdfDiagram.prototype.isStackContainer = function (cell) {
  // return this.isCurrentCellStyle(cell, 'childLayout', 'stackLayout');
  return this.graph.isSwimlane(cell)
}

RdfDiagram.prototype.isBlanknode = function (cell) {
  return cell.value === ''
}

RdfDiagram.prototype.getNamespaceContainer = function () {
  const graph = this.editorUi.editor.graph
  const cells = graph.getModel().getChildren(graph.getDefaultParent())
  const namespace = cells.find(cell => this.isNamespaceContainer(cell))
  return namespace
}

RdfDiagram.prototype.exportRDFAction = function () {
  const self = this

  // see drawio/drawio.git/src/main/webapp/js/diagramly/Menus.js : 400
  const selectionOnly = false
  const noPages = false

  const basename = self.ui.getBaseFilename(true)
  const div = document.createElement('div')
  div.style.whiteSpace = 'nowrap'
  // var noPages = editorUi.pages == null || editorUi.pages.length <= 1;

  const hd = document.createElement('h3')
  mxUtils.write(hd, mxResources.get('formatRdf'))
  hd.style.cssText =
    'width:100%;text-align:center;margin-top:0px;margin-bottom:4px'
  div.appendChild(hd)

  self.editorUi.addCheckbox(
    div,
    mxResources.get('selectionOnly'),
    false,
    self.graph.isSelectionEmpty()
  )
  self.editorUi.addCheckbox(
    div,
    mxResources.get('compressed'),
    Editor.defaultCompressed
  )
  // var pages = editorUi.addCheckbox(div, mxResources.get('allPages'), !noPages, noPages);
  // pages.style.marginBottom = '16px';

  const formatSelect = document.createElement('select')
  formatSelect.style.cssText = 'margin-right: 8px; margin-top: 16px;'
  const option = document.createElement('option')
  option.value = 'ttl'
  mxUtils.write(option, mxResources.get('formatTtl'))
  // option.value = driveUsers.length;
  // option.setAttribute('disabled', 'disabled');
  option.setAttribute('selected', 'selected')
  formatSelect.before(mxResources.get('format') + ': ')
  formatSelect.appendChild(option)
  div.appendChild(formatSelect)

  const dlg = new CustomDialog(
    self.editorUi,
    div,
    mxUtils.bind(this, function () {
      // self.editorUi.downloadFile('xml', !compressed.checked, null,
      //     !selection.checked, noPages || !pages.checked);
      // ui.saveData(`${basename}.ttl`, 'ttl', res.ttl_data, 'text/turtle');

      try {
        const rdf = self.convert2RDF(self.getXml())
        self.ui.saveData(
          `${basename}.ttl`,
          'ttl',
          rdf,
          'text/turtle'
        )
      } catch (error) {
        self.ui.alert(
          `rdf-diagram convert error:\n${error.toString()}\nSee console for details`
        )
      }
    }),
    null,
    mxResources.get('export')
  )

  self.editorUi.showDialog(dlg.container, 300, 200, true, true)
}

RdfDiagram.prototype.convert2RDF = function (xmlData) {
  try {
    const rdf = RdfConverter.convert_to_turtle(xmlData)
    EditorUi.debug('rdf-diagram.convert2RDF', { rdf })
    return rdf
  } catch (error) {
    EditorUi.logError(
      'rdf-diagram.convert2RDF error',
      null,
      null,
      null,
      error
    )
    throw error
  }
}

RdfDiagram.prototype.customCellsForShapePicker = function () {
  EditorUi.debug('rdf-diagram.customCellsShapePicker', [this])

  const self = this
  const graph = this.ui.editor.graph

  // var createVertex = mxUtils.bind(this, function(style, w, h, value)
  // {
  // 	return graph.createVertex(null, null, value || '', 0, 0, w || 120, h || 37, style, false);
  // });

  this.ui.getCellsForShapePicker_origin = this.ui.getCellsForShapePicker
  this.ui.createShapePicker_origin = this.ui.createShapePicker

  // set default edge label for new  shapes
  this.ui.createShapePicker = function (
    x,
    y,
    source,
    callback,
    direction,
    afterClick,
    cells,
    hovering,
    getInsertLocationFn,
    showEdges,
    startEditing
  ) {
    EditorUi.debug('rdf-diagram.createShapePicker', {
      x,
      y,
      source,
      cells,
      hovering
    })

    // add predicate to connection edge
    if (source != null) {
      afterClick = function (evt) {
        self.graph.model.setValue(
          source.edges[source.edges.length - 1],
          ':has'
        )
        self.ui.hideShapePicker()
      }
    }

    return self.ui.createShapePicker_origin(
      x,
      y,
      source,
      callback,
      direction,
      afterClick,
      cells,
      hovering,
      getInsertLocationFn,
      showEdges,
      startEditing
    )
  }

  // control shapes at cell shape picker
  this.ui.getCellsForShapePicker = function (cell, hovering, showEdges) {
    EditorUi.debug('rdf-diagram.getCellsForShapePicker', {
      cell,
      hovering,
      showEdges
    })
    const cells = []

    // TODO use default styles
    // May create cells from XML?!
    // cells = self.editorUi.importXml('<xml ...>')

    // Creates a clone of the source cell and moves it to the origin
    if (cell != null) {
      try {
        cell = graph.cloneCell(cell)

        if (graph.model.isVertex(cell) && cell.geometry != null) {
          cell.geometry.x = 0
          cell.geometry.y = 0
        }
        cells.push(cell)
      } catch (e) {
        cell = null
      }
    }

    const listCell = new mxCell(
      '<b>ex:Resource</b>\na rdfs:Class',
      new mxGeometry(0, 0, 160, 110),
      `swimlane;fontStyle=0;childLayout=stackLayout;horizontal=1;startSize=38;horizontalStack=0;resizeParent=1;resizeParentMax=0;resizeLast=0;
            collapsible=1;marginBottom=0;whiteSpace=wrap;html=1;fontSize=12;points=[[0,0,0,0,19],[1,0,0,0,19]];swimlaneLine=1;snapToPoint=1;
            strokeColor=default;fillColor=default;`
    )
    listCell.vertex = true
    const listItem = graph.createVertex(
      null,
      null,
      'rdfs:label "My Resource"',
      0,
      0,
      120,
      38,
      `text;strokeColor=none;fillColor=none;align=left;verticalAlign=middle;spacingLeft=4;spacingRight=4;
            overflow=hidden;points=[[0,0.5],[1,0.5]];portConstraint=eastwest;rotatable=0;whiteSpace=wrap;html=1;connectable=0;swimlaneLine=1;`,
      false
    )
    listCell.insert(listItem)

    cells.push(
      graph.createVertex(
        null,
        null,
        '',
        0,
        0,
        38,
        38,
        'ellipse;whiteSpace=wrap;html=1;',
        false
      ),
      graph.createVertex(
        null,
        null,
        '<b>ex:Resource',
        0,
        0,
        120,
        38,
        'rounded=0;whiteSpace=wrap;html=1;',
        false
      ),
      graph.createVertex(
        null,
        null,
        '<b>ex:Resource</b>\na rdfs:Class',
        0,
        0,
        120,
        38,
        'rounded=0;whiteSpace=wrap;html=1;',
        false
      ),
      listCell
    )

    console.log('Cells for Shape Picker: ', { cells })
    return cells
  }
}

RdfDiagram.prototype.getTextLabelOfCell = function (cell) {
  const span = document.createElement('span')
  span.innerHTML = this.graph.getLabel(cell)

  return span.textContent || span.innerText
}

RdfDiagram.prototype.initChangeDetections = function (evt) {
  const self = this
  const changes = evt.getProperty('edit').changes
  EditorUi.debug('rdf-diagram.changeDetection', { changes })

  // no actions required for following changes
  if (
    changes.every(
      (a) => a instanceof mxGeometryChange || a instanceof mxStyleChange
    )
  ) {
    return
  }

  if (changes.some(a =>
    a instanceof mxValueChange ||
    a instanceof mxChildChange ||
    a instanceof mxTerminalChange
  )) {
    self.doRDFCheck()
  }

  const handlings = changes.reduce(function (result, change) {
    if (change instanceof mxValueChange) {
      result.push(() => self.handleValueChange(change, evt))
    } else if (
      change instanceof mxChildChange &&
      !self.graph.model.isEdge(change.child)
    ) {
      // result.push(() => self.handleMoveNodeFromToList(change, evt))
    }
    return result
  }, [])

  // collect handlings to exec in one graph update
  if (handlings.length) {
    setTimeout(() => {
      self.graph.getModel().beginUpdate()
      try {
        handlings.forEach((hx) => hx())
      } finally {
        self.graph.getModel().endUpdate()
      }
    })
  }
}

RdfDiagram.prototype.handleValueChange = function (change, evt) {
  const self = this
  EditorUi.debug('rdf-diagram.handleValueChange', { change })
  const text = self.getTextLabelOfCell(change.cell)

  if (!text) return
  if (mxUtils.isNode(change.cell.value) && change.cell.hasAttribute('link')) { return }

  if (text.startsWith('<http') && text.endsWith('>')) {
    self.graph.setAttributeForCell(
      change.cell,
      'link',
      text.substring(1, text.length - 1)
    )
  }
}

RdfDiagram.prototype.handleMoveNodeFromToList = function (change, evt) {
  const self = this
  EditorUi.debug('rdf-diagram.handleMoveNodeFromOrToStack', { change, evt })
  const nodeLabel = self.getTextLabelOfCell(change.child)

  // TODO use global default styles
  const stackStyle =
    'swimlane;fontStyle=0;childLayout=stackLayout;horizontal=1;startSize=38;horizontalStack=0;resizeParent=1;resizeParentMax=0;resizeLast=0;collapsible=1;marginBottom=0;whiteSpace=wrap;html=1;fontSize=12;points=[[0,0,0,0,0],[0,0,0,0,19],[0.25,0,0,0,0],[0.5,0,0,0,0],[0.75,0,0,0,0],[1,0,0,0,0],[1,0,0,0,19]];swimlaneLine=1;snapToPoint=1;'

  const nodeStyle =
    'rounded=0;swimlaneLine=1;overflow=hidden;html=1;strokeColor=default;fillColor=default;align=center;spacing=2;spacingTop=2;spacingLeft=2;spacingBottom=2;spacingRight=2'

  const stackPropStyle =
    'text;strokeColor=none;fillColor=none;align=left;verticalAlign=middle;spacingLeft=4;spacingRight=4;overflow=hidden;points=[[0,0.5],[1,0.5]];portConstraint=eastwest;rotatable=0;whiteSpace=wrap;html=1;connectable=0;swimlaneLine=1'

  const stackSubStackPropStyle =
    'swimlane;fontStyle=0;childLayout=stackLayout;horizontal=1;startSize=30;horizontalStack=0;resizeParent=1;resizeParentMax=0;resizeLast=0;collapsible=1;marginBottom=0;whiteSpace=wrap;html=1;rounded=0;align=left;strokeColor=none;swimlaneLine=0;fillColor=none;spacingLeft=4;spacingRight=4;dashed=1;connectable=0;verticalAlign=middle;swimlaneBody=1;snapToPoint=1;padding=10;paddingTop=10;margin=10;marginLeft=10;marginRight=10;marginBottom=10'

  const ellipseStyle = 'ellipse;whiteSpace=wrap;html=1;'

  // var edgeStyle = self.graph.createCurrentEdgeStyle()
  const edgeStyle = 'rounded=0;orthogonalLoop=1;jettySize=auto;html=1;exitX=1;exitY=0;exitDx=0;exitDy=19;exitPerimeter=0;edgeStyle=orthogonalEdgeStyle;'

  const htmlEntities = function (str) {
    return String(str)
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/"/g, '&quot;')
  }

  const iamPO = function (node) {
    // TODO this only works if node turtle is good and ends with "."; improvement required
    const label = self.getTextLabelOfCell(node).trim()
    return label.includes(' ') && !label.endsWith('.')
  }

  const fromStackToRoot = function (node, prevStack) {
    let edgeFrom = null
    let edgeLabel = ''

    if (iamPO(node)) {
      edgeFrom = prevStack
      edgeLabel = nodeLabel.split(' ')[0] || ':p'
      const olabel = nodeLabel.split(' ').slice(1).join(' ') || nodeLabel
      self.graph.model.setValue(node, htmlEntities(olabel))
    } else {
      // I am a Subject or Object, use previous predicate ...
      if (prevStack.parent == null) {
        // previous parentStack is empty and was removed by drawio
        // find prev event stack
        const prevChange = evt
          .getProperty('edit')
          .changes.find(
            (a) =>
              a instanceof mxChildChange &&
              a.child.getId() == prevStack.getId()
          )
        if (prevChange) {
          edgeFrom = prevChange.previous
        } else {
          console.error(
            'Error: could not find prev ChildChange event to drag the property',
            { change }
          )
        }
      } else {
        edgeFrom = prevStack.parent
      }
      edgeLabel = self.getTextLabelOfCell(prevStack) || ':p'
    }

    if (edgeFrom) {
      self.graph.insertEdge(
        null,
        null,
        htmlEntities(edgeLabel),
        edgeFrom,
        node,
        edgeStyle
      )
    }

    if (self.isBlanknode(node) && node.children == null) {
      node.geometry.width = 38
      self.graph.setCellStyle(ellipseStyle, [node])
    } else if (self.isStackContainer(node)) {
      self.graph.setCellStyle(stackStyle, [node])
    } else {
      self.graph.setCellStyle(nodeStyle, [node])
    }
  }

  const fromRootToStack = function (node, stack) {
    let plabel = ':p'
    const edge = self.graph
      .getModel()
      .getEdges(stack)
      .find((a) => a.target.getId() == node.getId())

    if (edge) {
      plabel = self.getTextLabelOfCell(edge) || plabel
      self.graph.model.remove(edge)
    }

    if (self.isStackContainer(node) || self.isBlanknode(node)) {
      const subStackProp = self.graph.createVertex(stack, null, htmlEntities(plabel), 0, 0, 60, 40, stackSubStackPropStyle)
      self.graph.addCell(subStackProp, stack, change.previousIndex)
      self.graph.addCell(node, subStackProp)
      self.graph.setCellStyle(stackStyle, [node])
    } else {
      self.graph.setCellStyle(stackPropStyle, [node])
      self.graph.model.setValue(
        node,
        `${htmlEntities(plabel)} ${htmlEntities(nodeLabel)}`
      )
    }
  }

  const hasId = (node, id) => node && node.getId() == id

  const hasNotId = (node, id) => node && node.getId() != id

  // TODO handle lists (in container)
  // TODO if new items has inline turtle
  // TODO handle if node has other relations

  // moved from STACK TO ROOT
  if (
    hasId(change.parent, '1') &&
    hasNotId(change.previous, '1') &&
    self.isStackContainer(change.previous)
  ) {
    fromStackToRoot(change.child, change.previous)
  }
  // moved from ROOT 2 LIST
  else if (
    hasNotId(change.parent, '1') &&
    self.isStackContainer(change.parent) &&
    hasId(change.previous, '1')
  ) {
    fromRootToStack(change.child, change.parent)
  }
  // moved from LIBRARY to LIST
  else if (
    change.previous == null &&
    hasNotId(change.parent, '1') &&
    self.isStackContainer(change.parent)
  ) {
    const changesWith = evt
      .getProperty('edit')
      .changes.filter(
        (a) =>
          a instanceof mxChildChange && a.child.getId() == change.child.getId()
      )
    // required check, to exclude if property was just duplicated (and therefore previous is null)
    if (changesWith.length > 1) {
      fromRootToStack(change.child, change.parent)
    }
  } else if (
    change.parent &&
    change.previous &&
    change.parent.getId() == change.previous.getId()
  ) {
    // console.log('May handle: moved node inside a stack ...');
  } else {
    console.log('Warning: Unhandled ChildChange event', { change, evt })
  }
}

RdfDiagram.prototype.getXml = function () {
  const selectionOnly = false
  const noPages = false
  const uncompressed = true
  const xmlData =
      Graph.xmlDeclaration +
      '\n' +
      this.editorUi.getFileData(
        true,
        null,
        null,
        null,
        !selectionOnly,
        !noPages,
        null,
        null,
        null,
        uncompressed
      )

  EditorUi.debug('rdf-diagram.getXml', { xmlData })
  return xmlData
}

RdfDiagram.prototype.doRDFCheck = function () {
  const self = this

  if (!self.liveRDFCheckEnabled) return

  const run = function () {
    // TODO requires fixes (eg reloading the window, user defined highlights ...)
    if (self.prevBadCell) {
      self.unHighlightCell(self.prevBadCell)
    }

    try {
      const rdf = self.convert2RDF(self.getXml())
      self.graph.model.fireEvent(new mxEventObject('didRDFCheck', 'result', rdf))
      EditorUi.debug('rdf-diagram.doRDFCheck result', { result: rdf })
    } catch (error) {
      self.graph.model.fireEvent(new mxEventObject('didRDFCheck', 'result', error))
      EditorUi.debug('rdf-diagram.doRDFCheck error', { error })
      const badCell = self.graph.model.getCell(error.element_id)
      self.prevBadCell = badCell
      self.highlightCell(badCell)
    }
  }

  // TODO may run with delay?! .. (to allow multiple changes without running convert)
  run()
}

RdfDiagram.prototype.highlightCell = function (cell) {
  // this.graph.setCellStyles(mxConstants.STYLE_SHADOW, 1, [cell])
  this.graph.setCellStyles(mxConstants.STYLE_FONTCOLOR, '#CC0000', [cell])
}

RdfDiagram.prototype.unHighlightCell = function (cell) {
  // this.graph.setCellStyles(mxConstants.STYLE_SHADOW, 0, [cell])
  this.graph.setCellStyles(mxConstants.STYLE_FONTCOLOR, '#000000', [cell])
}

RdfDiagram.prototype.initRdfOutputWindow = function (x, y, w, h) {
  const self = this
  const outputWindow = function () {}

  const parentDiv = document.createElement('div')
  parentDiv.style.position = 'absolute'
  parentDiv.style.width = '100%'
  parentDiv.style.height = '100%'
  parentDiv.style.overflow = 'hidden'

  const turtleTextarea = document.createElement('textarea')
  turtleTextarea.setAttribute('wrap', 'off')
  turtleTextarea.setAttribute('spellcheck', 'false')
  turtleTextarea.setAttribute('autocorrect', 'off')
  turtleTextarea.setAttribute('autocomplete', 'off')
  turtleTextarea.setAttribute('autocapitalize', 'off')
  turtleTextarea.style.overflow = 'auto'
  turtleTextarea.style.resize = 'none'
  turtleTextarea.style.width = '100%'
  turtleTextarea.style.height = 'calc(100% - 50px)'
  turtleTextarea.style.marginBottom = '5px'
  parentDiv.appendChild(turtleTextarea)

  const errorWrapper = document.createElement('div')
  errorWrapper.style.width = '100%'
  errorWrapper.style.style = 'none'
  errorWrapper.style.padding = '10px'
  errorWrapper.style.boxSizing = 'border-box'
  parentDiv.appendChild(errorWrapper)

  const helpButton = this.editorUi.menus.createHelpLink('https://gitlab.com/infai/rdf-diagram-framework')
  helpButton.style.position = 'absolute'
  helpButton.style.margin = '10px'
  helpButton.style.right = '0'
  helpButton.style.bottom = '0'
  parentDiv.appendChild(helpButton)

  const formatSelect = document.createElement('select')
  formatSelect.className = 'geBtn'

  const turtleOption = document.createElement('option')
  turtleOption.setAttribute('value', 'turtle')
  mxUtils.write(turtleOption, mxResources.get('turtle'))
  turtleOption.setAttribute('selected', 'selected')
  formatSelect.appendChild(turtleOption)
  parentDiv.appendChild(formatSelect)

  mxResources.parse('rdfOutput=RDF Output')
  outputWindow.window = new mxWindow(mxResources.get('rdfOutput'), parentDiv, x, y, w, h, true, true)
  outputWindow.window.minimumSize = new mxRectangle(0, 0, 212, 120)
  outputWindow.window.destroyOnClose = false
  outputWindow.window.setMaximizable(false)
  outputWindow.window.setResizable(true)
  outputWindow.window.setClosable(true)

  outputWindow.window.addListener('show', mxUtils.bind(this, function () {
    outputWindow.window.fit()
    if (!self.liveRDFCheckEnabled) {
      this.toolbarContainer.querySelector('input').click()
    } else {
      this.doRDFCheck()
    }
  }))

  const printError = function (error) {
    const badCellId = error.element_id
    errorWrapper.innerText = ''
    errorWrapper.insertAdjacentHTML('beforeend', `<p><b>Error:</b><br />${error.message}</p>`)

    const badCell = self.graph.model.getCell(badCellId)
    const badCellBtn = document.createElement('button')
    badCellBtn.appendChild(document.createTextNode(`Select element ${badCellId}`))
    badCellBtn.setAttribute('title', `Select cell ${badCellId}`)
    badCellBtn.style.padding = '10px'
    errorWrapper.append(badCellBtn)

    mxEvent.addListener(badCellBtn, 'click', mxUtils.bind(this, function () {
      self.graph.getSelectionModel().setCell(badCell)
    }))
  }

  const toggleWrapperVisibility = function (type = 'result') {
    if (type == 'result') {
      formatSelect.style.display = 'inline'
      errorWrapper.style.display = 'none'
      turtleTextarea.style.display = 'block'
    } else {
      turtleTextarea.style.display = 'none'
      formatSelect.style.display = 'none'
      errorWrapper.style.display = 'block'
    }
  }

  this.graph.model.addListener('didRDFCheck', mxUtils.bind(self, function (sender, evt) {
    const result = evt.getProperty('result')
    // TODO this changes if return is JSON
    if (typeof result === 'object') {
      toggleWrapperVisibility('error')
      printError(result)
    } else {
      toggleWrapperVisibility()
      turtleTextarea.value = result
    }
  }))

  this.editorUi.installResizeHandler(outputWindow, true)

  return outputWindow
}

Draw.loadPlugin(function (ui) {
  new RdfDiagram(ui)
})
