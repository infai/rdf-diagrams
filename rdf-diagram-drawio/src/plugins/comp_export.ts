import { RdfDiagramComponent } from './rdf_diagram_component';

export class RdfExport extends RdfDiagramComponent {
    run() {
        console.log('Greetings from RDfExport  ...', {
            self: this,
        });
        this.createMenu();
    }

    createMenu() {
        EditorUi.debug('rdf-diagram.createUi', [this]);
        const self = this;

        mxResources.parse('exportRDF=RDF...');
        const exportAsMenu = this.editorUi.menus.get('exportAs');
        const oldExportAsfunct = exportAsMenu.funct;

        this.editorUi.actions.addAction('exportRDF', function () {
            self.exportRDFAction();
        });

        exportAsMenu.funct = function (menu: any, parent: any) {
            oldExportAsfunct.apply(this, arguments);
            self.editorUi.menus.addMenuItem(menu, 'exportRDF', parent);
        };
    }

    async exportRDFAction() {
        const self = this;

        const div = document.createElement('div');
        div.style.whiteSpace = 'nowrap';
        const hd = document.createElement('h3');
        mxUtils.write(hd, mxResources.get('formatRdf'));
        hd.style.cssText =
            'width:100%;text-align:center;margin-top:0px;margin-bottom:4px';
        div.appendChild(hd);

        const selectionOnly = self.editorUi.addCheckbox(
            div,
            mxResources.get('selectionOnly'),
            false,
            self.editorUi.editor.graph.isSelectionEmpty(),
        );
        const comporessed = self.editorUi.addCheckbox(
            div,
            mxResources.get('compressed'),
            Editor.defaultCompressed,
            null,
        );

        const formatSelect = document.createElement('select');
        formatSelect.style.cssText = 'margin-right: 8px; margin-top: 16px;';
        const option = document.createElement('option');
        option.value = 'ttl';
        mxUtils.write(option, mxResources.get('RDF/Turtle'));
        option.setAttribute('selected', 'selected');
        formatSelect.appendChild(option);
        div.appendChild(formatSelect);

        const dlg = new CustomDialog(
            self.editorUi,
            div,
            mxUtils.bind(this, async function () {
                try {
                    const rdf = await self.getRDF(
                        self.getXml({
                            selectionOnly: selectionOnly.checked,
                            uncompressed: !comporessed.checked,
                        }),
                    );
                    self.editorUi.saveData(
                        `${self.editorUi.getBaseFilename(true)}.ttl`,
                        'ttl',
                        rdf,
                        'text/turtle',
                    );
                } catch (error) {
                    self.editorUi.alert(
                        `rdf-diagram convert error:\n${error.toString()}\nSee console for details`,
                    );
                }
            }),
            null,
            mxResources.get('export'),
        );

        self.editorUi.showDialog(dlg.container, 300, 200, true, true);
    }
}
