declare type RdfLibError = {
    element_id: string,
    message: string
    stack: string
}
