import { RdfDiagramComponent } from './rdf_diagram_component';

export class LiveRDFSyntaxCheck extends RdfDiagramComponent {
    liveCheckEnabled: boolean = true;

    statusWrapper: HTMLElement;

    run(): void {
        const self = this;

        this.ui();

        this.graph.model.addListener(
            mxEvent.CHANGE,
            mxUtils.bind(
                self,
                function (sender: MxGraphModel, evt: mxEventObject) {
                    self.changeDetection(evt);
                },
            ),
        );
    }

    ui() {
        const self = this;

        const seperator = document.createElement('div');
        seperator.className = 'geSeparator';

        const wrapper = document.createElement('div');
        wrapper.className = 'geLabel rdfToolbarContainer';
        wrapper.style.cssText = 'display:inline-flex';
        wrapper.title = 'live RDF syntax check';

        this.editorUi.toolbar.container.appendChild(seperator);
        this.editorUi.toolbar.container.appendChild(wrapper);
        // menubar.appendChild(this.rdfrdfStatusContainer)

        // const checkbox = this.editorUi.addCheckbox(
        //     wrapper,
        //     mxResources.get('live RDF'),
        //     !(urlParams.test === '1' || urlParams.dev === '1'),
        //     null,
        // );
        // checkbox.title = mxResources.get('live RDF syntax check');
        // checkbox.style.marginTop = '3px';
        // mxEvent.addListener(
        //     checkbox,
        //     'change',
        //     mxUtils.bind(this, function (evt) {
        //         self.liveCheckEnabled = checkbox.checked;
        //         if (checkbox.checked) {
        //             self.updateRDF();
        //         }
        //     }),
        // );

        this.statusWrapper = document.createElement('span');
        wrapper.appendChild(this.statusWrapper);

        const errorWrapper = document.createElement('span');
        errorWrapper.style.color = '#b62623';
        errorWrapper.innerText = 'Syntax Error';

        const errorBtn = document.createElement('button');
        errorBtn.setAttribute('style', `
            color: "#b62623";
            margin: 0 4px;
            display: inline-block;
            border: 1px solid #b62623;
            padding: 1px 8px;
            border-radius: 4px;
            color: #b62623;
            cursor: pointer;
        `);
        errorBtn.innerText = 'Details...';
        errorBtn.title = 'Click to view error details';
        errorWrapper.append(errorBtn);

        mxEvent.addListener(
            errorBtn,
            'click',
            mxUtils.bind(this, function (evt) {
                self.editorUi.actions.get('toggleRdfPanel').funct(evt);
            }),
        );

        this.graph.model.addListener(
            'getRDF',
            mxUtils.bind(
                this,
                function (sender: MxGraphModel, evt: mxEventObject) {
                    const result = evt.getProperty('result');
                    if (result) {
                        self.setStatus('Syntax OK');
                    } else {
                        self.setStatus(errorWrapper);
                    }
                },
            ),
        );
    }

    setStatus(status: string | HTMLElement) {
        this.statusWrapper.innerText = '';
        if (status instanceof HTMLElement) {
            this.statusWrapper.append(status);
        } else {
            this.statusWrapper.textContent = `RDF: ${status}`;
        }
    }

    changeDetection(evt: mxEventObject) {
        const changes = evt.getProperty('edit').changes;

        if (
            changes.some(
                (a: any) =>
                    a instanceof mxValueChange ||
                    a instanceof mxChildChange ||
                    a instanceof mxTerminalChange,
            )
        ) {
            if (this.liveCheckEnabled) {
                this.updateRDF();
            }
        }
    }
}
