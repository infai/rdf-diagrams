import { Autocomplete } from './comp_autocomplete';
import { LiveRDFSyntaxCheck } from './comp_liveCheck';
import { RdfExport } from './comp_export';
import { RdfPanel } from './comp_panel';
import { RdfDiagramComponent } from './rdf_diagram_component';
import { HighlightBadNodes } from './comp_highlight';
// import { ShapePicker } from './comp_shapePicker';

export class RdfDiagramPlugin {
    graph: DrawioGraph;

    constructor(editorUi: DrawioUI) {
        const self = this;

        console.log('Greetings from RdfDiagramPlugin', { self });

        this.graph = editorUi.editor.graph;

        (async () => {
            const { RdfAutoComplete } = await import('rdf-diagram-lib');
            const rdfDiagramManager = RdfAutoComplete.new();
            this.addComponent(new Autocomplete(editorUi, rdfDiagramManager));
            this.addComponent(
                new HighlightBadNodes(editorUi, rdfDiagramManager),
            );
            // this.addComponent(new ShapePicker(editorUi, rdfDiagramManager));
            this.addComponent(new RdfPanel(editorUi, rdfDiagramManager));
            this.addComponent(new RdfExport(editorUi, rdfDiagramManager));
            this.addComponent(
                new LiveRDFSyntaxCheck(editorUi, rdfDiagramManager),
            );
        })();

        this.initUi();

        // self.editorUi.addListener('clientLoaded', mxUtils.bind(this, function(sender: any, evt: any) {
        //     self.updateRDF();
        // }))
        // self.editorUi.editor.addListener('fileLoaded', function() {
        //     self.updateRDF();
        // });
    }

    private addComponent(component: RdfDiagramComponent): void {
        const const_component = component;
        const_component.run();
    }

    private initUi() {
        this.graph.setAllowDanglingEdges(true);
        this.graph.setDisconnectOnMove(false);
        this.graph.setCloneInvalidEdges(false);
    }
}
