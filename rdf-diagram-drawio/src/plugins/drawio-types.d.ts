declare const Draw: {
    loadPlugin: (handler: (ui: DrawioUI) => void) => void;
};

declare const Graph: {
    xmlDeclaration: string;
};

declare const EditorUi: {
    logError: (
        arg0: string,
        arg1: null,
        arg2: null,
        arg3: null,
        error: any,
    ) => unknown;
    defaultCompressed: (
        div: HTMLDivElement,
        arg1: string,
        defaultCompressed: any,
    ) => unknown;
    debug: (name: string, data: any) => void;
};

declare const log: any;
declare class mxCellHighlight {
    constructor(graph: DrawioGraph, color: string, arg: number);

    public highlight(arg: DrawioCellState | null): void;
    public destroy(): void;
}

declare class mxWindow {
    setVisible(arg0: boolean): void;
    isVisible(): boolean;
    window: any;
    fit(): void;
    addListener(arg0: string, arg1: void): void;
    constructor(
        resource: mxResources,
        parent: HTMLElement,
        x: number,
        y: number,
        w: number,
        h: number,
        arg7: boolean,
        arg8: boolean,
    );
    setClosable(arg0: boolean): void;
    setResizable(arg0: boolean): void;
    setMaximizable(arg0: boolean): void;
    destroyOnClose: boolean;
    minimumSize: mxRectangle;
}

declare class mxRectangle {
    constructor(x: number, y: number, w: number, h: number);
}

declare class mxResources {
    static parse(value: string): void;
    static get(key: string): string;
}

declare class mxMouseEvent {
    public readonly graphX: number;
    public readonly graphY: number;
}

declare class mxEventObject {
    constructor(name: string, key?: string, value?: any);
    getProperty(key: string): any;
}

declare class MxGraphModel {}

declare const mxEvent: {
    CLICK: any;
    addListener(toggleRdfOutputBtn: HTMLElement, arg1: string, arg2: void): unknown;
	isConsumed(evt: KeyboardEvent|mxEventObject): unknown;
    consume(evt: KeyboardEvent|mxEventObject): unknown;
    DOUBLE_CLICK: string;
    CHANGE: string;
};

declare const mxUtils: {
    getTextContent(documentElement: HTMLElement): string;
    parseXml(arg0: any): unknown;
    createImage(arg0: string): HTMLElement;
	write(turtleOption: HTMLElement, arg1: string): unknown;
    bind(arg0: any, arg1: (evt: mxEventObject) => void): void
    bind(arg0: any, arg1: (sender: MxGraphModel, evt: mxEventObject) => void): void
	isNode(node: any): node is HTMLElement;
	createXmlDocument(): XMLDocument;
};

declare const mxLog: {
    debug: (arg0: string) => unknown;
    show: () => void;
    isVisible: () => unknown;
    window: any;
};

declare interface DrawioUI {
    hideShapePicker(): unknown;
    getCellsForShapePicker_origin: any;
    createShapePicker_origin: any;
    createShapePicker: any;
    getCellsForShapePicker: any;
    addListener(name: string, handler:void): void;
    buttonContainer: any;
    toolbarContainer: HTMLElement;
    toggleFormatElement: HTMLElement;
    getSelectionState: () => unknown;
    onKeyUp: any;
    onKeyDown: any;
    saveData: (arg0: string, arg1: string, rdf: any, arg3: string) => unknown;
    alert: (arg0: string) => unknown;
    getBaseFilename: (arg0: boolean) => string;
    spinner: any;
    toolbar: any;
    addCheckbox: (
        parent: HTMLElement,
        label: string,
        checked: boolean,
        arg3: any,
    ) => HTMLInputElement;
    getFileData: (
        arg0: boolean,
        arg1: null,
        arg2: null,
        arg3: null,
        arg4: boolean,
        arg5: boolean,
        arg6: null,
        arg7: null,
        arg8: null,
        uncompressed: boolean,
    ) => unknown;
    fireEvent: (arg0: any) => unknown;
    fileNode: Element | null;
    hideDialog: () => void;
    showDialog: (...args: any[]) => void;
    editor: DrawioEditor;
    actions: DrawioActions;
    menus: DrawioMenus;
    importLocalFile: (args: boolean) => void;
}

interface DrawioMenus {
    createHelpLink: (arg0: string) => HTMLElement;
    get: (name: string) => any;
    addMenuItems: (menu: any, arg: any, arg2: any) => void;
    addMenuItem: (menu: any, arg: any, arg2: any) => void;
}

interface DrawioAction {
    setSelectedCallback: (arg0: () => boolean) => unknown;
    setToggleAction: (arg0: boolean) => void;
}

interface DrawioActions {
    addAction: (name: string, action: () => void) => DrawioAction;
    get: (name: string) => { funct: (evt: mxEventObject) => void };
}

declare interface DrawioEditor {
    loadUrl(url: any, arg1: void): unknown;
    addListener(arg0: string, update: any): unknown;
    setStatus(arg0: string): unknown;
	graph: DrawioGraph;
}

declare interface mxView {
    getState: (arg0: any) => unknown;
}

declare interface DrawioGraph {
    cloneCell(cell: any): any;
    addCellOverlay(cell: DrawioCell, overlay: mxCellOverlay): unknown;
    removeCellOverlays(arg0: DrawioCell): unknown;
    setCellWarning(node: DrawioCell, arg1: string, icon?: mxImage): unknown;
    setCellStyles(STYLE_FONTCOLOR: string, arg1: string|number, arg2: DrawioCell[]): unknown;
	getView(): mxView;
	cellEditor: any;
	getSelectedEditingElement(): HTMLElement;
	container: any;
	isSelectionEmpty(): any;
	addSelectionCell(namespace: any): unknown;
	selectCells(arg0: boolean, arg1: boolean, testContainer: DrawioCell): unknown;
	getDefaultParent(): any;
	getModel(): any;
	setCloneInvalidEdges(arg0: boolean): unknown;
	setDisconnectOnMove(arg0: boolean): unknown;
	setAllowDanglingEdges(arg0: boolean): unknown;
	defaultThemeName: string;
	insertVertex(arg0: undefined, arg1: null, label: string, arg3: number, arg4: number, arg5: number, arg6: number, arg7: string): void;
	addListener: any;
	model: DrawioGraphModel;
	getLabel(cell: DrawioCell): string;
    getSelectionModel(): DrawioGraphSelectionModel;
    view: DrawioGraphView;

    addMouseListener: (listener: {
        mouseMove?: (graph: DrawioGraph, event: mxMouseEvent) => void;
        mouseDown?: (graph: DrawioGraph, event: mxMouseEvent) => void;
        mouseUp?: (graph: DrawioGraph, event: mxMouseEvent) => void;
    }) => void;
}

declare interface DrawioGraphView {
    getState: (cell: DrawioCell) => DrawioCellState;
    canvas: SVGElement;
}

declare interface MxCellState {
    cell: DrawioCell;
}

declare interface DrawioCellState {
    cell: DrawioCell;
    x: number;
    y: number;
    width: number;
    height: number;
}

declare interface DrawioGraphSelectionModel {
    addListener: (event: string, handler: (...args: any[]) => void) => void;
    cells: DrawioCell[];
}

declare interface DrawioCell {
    id: string;
    style: string;
}

declare interface DrawioGraphModel {
    getCell(nodeId: string): unknown;
    // |((evt: mxEventObject) => void)
    addListener: (arg0: string, arg1: void) => unknown;
    fireEvent: (arg0: mxEventObject) => unknown;
    setValue: (c: DrawioCell, label: string | any) => void;
    beginUpdate: () => void;
    endUpdate: () => void;
    cells: Record<any, DrawioCell>;
    setStyle: (cell: DrawioCell, style: string) => void;
    isVertex: (cell: DrawioCell) => boolean;
}

declare class UserObject {}

declare class mxValueChange {
    cell: DrawioCell;
    model: MxGraphModel;
    previous: string | UserObject;
    value: string | UserObject;
}

declare class mxChildChange {
    child: DrawioCell;
    parent: DrawioCell;
    previous: DrawioCell;
}

declare class mxTerminalChange {}

declare const urlParams: {
    [key: string]: string;
};

declare const Editor: {
    defaultCompressed: boolean;
};

declare class CustomDialog {
    container(
        container: any,
        arg1: number,
        arg2: number,
        arg3: boolean,
        arg4: boolean,
    ): void;
    constructor(
        editorUi: DrawioUI,
        content: HTMLElement,
        funct: void,
        dontknow: void,
        mxResources: string,
    );
}

declare const RdfConverter: {
    convert_to_turtle(xmlData: any):any;
}

declare const mxConstants: {
    ALIGN_MIDDLE: any;
    ALIGN_TOP: any;
    ALIGN_RIGHT: any;
    STYLE_FONTSTYLE: string;
    FONT_UNDERLINE: 4;
    STYLE_FONTCOLOR: string
}

declare class mxImage {
    constructor(src: string, width?: number, height?: number);
    src: string
    width: number
    height: number
}

declare class mxCellOverlay {
    addListener(CLICK: any, arg1: (sender: any, evt: any) => void): void;
    constructor(image: mxImage, tooltip?: string, align?: any, verticalAlign?: any, offset?: any,  cursor?: string)
}
