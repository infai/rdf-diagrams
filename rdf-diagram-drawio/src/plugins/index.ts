import { RdfDiagramPlugin } from './rdf-diagram';

Draw.loadPlugin((editorUi) => {
    const rdfDiagramPlugin = new RdfDiagramPlugin(editorUi);
});
