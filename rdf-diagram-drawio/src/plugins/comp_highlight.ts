/**
 * component to highlight bad elements
 */

import { RdfDiagramComponent } from './rdf_diagram_component';

export class HighlightBadNodes extends RdfDiagramComponent {
    warningImage =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABg2lDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TS0WqDnYQcchQneyiIrppFYpQIdQKrTqYXPoFTRqSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxdnBSdJES/5cUWsR4cNyPd/ced+8AoVFhmtU1C2i6baaTCTGbWxXDrwihD2HMQJCZZcxJUgq+4+seAb7exXmW/7k/R6+atxgQEIlnmWHaxBvEU5u2wXmfOMpKskp8Tjxm0gWJH7muePzGueiywDOjZiY9TxwlFosdrHQwK5ka8SRxTNV0yheyHquctzhrlRpr3ZO/MJLXV5a5TnMYSSxiCRJEKKihjApsxGnVSbGQpv2Ej3/I9UvkUshVBiPHAqrQILt+8D/43a1VmBj3kiIJIPTiOB8jQHgXaNYd5/vYcZonQPAZuNLb/moDmP4kvd7WYkdA/zZwcd3WlD3gcgcYfDJkU3alIE2hUADez+ibcsDALdCz5vXW2sfpA5ChrlI3wMEhMFqk7HWfd3d39vbvmVZ/P3I2cqbbG6iAAAAABmJLR0QA4AAfAMvKW4PVAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH6AUOCAEIBy8jxwAAAudJREFUaN7tmctOFEEUhv8uMYEQXJu4ZePKFRsfwMgTKEvDa/gArlAMREg0XqPiwhhYoBJNTNCQHhwG5TaLwaBDZAgiDoPdVdVd5UJoBLpDX+o4s5h/1526nfPX6a6vG2iqqUyyqAbWWusDE1kWyVyMcvFepQSvshwaUEMHAABQCq59G+7UEKB8smkYVfZFyYbaeAe1OQmxnCNzgWYLCQd8uj+45nYfNP/d+A7sZdj9NA7truzf52Xwzy9JXDDugP9rHXJ+4Mh9MdcPf6vSuA4E2Z9+DCg3pIGEm3to3AVmcvFydRH+12fR7pSfQ64uGg3C3BZSHrg9fGwzPjUI7cvG2UJ7meRLk1Bb9vFxVvMQxffGXDDigHJrEDM3Y7fn+T4op1p/B4Lsz7yAFmvxO8oN8MKYERcyO+BvrkIWhxP3k0uD8H98yxwEy5R9reHaDwCd7qzj2veAjGXAsmwdsVKA/30svXtr4xArs5lcSO+AJ8BztzIXIbcHoCVP3b8ldeHOv4GuzUW2O3Xlw4Hr6t3z4ePtLIAvvEXruW5orXVS8EnlgNrZgpjtN/YyErPXoWo/6WsgOO98HAH8qrEA4NfgTj9JVQss6eK9Sgle6b7xU6X35VEq/Ey2hXYxkUpp8JMlyf4eJpJhdAr8jL+FDmEilZLiJ4tduIcwkex7UkL8jOVAFCZSKQl+skyYSGZDfPy04mCi87oX9VDbhTs4eebs34VGvKGZCUwkK+gY+MlMYGLoIauzFx09E+jomUBLZzoH4+BnpAPK2YbI30hvf9dlWK3tsFrb0dZ1KX1B529AOdvxHQiyXxiFluuot7RcBy+MRrrAwjGxDFkcyjSxk3sK7dag3Rqc3EimsWRxCP5mOTQIKwwTd15dy0RaFDpxuhvtF68ClnXgicRMYyKVovCTUWAi3TnpKH6yJJhY94Lexc9/18yoMJHsnHQIP4MAjGMiWTHs42fwFKL6g0gtql+3TTXV1H/UH7/mNle6zCoiAAAAAElFTkSuQmCC';
    errorImage =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABg2lDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TS0WqDnYQcchQneyiIrppFYpQIdQKrTqYXPoFTRqSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxdnBSdJES/5cUWsR4cNyPd/ced+8AoVFhmtU1C2i6baaTCTGbWxXDrwihD2HMQJCZZcxJUgq+4+seAb7exXmW/7k/R6+atxgQEIlnmWHaxBvEU5u2wXmfOMpKskp8Tjxm0gWJH7muePzGueiywDOjZiY9TxwlFosdrHQwK5ka8SRxTNV0yheyHquctzhrlRpr3ZO/MJLXV5a5TnMYSSxiCRJEKKihjApsxGnVSbGQpv2Ej3/I9UvkUshVBiPHAqrQILt+8D/43a1VmBj3kiIJIPTiOB8jQHgXaNYd5/vYcZonQPAZuNLb/moDmP4kvd7WYkdA/zZwcd3WlD3gcgcYfDJkU3alIE2hUADez+ibcsDALdCz5vXW2sfpA5ChrlI3wMEhMFqk7HWfd3d39vbvmVZ/P3I2cqbbG6iAAAAABmJLR0QA4AAfAMvKW4PVAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH6AUOBzofsxIOBQAABNVJREFUaN7tmV1sFFUUx393ttvuytBgCdpuNFK6rQSqD7TxgY/gE4oPFQoETE0gAbFpShBJq4CCqTZg/YJIFEVMMBIgliJ9KJUnmwIxhmqDhQR2S2vUFm1CsDvpDmx3rg87td1tl1pmuzsmPck87eSe/2/O2XvPPQembMosmUjEIlLKbGAp8BRQCMwBHgJU8xUN+Au4AXQAPwItQoibKQOQUrqAMmAVUi4P9fxByO8n3Olj0HedcHcn8s/fI04efgTH7DzS8gtw5OXj9Obj9HhAiLPAKeCYEEJPCoCUUgUqgJeMQMCrX7xAsP4Exs/nJ7SOUrQEd+laXIsWo6iqHzgMfCKE0CYNQEq5DthuBIPFA9+dJXjwPbjdZy0HZszCXVnNA88uR3G5LgEfCCFOJBRASpkF7AJevdPRQWDvW8hr7Qn9MypzF6C+vpuMwkKAD4FaIcQtywBSyieA3YTDqwOnT6Hv2zmpu4p7x17UlatAUeqBGiHEL/cNIKUsAmpkKPRc/+HPuPvl/qRsjekbt5G5aTPC6WwCdgsh2iYMIKWcDRzAMEr+/vxT7n7xUVL39/T1lWRWbEE4HI3AViFE95ipd4813gBKAg31SRcPcPfoQbTGbwFKTC3/PQJSyq3A/jsdHfRvWJHSkzbzqzNkzJsP8IoQ4sC4EZBSzgeqDV1Hq3s75aVCoHYPRjAIUG1qGzeFKgHPQHMTxtW2+3Y865I/6rnvE/9aOwPnmgE8prb4AFLKJ4FyQ9Mih5RNLPhxHeH+foByU2PcCLwIoF84b/2ETaTd7kP/4WKUxngAqzEMgg0nbVc266dOgpQRjWMBSCmfBnJDvT0Yba22AzDaWgn19gLkmlpHRWAJQMjvx64W8vuitMYCLAAYHH7JdjbY6YvSGgtQABD2X7ctgDH8cQvGAvAAhLs7bQsQ/rUrSmssQCbw7zXQlhG4+VuU1vGKuf+FjQToH7qA21Zs9qNRWgHSRvzeA8xQcr2EfZctO+sr9iYcwPFY7kitoyJwHSAtL9++EcgviNIaC/ATQJrXvgBpc7xRWmMBWgGcNgYYoa11FIAQ4nugy+nxoBQtsewso7yKmS3tzGxpJ6O8ynr6FC/FmZMD0GVqHXMbrUcI3KVrLTtUXyhDmaaiTFNR15VZXs9VugaEiGi8Rzn9NYBr0WJEVrZtUkdkZeNeuDhK45gAQojLwCFFVXFVbLfkVDt+DEPTMDQN7cQxa1+/sgpFVQEOmRrjdyXMi/M5qeueW5vXW7oXJ2TrnLuABw8fRXG7e4BlQogr97zUmy/UCZcLtfrNlKePunMPitsNUBcrPm4tZPZfjmQUFuLesTdl4l273h3qCR0Zqyc0XjH3DtCorlxF+qZtSRefsWEL00tWADSaWpgQgNmLrEFRmjI3biZ9Y/Ig0jdtY/rLFeBwNBHpUHfH1TluY2movW4YqwNnTqPXvjbpaTP9+ZWJaa+PgBgecFy9gravJuG7kzKvKDLgiOR84gYcMSCREZOuFw+ca0Y/+D7ylrVBo8jKxlVZxbRlzyAma8QUAzE85NM0b/DiefSGbzAutUy4tnGVrsG9MIlDvhiQ6DFrbw8hv5/BTh+G38fgDR+yrzfiZFYODu/jOObkkTY0Zs3JSc2YNQ5MygbdUzZlFu0frdXKUlqTtvwAAAAASUVORK5CYII=';

    run() {
        const self = this;

        this.graph.model.addListener(
            'getRDF',
            mxUtils.bind(
                this,
                function (sender: MxGraphModel, evt: mxEventObject) {
                    const error = evt.getProperty('error') as RdfLibError;
                    this.clearHighlights();
                    if (error && error.element_id) {
                        self.highlightCell(error.element_id as string, error);
                    }
                },
            ),
        );

        // this.graph.addListener(mxEvent.SIZE, this.refresh);
    }

    // refresh() { // update overlay/icon positions ... }

    highlightCell(cellId: string, error: RdfLibError) {
        const self = this;
        const cell = this.graph.model.getCell(cellId) as DrawioCell;

        // this.graph.setCellWarning(node, `RDF Syntax Error: ${error.message}`, new mxImage(this.errorImage, 24, 24));
        const overlay = new mxCellOverlay(
            new mxImage(this.errorImage, 24, 24),
            `Syntax Error: ${error.message}`,
            mxConstants.ALIGN_RIGHT,
            mxConstants.ALIGN_MIDDLE,
            null,
            'pointer',
        );
        this.graph.addCellOverlay(cell, overlay);
        cell.style = cell.style + "fillColor=#ffe6cc;strokeColor=#d6b656;";

        overlay.addListener(mxEvent.CLICK, function (sender, evt) {
            self.editorUi.actions.get('toggleRdfPanel').funct(evt);
        });

        // this.graph.setCellStyles(mxConstants.STYLE_FONTCOLOR, '#CC0000', [node])
        // this.graph.setCellStyles(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_UNDERLINE, [node])
        // this.prevBadCell = node
    }

    clearHighlights() {
        // if (this.prevBadCell) {
        //     this.graph.setCellStyles(mxConstants.STYLE_FONTCOLOR, '#000000', [this.prevBadCell])
        //     this.graph.setCellStyles(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_UNDERLINE, [this.prevBadCell])
        // }

        const cells = this.graph.model.cells;

        for (const id in cells) {
            // this.graph.setCellWarning(cells[id], null)
            this.graph.removeCellOverlays(cells[id]);
        }
    }
}
