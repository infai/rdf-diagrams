import { RdfAutoComplete } from 'rdf-diagram-lib';

export class RdfDiagramComponent {
    constructor(editorUi: DrawioUI, rdfAutoCompletion: RdfAutoComplete) {
        this.graph = editorUi.editor.graph;
        this.editorUi = editorUi;
        this.rdfAutoComplete = rdfAutoCompletion;
    }

    graph: DrawioGraph;
    editorUi: DrawioUI;
    rdfAutoComplete: RdfAutoComplete;

    run(): void {}

    getXml({
        selectionOnly = false,
        uncompressed = true,
        noPages = false,
    }: {
        selectionOnly?: boolean;
        uncompressed?: boolean;
        noPages?: boolean;
    }) {
        const self = this;
        const xmlData =
            Graph.xmlDeclaration +
            '\n' +
            self.editorUi.getFileData(
                true,
                null,
                null,
                null,
                !selectionOnly,
                !noPages,
                null,
                null,
                null,
                uncompressed,
            );

        EditorUi.debug('RdfDiagram.getXml', { xmlData });
        return xmlData;
    }

    async getRDF(model?: string): Promise<string> {
        let rdf;
        try {
            model = model || this.getXml({});
            rdf = await this.rdfAutoComplete.update_and_export_to_turtle(model);
            this.graph.model.fireEvent(
                new mxEventObject('getRDF', 'result', rdf),
            );
            EditorUi.debug('RdfDiagram.getRDF', { rdf });
        } catch (error) {
            this.graph.model.fireEvent(
                new mxEventObject('getRDF', 'error', error),
            );
            EditorUi.logError(
                'RdfDiagram.getRDF error',
                null,
                null,
                null,
                error,
            );
            // throw error
        }
        return rdf;
    }

    updateRDF() {
        this.getRDF();
    }
}
