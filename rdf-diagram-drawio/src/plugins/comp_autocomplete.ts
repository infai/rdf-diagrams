import { RdfDiagramComponent } from './rdf_diagram_component';

export class Autocomplete extends RdfDiagramComponent {
    wrapper: HTMLElement;
    focus: number = -1;
    selectedElement: HTMLElement;

    updateInProgress: Boolean = false;

    delayedSearchTimer: ReturnType<typeof setTimeout>;

    run() {
        const self = this;
        console.log('Greetings from Autocomplete ...', {
            self: this,
        });
        this.initCss();

        this.wrapper = document.createElement('DIV');
        this.wrapper.setAttribute('class', 'rdfd-autocomplete');
        this.wrapper.style.position = 'absolute';
        this.wrapper.style.zIndex = '2';
        this.wrapper.style.fontSize = '14px';
        this.graph.container.appendChild(this.wrapper);

        // origin key handler
        const originKeyDown = this.editorUi.onKeyDown;

        this.editorUi.onKeyDown = function (evt: KeyboardEvent) {
            if (
                !self.graph.cellEditor.isContentEditing() ||
                !self.graph.getSelectedEditingElement()
            ) {
                return;
            }

            self.keyHandler(evt);

            // apply any origin keyhandler, but not Tab to prevent lost focus
            if (!mxEvent.isConsumed(evt) && evt.code != 'Tab') {
                originKeyDown.apply(this, arguments);
            }
        };

        document.addEventListener('click', function (e) {
            self.close();
        });
    }

    initCss() {
        try {
            var style = document.createElement('style');
            style.type = 'text/css';
            style.innerHTML = [
                '.rdfd-autocomplete {',
                'background: white;',
                'border: 1px solid #161616 !important;',
                'box-shadow: 3px 3px 3px 0px #bababa;',
                '}',
                '.rdfd-autocomplete div {',
                'padding: 3px 12px;',
                '}',
                '.rdfd-autocomplete div:hover,',
                '.rdfd-autocomplete .rdfd-autocomplete-active {',
                'background: #f2f2f2;',
                'cursor:pointer',
                '}',
            ].join('\n');
            document.getElementsByTagName('head')[0].appendChild(style);
        } catch (e) {
            // ignore
        }
    }

    keyHandler(evt: KeyboardEvent) {
        const self = this;

        if (evt.code == 'ArrowDown') {
            this.setFocus(++this.focus);
        } else if (evt.code == 'Tab') {
            evt.preventDefault();
            this.setFocus(++this.focus);
        } else if (evt.code == 'ArrowUp') {
            this.setFocus(--this.focus);
        } else if (evt.code == 'Enter') {
            if (this.wrapper.childNodes.length && this.focus >= 0) {
                evt.preventDefault();
                this.getNode(this.focus).click();
            }
        } else {
            if (!this.updateInProgress) {
                clearTimeout(this.delayedSearchTimer);
                this.delayedSearchTimer = setTimeout(() => {
                    self.updateInProgress = true;
                    self.update();
                }, 30);
            }
        }
    }

    setFocus(index: number) {
        if (!this.wrapper.childNodes.length) return;
        this.focus = index;

        for (const node of this.wrapper.children) {
            node.classList.remove('rdfd-autocomplete-active');
        }

        if (this.focus >= this.wrapper.childNodes.length) this.focus = 0;
        if (this.focus < 0) this.focus = this.wrapper.childNodes.length - 1;

        this.getNode(this.focus).classList.add('rdfd-autocomplete-active');
    }

    getNode(i: number) {
        return this.wrapper.childNodes[i] as HTMLElement;
    }

    update() {
        const self = this;

        self.selectedElement = self.graph.getSelectedEditingElement();
        const text = this.selectedElement.innerText;

        // Preserve the trailing whitespace characters
        const tokens = this.selectedElement.innerText
            .trim()
            .split(/(\S+\s+)/)
            .filter((token) => token !== '');
        const search = tokens.length > 0 ? tokens.pop() : '';
        const state = this.graph
            .getView()
            .getState(this.graph.cellEditor.getEditingCell()) as MxCellState;

        this.close();

        if (!state || !state.cell) return;

        this.wrapper.style.left = `${this.selectedElement.offsetLeft - this.selectedElement.offsetWidth - 7}px`;
        this.wrapper.style.top = `${this.selectedElement.offsetTop + this.selectedElement.offsetHeight / 2}px`;

        console.log('updateAC', {
            search: search,
            text: text,
            tokens: tokens,
            node: self.selectedElement,
        });

        const setValue = (value: string) => {
            self.selectedElement.innerText = tokens.join('') + value;
            self.placeCaretAtEnd(self.selectedElement);
        };

        const result = this.fetch(
            state.cell.id,
            search,
            window.getSelection().anchorOffset,
        );

        if (result.length > 0) {
            this.wrapper.style.display = 'block';
        }

        result.map((a, i) => {
            const el = document.createElement('DIV');
            el.innerText = a.value;
            this.wrapper.append(el);

            // Adjust the wrapper width based on the longest string
            const longestString = result.reduce(
                (longest, current) =>
                    current.value.length > longest.length
                        ? current.value
                        : longest,
                '',
            );

            this.wrapper.style.width = `fit-content`;

            el.addEventListener('click', () => {
                setValue(a.value);
                self.close();
            });
            el.onmouseenter = () => {
                self.setFocus(i);
            };
        });
        self.updateInProgress = false;
    }

    close() {
        this.wrapper.innerHTML = '';
        this.wrapper.style.display = 'none';
        this.focus = -1;
    }

    placeCaretAtEnd(el: HTMLElement) {
        setTimeout(() => {
            const range = document.createRange();
            range.selectNodeContents(el);
            range.collapse(false);
            var selection = window.getSelection();
            selection?.removeAllRanges();
            selection?.addRange(range);
        });
    }

    fetch(
        nodeId: string,
        search: string,
        caretPos: number,
    ): { value: string }[] {
        const completions = this.rdfAutoComplete.complete(search);
        console.log('comp: ' + completions);

        return completions.map((completion: string) => ({ value: completion }));
    }
}
