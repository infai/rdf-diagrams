// rdf turtle syntax check

import { RdfDiagramComponent } from './rdf_diagram_component';

export class RdfPanel extends RdfDiagramComponent {
    panel: mxWindow|null;
    resultWrapper: HTMLTextAreaElement;
    errorWrapper: HTMLElement;
    formatSelection: HTMLElement;

    panelImg =
        'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgLTk2MCA5NjAgOTYwIiB3aWR0aD0iMjQiPjxwYXRoIGQ9Ik0yMDAtMTIwcS0zMyAwLTU2LjUtMjMuNVQxMjAtMjAwdi01NjBxMC0zMyAyMy41LTU2LjVUMjAwLTg0MGg1NjBxMzMgMCA1Ni41IDIzLjVUODQwLTc2MHY1NjBxMCAzMy0yMy41IDU2LjVUNzYwLTEyMEgyMDBabTAtODBoNTYwdi00ODBIMjAwdjQ4MFptODAtMjgwdi04MGg0MDB2ODBIMjgwWm0wIDE2MHYtODBoMjQwdjgwSDI4MFoiLz48L3N2Zz4=';

    run() {
        console.log('Greetings from RDFDiagramComponent RdfOutputPanel', this);

        this.panel = null;
        this.createButton();
        this.createMenu();
    }

    createButton() {
        const self = this;
        const btn = document.createElement('button');
        btn.className = 'geBtn gePrimaryBtn';
        btn.style.display = 'inline-block';
        btn.style.position = 'relative';
        btn.style.backgroundImage = 'none';
        btn.style.padding = '2px 10px 0 10px';
        btn.style.marginTop = '-10px';
        btn.style.cursor = 'pointer';
        btn.style.height = '32px';
        btn.style.minWidth = '0px';
        btn.style.top = '-2px';
        btn.setAttribute('title', mxResources.get('Open RDF Panel'));

        const icon = document.createElement('img');
        icon.className = 'geInverseAdaptiveAsset';
        icon.setAttribute('src', this.panelImg);
        icon.setAttribute('align', 'absmiddle');
        icon.style.marginRight = '4px';
        icon.style.marginTop = '-3px';
        icon.style.height = '18px';
        icon.style.filter = 'invert(1)';
        // icon.style.opacity = '0.5';
        btn.appendChild(icon);

        mxUtils.write(btn, mxResources.get('RDF Panel'));

        mxEvent.addListener(
            btn,
            'click',
            mxUtils.bind(this, function (evt) {
                self.editorUi.actions.get('toggleRdfPanel').funct(evt);
            }),
        );

        this.editorUi.buttonContainer.appendChild(btn);
    }

    createMenu() {
        const self = this;

        mxResources.parse('toggleRdfPanel=RDF Output Panel');
        const viewMenu = this.editorUi.menus.get('view');
        const oldViewFunct = viewMenu.funct;
        viewMenu.funct = function (menu: any, parent: any) {
            oldViewFunct.apply(this, arguments);
            self.editorUi.menus.addMenuItems(
                menu,
                ['-', 'toggleRdfPanel'],
                parent,
            );
        };

        const viewPanelsMenu = this.editorUi.menus.get('viewPanels');
        const oldViewPanelsMenu = viewPanelsMenu.funct;

        viewPanelsMenu.funct = function (menu: any, parent: any) {
            oldViewPanelsMenu.apply(this, arguments);
            self.editorUi.menus.addMenuItems(
                menu,
                ['-', 'toggleRdfPanel'],
                parent,
            );
        };

        const action = this.editorUi.actions.addAction('toggleRdfPanel', () => {
            // mxUtils.bind(this, true)
            if (self.panel == null) {
                self.createPanel(
                    document.body.offsetWidth - 640,
                    80,
                    420,
                    document.body.offsetHeight - 100
                )
                // this.rdfOutputWindow.window.addListener('show', mxUtils.bind(this, function()
                // {
                // 	self.editorUi.fireEvent(new mxEventObject('rdfOutput'));
                // }));
                // this.rdfOutputWindow.window.addListener('hide', function()
                // {
                // 	self.editorUi.fireEvent(new mxEventObject('rdfOutput'));
                // });
                self.panel.setVisible(true);
                self.editorUi.fireEvent(new mxEventObject('toggleRdfPanel'));
            } else {
                self.panel.setVisible(!self.panel.isVisible());
            }
        });

        console.log({ action });

        action.setToggleAction(true);
        action.setSelectedCallback(() => {
            return self.panel != null && self.panel.isVisible();
        });
    }

    async createPanel(x: number, y: number, w: number, h: number) {
        const self = this;

        const parentDiv = document.createElement('div');
        parentDiv.style.position = 'absolute';
        parentDiv.style.width = '100%';
        parentDiv.style.height = '100%';
        parentDiv.style.overflow = 'hidden';

        this.resultWrapper = document.createElement('textarea');
        this.resultWrapper.setAttribute('wrap', 'off');
        this.resultWrapper.setAttribute('spellcheck', 'false');
        this.resultWrapper.setAttribute('autocorrect', 'off');
        this.resultWrapper.setAttribute('autocomplete', 'off');
        this.resultWrapper.setAttribute('autocapitalize', 'off');
        this.resultWrapper.style.overflow = 'auto';
        this.resultWrapper.style.resize = 'none';
        this.resultWrapper.style.width = 'calc(100% - 6px)';
        this.resultWrapper.style.height = 'calc(100% - 50px)';
        this.resultWrapper.style.marginBottom = '5px';
        parentDiv.appendChild(this.resultWrapper);

        this.errorWrapper = document.createElement('div');
        this.errorWrapper.style.width = '100%';
        this.errorWrapper.style.display = 'none';
        this.errorWrapper.style.padding = '10px';
        this.errorWrapper.style.boxSizing = 'border-box';
        parentDiv.appendChild(this.errorWrapper);

        const helpButton = this.editorUi.menus.createHelpLink(
            'https://gitlab.com/infai/rdf-diagram-framework',
        );
        helpButton.style.position = 'absolute';
        helpButton.style.margin = '10px';
        helpButton.style.right = '0';
        helpButton.style.bottom = '0';
        parentDiv.appendChild(helpButton);

        this.formatSelection = document.createElement('select');
        this.formatSelection.className = 'geBtn';

        const turtleOption = document.createElement('option');
        turtleOption.setAttribute('value', 'turtle');
        mxUtils.write(turtleOption, mxResources.get('turtle'));
        turtleOption.setAttribute('selected', 'selected');
        this.formatSelection.appendChild(turtleOption);
        parentDiv.appendChild(this.formatSelection);

        mxResources.parse('toggleRdfPanel=RDF Output');
        this.panel = new mxWindow(
            mxResources.get('toggleRdfPanel'),
            parentDiv,
            x,
            y,
            w,
            h,
            true,
            true,
        );
        this.panel.minimumSize = new mxRectangle(0, 0, 212, 120);
        this.panel.destroyOnClose = false;
        this.panel.setMaximizable(false);
        this.panel.setResizable(true);
        this.panel.setClosable(true);

        let rdf = self.getRDF();

        this.panel.addListener(
            'show',
            mxUtils.bind(this, function () {
                this.panel.fit();
                rdf;
            }),
        );

        this.graph.model.addListener(
            'getRDF',
            mxUtils.bind(self, function (sender: any, evt: mxEventObject) {
                const result = evt.getProperty('result');

                if (result) {
                    self.showResult(result as string);
                } else {
                    console.log('Print error', evt);
                    self.showError(evt.getProperty('error'));
                }
            }),
        );
    }

    showResult(result: string) {
        if (this.panel == null) return;

        this.hideError()
        this.resultWrapper.style.display = 'block'
        this.formatSelection.style.display = 'inline'
        this.resultWrapper.value = result
    }

    hideResult() {
        this.errorWrapper.style.display = 'block'
        this.resultWrapper.style.display = 'none'
        this.formatSelection.style.display = 'none'
    }

    showError(error: any) {
        if (this.panel == null) return

        this.hideResult()

        this.errorWrapper.innerText = '';
        this.errorWrapper.insertAdjacentHTML(
            'beforeend',
            '<p><b>Error:</b></p>',
        );
        this.errorWrapper.append(error.message);

        // TODO add button to select and scroll into view of the bad cell

        // var badCellId = error.element_id;
        // var badCell = self.graph.model.getCell(badCellId);
        // var badCellBtn = document.createElement('button');
        // badCellBtn.appendChild(document.createTextNode(`Select element ${badCellId}`));
        // badCellBtn.setAttribute('title', `Select cell ${badCellId}`);
        // badCellBtn.style.padding = "10px"
        // errorWrapper.append(badCellBtn)

        // mxEvent.addListener(badCellBtn, 'click', mxUtils.bind(this, function() {
        //     self.graph.getSelectionModel().setCell(badCell);
        // }));
    }

    hideError() {
        this.errorWrapper.style.display = 'none'
    }
}
