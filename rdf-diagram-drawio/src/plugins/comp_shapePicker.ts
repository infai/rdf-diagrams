import { RdfDiagramComponent } from './rdf_diagram_component';

export class ShapePicker extends RdfDiagramComponent {
    createShapePicker_origin: any;
    getCellsForShapePicker_origin: any;

    run() {
        console.log('Greeting froms RDFD.ShapePicker!');

        this.ui();
    }

    ui() {
        const self = this;

        this.createShapePicker_origin = this.editorUi.createShapePicker;
        this.getCellsForShapePicker_origin =
            this.editorUi.getCellsForShapePicker;

        this.createShapePicker();
        this.getCellsForShapePicker();
    }

    createShapePicker() {
        const self = this;

        this.editorUi.createShapePicker = function (
            x: any,
            y: any,
            source: any,
            callback: any,
            direction: any,
            afterClick: any,
            cells: any,
            hovering: any,
            getInsertLocationFn: any,
            showEdges: any,
            startEditing: any,
        ) {
            // add predicate to connection edge
            if (source != null) {
                afterClick = function (evt: any) {
                    self.graph.model.setValue(
                        source.edges[source.edges.length - 1],
                        '&lt;predicate&gt;',
                    );
                    self.editorUi.hideShapePicker();
                };
            }

            return self.createShapePicker_origin.call(
                self.editorUi,
                x,
                y,
                source,
                callback,
                direction,
                afterClick,
                cells,
                hovering,
                getInsertLocationFn,
                showEdges,
                startEditing,
            );
        };
    }

    getCellsForShapePicker() {
        const self = this;

        this.editorUi.getCellsForShapePicker = function (
            cell: any,
            hovering: any,
            showEdges: any,
        ) {
            const cells: any[] = [];

            // TODO parse rdfd library => see loadLibrary()

            // create a clone of the source cell
            if (cell != null) {
                try {
                    cell = self.graph.cloneCell(cell);

                    if (
                        self.graph.model.isVertex(cell) &&
                        cell.geometry != null
                    ) {
                        cell.geometry.x = 0;
                        cell.geometry.y = 0;
                        self.graph.model.setValue(cell, '&lt;object&gt;');
                    }
                    cells.push(cell);
                } catch (e) {
                    cell = null;
                }
            }
            return cells;
        };
    }

    loadLibrary() {
        const libUrl = 'http://localhost:8080/plugins/rdf-diagram/library.xml';
        this.editorUi.editor.loadUrl(
            libUrl,
            mxUtils.bind(this, function (data) {
                try {
                    var doc = mxUtils.parseXml(data) as XMLDocument;
                    if (doc.documentElement.nodeName == 'mxlibrary') {
                        console.log('RDFD XML doc', { doc });
                        var temp = JSON.parse(
                            mxUtils.getTextContent(doc.documentElement),
                        );

                        if (temp != null && temp.length > 0) {
                            console.log('RDFD temp xmllib', { temp });
                        }
                    }
                } catch (error) {
                    console.log(error);
                }
            }),
        );
    }
}
