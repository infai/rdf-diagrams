/**
 * Copyright (c) 2006-2020, JGraph Ltd
 * Copyright (c) 2006-2020, draw.io AG
 */
// Overrides of global vars need to be pre-loaded
window.DRAWIO_BASE_URL = null // Replace with path to base of deployment, e.g. https://www.example.com/folder
window.DRAWIO_VIEWER_URL = null // Replace your path to the viewer js, e.g. https://www.example.com/js/viewer.min.js
window.DRAWIO_LIGHTBOX_URL = null // Replace with your lightbox URL, eg. https://www.example.com
window.DRAW_MATH_URL = 'math/es5'
window.DRAWIO_CONFIG = {
  defaultVertexStyle: {
    html: 1,
    shadow: 1,
    spacingLeft: 10,
    spacingRight: 10,
    whiteSpace: 'wrap',
    fontSize: 14
  },
  defaultEdgeStyle: {
    html: 1,
    edgeStyle: 'elbowEdgeStyle',
    rounded: 0,
    orthogonalLoop: 1,
    jettySize: 'auto',
    endArrow: 'block',
    endFill: 1,
    endSize: 5,
    spacingBottom: 20,
    shadow: 1,
    fontSize: 14
  },

  enabledLibraries: [], // we dont need any other library
  libraries: [
    {
      "title": {
          "main": "RDF-Diagrams"
      },
      "entries": [
          {
              "id": "rdf-diagrams",
              "title": {
                  "main": "RDF-Diagrams",
                  "de": "RDF-Diagrams"
              },
              "desc": {
                  "main": "Collection of Graphics for RDF-Diagrams",
                  "de": "Sammlung von Grafiken für RDF-Diagrams"
              },
              "libs": [
                  {
                      "title": {
                          "main": "RDF-Diagrams",
                          "de": "RDF-Diagrams"
                      },
                      "url": `${window.location.href.substring(0, window.location.href.lastIndexOf('/'))}/plugins/rdf-diagram/library.xml`,
                      "prefetch": true
                  }
              ]
          }
      ]
  }],
  defaultLibraries: 'rdf-diagrams',

  defaultMacroParameters: {
    border: false,
    toolbarStyle: 'inline'
  },
  gptApiKey: '',
  gptModel: 'gpt-4',
}
urlParams.sync = 'auto'
urlParams.test = '0'
urlParams.dev = '0'
urlParams.splash = '0'
urlParams.p = 'rdf-diagram'
urlParams.dark = '0'
urlParams.gl = '1'
urlParams.cors = ''
// window.DRAWIO_BASE_URL = 'http://localhost:8080/'
// window.DRAWIO_GITLAB_URL = 'http://gitlab.com/oauth/token'
// window.DRAWIO_GITLAB_ID = ''
// window.DRAWIO_GITLAB_SECRET = ''


console.log('urlParams', urlParams);
