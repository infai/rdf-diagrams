const CopyPlugin = require('copy-webpack-plugin')
const path = require('path')

module.exports = {
    mode: 'development',
    target: 'web',
    devtool: 'inline-source-map',
    experiments: { asyncWebAssembly: true },
    entry: './src/plugins/index.ts',
    devServer: {
        https: true,
        devMiddleware: { writeToDisk: true },
        static: { directory: path.join(__dirname, 'dist') },
        open: true,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': '*',
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Credentials': 'true',
            'Access-Control-Expose-Headers': '*',
            'Access-Control-Max-Age': '3600',
            // Extremely permissive CSP (not recommended for production)
            'Content-Security-Policy': "default-src * 'unsafe-inline' 'unsafe-eval'; script-src * 'unsafe-inline' 'unsafe-eval'; connect-src * 'unsafe-inline'; img-src * data: blob: 'unsafe-inline'; frame-src *; style-src * 'unsafe-inline';"
        },
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: 'third_party/drawio/src/main/webapp',
                    to: 'dist',
                    globOptions: { ignore: ['PostConfig.js', 'PreConfig.js'] }
                },
                { from: './src/js', to: 'dist/js' },
                { from: 'static', to: 'dist/plugins/rdf-diagram' }
            ].map(pattern => ({ ...pattern, to: path.join(__dirname, pattern.to) }))
        })
    ],
    module: {
        rules: [
            {
                test: /\.ts?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        filename: 'rdf-diagram.js',
        path: path.join(__dirname, 'dist/plugins'),
        clean: true
    }
}
