A powerful Draw.io plugin that enables visual creation and editing of RDF diagrams with real-time validation and auto-completion.

A component of the [RDF-Diagram-Framework](https://gitlab.com/infai/rdf-diagram-framework).

## ✨ Features

- **Visual RDF graph** creation and editing
- Real-time syntax and RDF-Diagram **validation**
- **Auto-completion** for RDF terms
- Integrated shape library
- Live **RDF preview**
- Export to Turtle

## Usage

### Online Demo

   Try it directly in your browser: [Draw.io with RDF-D Plugin](https://infai.gitlab.io/rdf-diagram-framework?test=1&splash=0)

### Local Development Setup

1. Clone the repository:

```bash
git clone https://gitlab.com/infai/rdf-diagram-framework.git
cd rdf-diagram-framework/rdf-diagram-drawio
```

1. Install dependencies:

```bash
cargo install just
```

1. Build the plugin:

```bash
just build
```

1. Start development server:

```bash
npm run develop
```

1. Open Draw.io in your browser
1. Use the RDF shapes from the library panel
1. Create your RDF diagrams
1. Press the `RDF Panel` button in the upper right corner for a preview
