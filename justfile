install:
    cargo install --path=rdf-diagram-cli

test:
    just docs/test
    just rdf-diagram-cli/test
    just rdf-diagram-drawio/test
    just rdf-diagram-lib/test
    just rdf-diagram-tree-sitter/test

lint:
    just docs/lint
    just rdf-diagram-cli/lint
    just rdf-diagram-drawio/lint
    just rdf-diagram-lib/lint
    just rdf-diagram-tree-sitter/lint

pre-commit-check: test lint
    cargo install commitlint-rs
    commitlint -f HEAD~1
    just ./docs/build
    just ./rdf-diagram-drawio/build
