ARG RUST_VERSION=1.80

FROM rust:${RUST_VERSION}

RUN apt-get update && apt-get install -y --no-install-recommends \
  ca-certificates \
  curl \
  clang \
  firefox-esr \
  llvm \
  && curl -sL https://deb.nodesource.com/setup_18.x | bash - \
  && apt-get install -y --no-install-recommends nodejs \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* \
  && cargo install just cargo-udeps mdbook mdbook-template mdbook-linkcheck
