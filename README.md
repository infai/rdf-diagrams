# RDF Diagram Framework

A comprehensive toolkit for creating, editing, and converting **visual RDF graph representations**. An RDF-Diagram can express all concepts of [RDF 1.1](https://www.w3.org/TR/rdf11-concepts/) (except RDF Datasets).

- **Try it**: [Draw.io with RDF-D Plugin](https://infai.gitlab.io/rdf-diagram-framework?test=1&splash=0)
- **Read the** [Specification](https://infai.gitlab.io/rdf-diagram-framework/docs/)

This is an RDF-Diagram created with Draw.io:

![image](./docs/src/exports/images/introduction-mona-lisa.svg)

and it directly translates to turtle:

```turtle
# Prefixes ommitted

<http://example.org/bob#me> a foaf:Person;
  foaf:topic_interest wd:Q12418;
  schema:birthDate "1990-07-04"^^xsd:date;
  foaf:knows <http://example.org/alice#me>.

<http://data.europeana.eu/item/04802/243FA8618938F4117025F17A8B813C5F9AA4D619>
  dcterms:subject wd:Q12418.

wd:Q12418
  dcterms:creator dbp:Leonardo_da_Vinci;
  dcterms:title "Mona Lisa".
```

## Overview

The RDF-Diagram-Framework provides a standardized visual language for RDF graphs, making them more accessible and easier to understand. It offers a complete ecosystem of tools for working with RDF diagrams, from visual editing to programmatic manipulation.

## Key Components

### [Draw.io Plugin](https://gitlab.com/infai/rdf-diagram-framework/-/tree/main/rdf-diagram-drawio?ref_type=heads)

- **Visual RDF graph** creation and editing
- Real-time syntax and RDF-Diagram **validation**
- **Auto-completion** for RDF terms
- Integrated shape library
- Live **RDF preview**
- Export to Turtle

### [Command-Line Interface](https://gitlab.com/infai/rdf-diagram-framework/-/tree/main/rdf-diagram-cli)

- **Convert** from Draw.io RDF-Diagrams to RDF serializations (Turtle and N-Triples)
- Support for multiple RDF formats
- Batch processing capabilities

### [Library](https://gitlab.com/infai/rdf-diagram-framework/-/tree/main/rdf-diagram-lib)

- Available for **Rust** and **JavaScript**
- RDF conversion utilities

### [Specification](https://infai.gitlab.io/rdf-diagram-framework/docs/)

- Comprehensive visual language definition
- Covers [RDF 1.1](https://www.w3.org/TR/rdf11-concepts/) (except RDF Datasets)
- Syntax is equivalent to [Turtle 1.1](https://www.w3.org/TR/turtle/)
- Semantic mappings of diagram elements to RDF
- Best practices and examples

## Usage

### Using the Draw.io Plugin

1. Visit [Draw.io with RDF-D Plugin](https://infai.gitlab.io/rdf-diagram-framework?test=1&splash=0)
2. Use the RDF shapes from the library panel
3. Create your RDF diagrams
4. Access the RDF preview via the RDF Panel button

### Installing the [CLI](https://gitlab.com/infai/rdf-diagram-framework/-/tree/main/rdf-diagram-cli)

```bash
git clone git@gitlab.com:infai/rdf-diagram-framework.git
cd rdf-diagram-framework
cargo install --path=rdf-diagram-cli
```

### Using the [Library](https://gitlab.com/infai/rdf-diagram-framework/-/tree/main/rdf-diagram-lib)

```bash
cargo install rdf-diagram-lib
```

```rust
use rdf_diagram_lib::{drawio_to_turtle, Result};

let xml = include_str!("diagram.drawio");
let turtle = drawio_to_turtle(&xml)?;
```
