# tree-sitter-rdf-diagram-framework

Supporting [tree-sitter](https://github.com/tree-sitter/tree-sitter) grammar for the [RDF-Diagram-Framework](https://gitlab.com/infai/rdf-diagram-framework).
