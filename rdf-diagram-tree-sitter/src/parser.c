#include "tree_sitter/parser.h"

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 124
#define LARGE_STATE_COUNT 2
#define SYMBOL_COUNT 88
#define ALIAS_COUNT 1
#define TOKEN_COUNT 43
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 1
#define MAX_ALIAS_SEQUENCE_LENGTH 4
#define PRODUCTION_ID_COUNT 3

enum ts_symbol_identifiers {
  sym_pn_prefix = 1,
  anon_sym_COMMA = 2,
  sym_comment = 3,
  anon_sym_DOT = 4,
  anon_sym_ATprefix = 5,
  anon_sym_ATbase = 6,
  aux_sym_sparql_base_token1 = 7,
  aux_sym_sparql_prefix_token1 = 8,
  anon_sym_SEMI = 9,
  anon_sym_a = 10,
  anon_sym_LBRACK = 11,
  anon_sym_RBRACK = 12,
  anon_sym_LPAREN = 13,
  anon_sym_RPAREN = 14,
  anon_sym_LT = 15,
  aux_sym_iri_reference_token1 = 16,
  aux_sym_iri_reference_token2 = 17,
  anon_sym_GT = 18,
  sym_integer = 19,
  sym_decimal = 20,
  sym_double = 21,
  anon_sym_DQUOTE = 22,
  aux_sym__string_literal_quote_token1 = 23,
  anon_sym_SQUOTE = 24,
  aux_sym__string_literal_single_quote_token1 = 25,
  anon_sym_SQUOTE_SQUOTE_SQUOTE = 26,
  anon_sym_SQUOTE_SQUOTE = 27,
  aux_sym__string_literal_long_single_quote_token1 = 28,
  anon_sym_DQUOTE_DQUOTE_DQUOTE = 29,
  anon_sym_DQUOTE_DQUOTE = 30,
  aux_sym__string_literal_long_quote_token1 = 31,
  anon_sym_AT = 32,
  anon_sym_CARET_CARET = 33,
  anon_sym_true = 34,
  anon_sym_false = 35,
  anon_sym_COLON = 36,
  anon_sym__COLON = 37,
  aux_sym_blank_node_label_token1 = 38,
  sym_lang_tag = 39,
  sym_echar = 40,
  sym_anon = 41,
  sym_pn_local = 42,
  sym_rdf_diagram = 43,
  sym_term = 44,
  sym_property_term = 45,
  sym_turtle_doc = 46,
  sym_statement = 47,
  sym_directive = 48,
  sym_prefix_id = 49,
  sym_base = 50,
  sym_sparql_base = 51,
  sym_sparql_prefix = 52,
  sym_triples = 53,
  sym_property_list = 54,
  sym_property = 55,
  sym_object_list = 56,
  sym_predicate = 57,
  sym_subject = 58,
  sym_object = 59,
  sym_literal = 60,
  sym_blank_node_property_list = 61,
  sym_collection = 62,
  sym_object_collection = 63,
  sym_numeric_literal = 64,
  sym_string = 65,
  sym_iri_reference = 66,
  sym__string_literal_quote = 67,
  sym__string_literal_single_quote = 68,
  sym__string_literal_long_single_quote = 69,
  sym__string_literal_long_quote = 70,
  sym_rdf_literal = 71,
  sym_boolean_literal = 72,
  sym_iri = 73,
  sym_prefixed_name = 74,
  sym_blank_node = 75,
  sym_namespace = 76,
  sym_blank_node_label = 77,
  aux_sym_property_term_repeat1 = 78,
  aux_sym_turtle_doc_repeat1 = 79,
  aux_sym_property_list_repeat1 = 80,
  aux_sym_object_list_repeat1 = 81,
  aux_sym_object_collection_repeat1 = 82,
  aux_sym_iri_reference_repeat1 = 83,
  aux_sym__string_literal_quote_repeat1 = 84,
  aux_sym__string_literal_single_quote_repeat1 = 85,
  aux_sym__string_literal_long_single_quote_repeat1 = 86,
  aux_sym__string_literal_long_quote_repeat1 = 87,
  alias_sym_lexical_form = 88,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [sym_pn_prefix] = "pn_prefix",
  [anon_sym_COMMA] = ",",
  [sym_comment] = "comment",
  [anon_sym_DOT] = ".",
  [anon_sym_ATprefix] = "@prefix",
  [anon_sym_ATbase] = "@base",
  [aux_sym_sparql_base_token1] = "sparql_base_token1",
  [aux_sym_sparql_prefix_token1] = "sparql_prefix_token1",
  [anon_sym_SEMI] = ";",
  [anon_sym_a] = "a",
  [anon_sym_LBRACK] = "[",
  [anon_sym_RBRACK] = "]",
  [anon_sym_LPAREN] = "(",
  [anon_sym_RPAREN] = ")",
  [anon_sym_LT] = "<",
  [aux_sym_iri_reference_token1] = "iri_reference_token1",
  [aux_sym_iri_reference_token2] = "iri_reference_token2",
  [anon_sym_GT] = ">",
  [sym_integer] = "integer",
  [sym_decimal] = "decimal",
  [sym_double] = "double",
  [anon_sym_DQUOTE] = "\"",
  [aux_sym__string_literal_quote_token1] = "_string_literal_quote_token1",
  [anon_sym_SQUOTE] = "'",
  [aux_sym__string_literal_single_quote_token1] = "_string_literal_single_quote_token1",
  [anon_sym_SQUOTE_SQUOTE_SQUOTE] = "'''",
  [anon_sym_SQUOTE_SQUOTE] = "''",
  [aux_sym__string_literal_long_single_quote_token1] = "_string_literal_long_single_quote_token1",
  [anon_sym_DQUOTE_DQUOTE_DQUOTE] = "\"\"\"",
  [anon_sym_DQUOTE_DQUOTE] = "\"\"",
  [aux_sym__string_literal_long_quote_token1] = "_string_literal_long_quote_token1",
  [anon_sym_AT] = "@",
  [anon_sym_CARET_CARET] = "^^",
  [anon_sym_true] = "true",
  [anon_sym_false] = "false",
  [anon_sym_COLON] = ":",
  [anon_sym__COLON] = "_:",
  [aux_sym_blank_node_label_token1] = "id",
  [sym_lang_tag] = "lang_tag",
  [sym_echar] = "echar",
  [sym_anon] = "anon",
  [sym_pn_local] = "pn_local",
  [sym_rdf_diagram] = "rdf_diagram",
  [sym_term] = "term",
  [sym_property_term] = "property",
  [sym_turtle_doc] = "turtle_doc",
  [sym_statement] = "statement",
  [sym_directive] = "directive",
  [sym_prefix_id] = "prefix_id",
  [sym_base] = "base",
  [sym_sparql_base] = "base",
  [sym_sparql_prefix] = "prefix_id",
  [sym_triples] = "triples",
  [sym_property_list] = "property_list",
  [sym_property] = "property",
  [sym_object_list] = "object_list",
  [sym_predicate] = "predicate",
  [sym_subject] = "subject",
  [sym_object] = "object",
  [sym_literal] = "literal",
  [sym_blank_node_property_list] = "blank_node_property_list",
  [sym_collection] = "collection",
  [sym_object_collection] = "object_collection",
  [sym_numeric_literal] = "numeric_literal",
  [sym_string] = "string",
  [sym_iri_reference] = "iri_reference",
  [sym__string_literal_quote] = "_string_literal_quote",
  [sym__string_literal_single_quote] = "_string_literal_single_quote",
  [sym__string_literal_long_single_quote] = "_string_literal_long_single_quote",
  [sym__string_literal_long_quote] = "_string_literal_long_quote",
  [sym_rdf_literal] = "rdf_literal",
  [sym_boolean_literal] = "boolean_literal",
  [sym_iri] = "iri",
  [sym_prefixed_name] = "prefixed_name",
  [sym_blank_node] = "blank_node",
  [sym_namespace] = "namespace",
  [sym_blank_node_label] = "blank_node_label",
  [aux_sym_property_term_repeat1] = "property_term_repeat1",
  [aux_sym_turtle_doc_repeat1] = "turtle_doc_repeat1",
  [aux_sym_property_list_repeat1] = "property_list_repeat1",
  [aux_sym_object_list_repeat1] = "object_list_repeat1",
  [aux_sym_object_collection_repeat1] = "object_collection_repeat1",
  [aux_sym_iri_reference_repeat1] = "iri_reference_repeat1",
  [aux_sym__string_literal_quote_repeat1] = "_string_literal_quote_repeat1",
  [aux_sym__string_literal_single_quote_repeat1] = "_string_literal_single_quote_repeat1",
  [aux_sym__string_literal_long_single_quote_repeat1] = "_string_literal_long_single_quote_repeat1",
  [aux_sym__string_literal_long_quote_repeat1] = "_string_literal_long_quote_repeat1",
  [alias_sym_lexical_form] = "lexical_form",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [sym_pn_prefix] = sym_pn_prefix,
  [anon_sym_COMMA] = anon_sym_COMMA,
  [sym_comment] = sym_comment,
  [anon_sym_DOT] = anon_sym_DOT,
  [anon_sym_ATprefix] = anon_sym_ATprefix,
  [anon_sym_ATbase] = anon_sym_ATbase,
  [aux_sym_sparql_base_token1] = aux_sym_sparql_base_token1,
  [aux_sym_sparql_prefix_token1] = aux_sym_sparql_prefix_token1,
  [anon_sym_SEMI] = anon_sym_SEMI,
  [anon_sym_a] = anon_sym_a,
  [anon_sym_LBRACK] = anon_sym_LBRACK,
  [anon_sym_RBRACK] = anon_sym_RBRACK,
  [anon_sym_LPAREN] = anon_sym_LPAREN,
  [anon_sym_RPAREN] = anon_sym_RPAREN,
  [anon_sym_LT] = anon_sym_LT,
  [aux_sym_iri_reference_token1] = aux_sym_iri_reference_token1,
  [aux_sym_iri_reference_token2] = aux_sym_iri_reference_token2,
  [anon_sym_GT] = anon_sym_GT,
  [sym_integer] = sym_integer,
  [sym_decimal] = sym_decimal,
  [sym_double] = sym_double,
  [anon_sym_DQUOTE] = anon_sym_DQUOTE,
  [aux_sym__string_literal_quote_token1] = aux_sym__string_literal_quote_token1,
  [anon_sym_SQUOTE] = anon_sym_SQUOTE,
  [aux_sym__string_literal_single_quote_token1] = aux_sym__string_literal_single_quote_token1,
  [anon_sym_SQUOTE_SQUOTE_SQUOTE] = anon_sym_SQUOTE_SQUOTE_SQUOTE,
  [anon_sym_SQUOTE_SQUOTE] = anon_sym_SQUOTE_SQUOTE,
  [aux_sym__string_literal_long_single_quote_token1] = aux_sym__string_literal_long_single_quote_token1,
  [anon_sym_DQUOTE_DQUOTE_DQUOTE] = anon_sym_DQUOTE_DQUOTE_DQUOTE,
  [anon_sym_DQUOTE_DQUOTE] = anon_sym_DQUOTE_DQUOTE,
  [aux_sym__string_literal_long_quote_token1] = aux_sym__string_literal_long_quote_token1,
  [anon_sym_AT] = anon_sym_AT,
  [anon_sym_CARET_CARET] = anon_sym_CARET_CARET,
  [anon_sym_true] = anon_sym_true,
  [anon_sym_false] = anon_sym_false,
  [anon_sym_COLON] = anon_sym_COLON,
  [anon_sym__COLON] = anon_sym__COLON,
  [aux_sym_blank_node_label_token1] = aux_sym_blank_node_label_token1,
  [sym_lang_tag] = sym_lang_tag,
  [sym_echar] = sym_echar,
  [sym_anon] = sym_anon,
  [sym_pn_local] = sym_pn_local,
  [sym_rdf_diagram] = sym_rdf_diagram,
  [sym_term] = sym_term,
  [sym_property_term] = sym_property,
  [sym_turtle_doc] = sym_turtle_doc,
  [sym_statement] = sym_statement,
  [sym_directive] = sym_directive,
  [sym_prefix_id] = sym_prefix_id,
  [sym_base] = sym_base,
  [sym_sparql_base] = sym_base,
  [sym_sparql_prefix] = sym_prefix_id,
  [sym_triples] = sym_triples,
  [sym_property_list] = sym_property_list,
  [sym_property] = sym_property,
  [sym_object_list] = sym_object_list,
  [sym_predicate] = sym_predicate,
  [sym_subject] = sym_subject,
  [sym_object] = sym_object,
  [sym_literal] = sym_literal,
  [sym_blank_node_property_list] = sym_blank_node_property_list,
  [sym_collection] = sym_collection,
  [sym_object_collection] = sym_object_collection,
  [sym_numeric_literal] = sym_numeric_literal,
  [sym_string] = sym_string,
  [sym_iri_reference] = sym_iri_reference,
  [sym__string_literal_quote] = sym__string_literal_quote,
  [sym__string_literal_single_quote] = sym__string_literal_single_quote,
  [sym__string_literal_long_single_quote] = sym__string_literal_long_single_quote,
  [sym__string_literal_long_quote] = sym__string_literal_long_quote,
  [sym_rdf_literal] = sym_rdf_literal,
  [sym_boolean_literal] = sym_boolean_literal,
  [sym_iri] = sym_iri,
  [sym_prefixed_name] = sym_prefixed_name,
  [sym_blank_node] = sym_blank_node,
  [sym_namespace] = sym_namespace,
  [sym_blank_node_label] = sym_blank_node_label,
  [aux_sym_property_term_repeat1] = aux_sym_property_term_repeat1,
  [aux_sym_turtle_doc_repeat1] = aux_sym_turtle_doc_repeat1,
  [aux_sym_property_list_repeat1] = aux_sym_property_list_repeat1,
  [aux_sym_object_list_repeat1] = aux_sym_object_list_repeat1,
  [aux_sym_object_collection_repeat1] = aux_sym_object_collection_repeat1,
  [aux_sym_iri_reference_repeat1] = aux_sym_iri_reference_repeat1,
  [aux_sym__string_literal_quote_repeat1] = aux_sym__string_literal_quote_repeat1,
  [aux_sym__string_literal_single_quote_repeat1] = aux_sym__string_literal_single_quote_repeat1,
  [aux_sym__string_literal_long_single_quote_repeat1] = aux_sym__string_literal_long_single_quote_repeat1,
  [aux_sym__string_literal_long_quote_repeat1] = aux_sym__string_literal_long_quote_repeat1,
  [alias_sym_lexical_form] = alias_sym_lexical_form,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [sym_pn_prefix] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_COMMA] = {
    .visible = true,
    .named = false,
  },
  [sym_comment] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_DOT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_ATprefix] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_ATbase] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_sparql_base_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_sparql_prefix_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_SEMI] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_a] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LT] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_iri_reference_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_iri_reference_token2] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_GT] = {
    .visible = true,
    .named = false,
  },
  [sym_integer] = {
    .visible = true,
    .named = true,
  },
  [sym_decimal] = {
    .visible = true,
    .named = true,
  },
  [sym_double] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_DQUOTE] = {
    .visible = true,
    .named = false,
  },
  [aux_sym__string_literal_quote_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_SQUOTE] = {
    .visible = true,
    .named = false,
  },
  [aux_sym__string_literal_single_quote_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_SQUOTE_SQUOTE_SQUOTE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SQUOTE_SQUOTE] = {
    .visible = true,
    .named = false,
  },
  [aux_sym__string_literal_long_single_quote_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_DQUOTE_DQUOTE_DQUOTE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DQUOTE_DQUOTE] = {
    .visible = true,
    .named = false,
  },
  [aux_sym__string_literal_long_quote_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_AT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_CARET_CARET] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_true] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_false] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COLON] = {
    .visible = true,
    .named = false,
  },
  [anon_sym__COLON] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_blank_node_label_token1] = {
    .visible = true,
    .named = true,
  },
  [sym_lang_tag] = {
    .visible = true,
    .named = true,
  },
  [sym_echar] = {
    .visible = true,
    .named = true,
  },
  [sym_anon] = {
    .visible = true,
    .named = true,
  },
  [sym_pn_local] = {
    .visible = true,
    .named = true,
  },
  [sym_rdf_diagram] = {
    .visible = true,
    .named = true,
  },
  [sym_term] = {
    .visible = true,
    .named = true,
  },
  [sym_property_term] = {
    .visible = true,
    .named = true,
  },
  [sym_turtle_doc] = {
    .visible = true,
    .named = true,
  },
  [sym_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_directive] = {
    .visible = true,
    .named = true,
  },
  [sym_prefix_id] = {
    .visible = true,
    .named = true,
  },
  [sym_base] = {
    .visible = true,
    .named = true,
  },
  [sym_sparql_base] = {
    .visible = true,
    .named = true,
  },
  [sym_sparql_prefix] = {
    .visible = true,
    .named = true,
  },
  [sym_triples] = {
    .visible = true,
    .named = true,
  },
  [sym_property_list] = {
    .visible = true,
    .named = true,
  },
  [sym_property] = {
    .visible = true,
    .named = true,
  },
  [sym_object_list] = {
    .visible = true,
    .named = true,
  },
  [sym_predicate] = {
    .visible = true,
    .named = true,
  },
  [sym_subject] = {
    .visible = true,
    .named = true,
  },
  [sym_object] = {
    .visible = true,
    .named = true,
  },
  [sym_literal] = {
    .visible = true,
    .named = true,
  },
  [sym_blank_node_property_list] = {
    .visible = true,
    .named = true,
  },
  [sym_collection] = {
    .visible = true,
    .named = true,
  },
  [sym_object_collection] = {
    .visible = true,
    .named = true,
  },
  [sym_numeric_literal] = {
    .visible = true,
    .named = true,
  },
  [sym_string] = {
    .visible = true,
    .named = true,
  },
  [sym_iri_reference] = {
    .visible = true,
    .named = true,
  },
  [sym__string_literal_quote] = {
    .visible = false,
    .named = true,
  },
  [sym__string_literal_single_quote] = {
    .visible = false,
    .named = true,
  },
  [sym__string_literal_long_single_quote] = {
    .visible = false,
    .named = true,
  },
  [sym__string_literal_long_quote] = {
    .visible = false,
    .named = true,
  },
  [sym_rdf_literal] = {
    .visible = true,
    .named = true,
  },
  [sym_boolean_literal] = {
    .visible = true,
    .named = true,
  },
  [sym_iri] = {
    .visible = true,
    .named = true,
  },
  [sym_prefixed_name] = {
    .visible = true,
    .named = true,
  },
  [sym_blank_node] = {
    .visible = true,
    .named = true,
  },
  [sym_namespace] = {
    .visible = true,
    .named = true,
  },
  [sym_blank_node_label] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_property_term_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_turtle_doc_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_property_list_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_object_list_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_object_collection_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_iri_reference_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__string_literal_quote_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__string_literal_single_quote_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__string_literal_long_single_quote_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__string_literal_long_quote_repeat1] = {
    .visible = false,
    .named = false,
  },
  [alias_sym_lexical_form] = {
    .visible = true,
    .named = true,
  },
};

enum ts_field_identifiers {
  field_datatype = 1,
};

static const char * const ts_field_names[] = {
  [0] = NULL,
  [field_datatype] = "datatype",
};

static const TSFieldMapSlice ts_field_map_slices[PRODUCTION_ID_COUNT] = {
  [2] = {.index = 0, .length = 1},
};

static const TSFieldMapEntry ts_field_map_entries[] = {
  [0] =
    {field_datatype, 2},
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
  [1] = {
    [1] = alias_sym_lexical_form,
  },
};

static const uint16_t ts_non_terminal_alias_map[] = {
  aux_sym_iri_reference_repeat1, 2,
    aux_sym_iri_reference_repeat1,
    alias_sym_lexical_form,
  aux_sym__string_literal_quote_repeat1, 2,
    aux_sym__string_literal_quote_repeat1,
    alias_sym_lexical_form,
  aux_sym__string_literal_single_quote_repeat1, 2,
    aux_sym__string_literal_single_quote_repeat1,
    alias_sym_lexical_form,
  aux_sym__string_literal_long_single_quote_repeat1, 2,
    aux_sym__string_literal_long_single_quote_repeat1,
    alias_sym_lexical_form,
  aux_sym__string_literal_long_quote_repeat1, 2,
    aux_sym__string_literal_long_quote_repeat1,
    alias_sym_lexical_form,
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 4,
  [5] = 5,
  [6] = 6,
  [7] = 7,
  [8] = 8,
  [9] = 9,
  [10] = 10,
  [11] = 11,
  [12] = 12,
  [13] = 13,
  [14] = 14,
  [15] = 15,
  [16] = 16,
  [17] = 17,
  [18] = 18,
  [19] = 19,
  [20] = 20,
  [21] = 21,
  [22] = 22,
  [23] = 23,
  [24] = 24,
  [25] = 25,
  [26] = 26,
  [27] = 27,
  [28] = 28,
  [29] = 29,
  [30] = 30,
  [31] = 31,
  [32] = 32,
  [33] = 33,
  [34] = 34,
  [35] = 35,
  [36] = 36,
  [37] = 37,
  [38] = 38,
  [39] = 36,
  [40] = 37,
  [41] = 38,
  [42] = 42,
  [43] = 43,
  [44] = 44,
  [45] = 45,
  [46] = 46,
  [47] = 47,
  [48] = 48,
  [49] = 49,
  [50] = 50,
  [51] = 51,
  [52] = 52,
  [53] = 53,
  [54] = 54,
  [55] = 55,
  [56] = 56,
  [57] = 57,
  [58] = 58,
  [59] = 35,
  [60] = 60,
  [61] = 61,
  [62] = 57,
  [63] = 63,
  [64] = 64,
  [65] = 65,
  [66] = 66,
  [67] = 67,
  [68] = 36,
  [69] = 37,
  [70] = 38,
  [71] = 37,
  [72] = 72,
  [73] = 73,
  [74] = 74,
  [75] = 75,
  [76] = 76,
  [77] = 77,
  [78] = 38,
  [79] = 36,
  [80] = 80,
  [81] = 81,
  [82] = 82,
  [83] = 83,
  [84] = 84,
  [85] = 85,
  [86] = 86,
  [87] = 87,
  [88] = 88,
  [89] = 89,
  [90] = 90,
  [91] = 91,
  [92] = 92,
  [93] = 93,
  [94] = 94,
  [95] = 95,
  [96] = 96,
  [97] = 97,
  [98] = 98,
  [99] = 99,
  [100] = 100,
  [101] = 101,
  [102] = 102,
  [103] = 103,
  [104] = 104,
  [105] = 105,
  [106] = 106,
  [107] = 107,
  [108] = 108,
  [109] = 109,
  [110] = 110,
  [111] = 111,
  [112] = 112,
  [113] = 113,
  [114] = 114,
  [115] = 115,
  [116] = 116,
  [117] = 117,
  [118] = 118,
  [119] = 119,
  [120] = 117,
  [121] = 121,
  [122] = 117,
  [123] = 117,
};

static TSCharacterRange aux_sym_blank_node_label_token1_character_set_1[] = {
  {'0', '9'}, {'A', 'Z'}, {'_', '_'}, {'a', 'z'}, {0xc0, 0xd6}, {0xd8, 0xf6}, {0xf8, 0x2ff}, {0x370, 0x37d},
  {0x37f, 0x1fff}, {0x200c, 0x200d}, {0x2070, 0x218f}, {0x2c00, 0x2fef}, {0x3001, 0xd7ff}, {0xf900, 0xfdcf}, {0xfdf0, 0xfffd}, {0x10000, 0xeffff},
};

static TSCharacterRange aux_sym_blank_node_label_token1_character_set_2[] = {
  {'-', '.'}, {'0', '9'}, {'A', 'Z'}, {'_', '_'}, {'a', 'z'}, {0xb7, 0xb7}, {0xc0, 0xd6}, {0xd8, 0xf6},
  {0xf8, 0x37d}, {0x37f, 0x1fff}, {0x200c, 0x200d}, {0x203f, 0x2040}, {0x2070, 0x218f}, {0x2c00, 0x2fef}, {0x3001, 0xd7ff}, {0xf900, 0xfdcf},
  {0xfdf0, 0xfffd}, {0x10000, 0xeffff},
};

static TSCharacterRange sym_pn_prefix_character_set_1[] = {
  {'A', 'Z'}, {'a', 'z'}, {0xc0, 0xd6}, {0xd8, 0xf6}, {0xf8, 0x2ff}, {0x370, 0x37d}, {0x37f, 0x1fff}, {0x200c, 0x200d},
  {0x2070, 0x218f}, {0x2c00, 0x2fef}, {0x3001, 0xd7ff}, {0xf900, 0xfdcf}, {0xfdf0, 0xfffd}, {0x10000, 0xeffff},
};

static TSCharacterRange sym_pn_local_character_set_1[] = {
  {'%', '%'}, {'0', ':'}, {'A', 'Z'}, {'\\', '\\'}, {'_', '_'}, {'a', 'z'}, {0xc0, 0xd6}, {0xd8, 0xf6},
  {0xf8, 0x2ff}, {0x370, 0x37d}, {0x37f, 0x1fff}, {0x200c, 0x200d}, {0x2070, 0x218f}, {0x2c00, 0x2fef}, {0x3001, 0xd7ff}, {0xf900, 0xfdcf},
  {0xfdf0, 0xfffd}, {0x10000, 0xeffff},
};

static TSCharacterRange sym_pn_local_character_set_2[] = {
  {'%', '%'}, {'-', '.'}, {'0', ':'}, {'A', 'Z'}, {'\\', '\\'}, {'_', '_'}, {'a', 'z'}, {0xb7, 0xb7},
  {0xc0, 0xd6}, {0xd8, 0xf6}, {0xf8, 0x37d}, {0x37f, 0x1fff}, {0x200c, 0x200d}, {0x203f, 0x2040}, {0x2070, 0x218f}, {0x2c00, 0x2fef},
  {0x3001, 0xd7ff}, {0xf900, 0xfdcf}, {0xfdf0, 0xfffd}, {0x10000, 0xeffff},
};

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(58);
      ADVANCE_MAP(
        '"', 80,
        '#', 72,
        '\'', 84,
        '(', 69,
        ')', 70,
        ',', 59,
        '.', 61,
        ':', 99,
        ';', 65,
        '<', 71,
        '>', 75,
        '@', 97,
        '[', 66,
        '\\', 22,
        ']', 68,
        '^', 24,
      );
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(0);
      if (lookahead > ' ' &&
          (lookahead < '@' || '^' < lookahead) &&
          (lookahead < '`' || '}' < lookahead) &&
          (lookahead < 0xc0 || 0xd6 < lookahead) &&
          (lookahead < 0xd8 || 0xf6 < lookahead) &&
          (lookahead < 0xf8 || 0x2ff < lookahead) &&
          (lookahead < 0x370 || 0x37d < lookahead) &&
          (lookahead < 0x37f || 0x1fff < lookahead) &&
          lookahead != 0x200c &&
          lookahead != 0x200d &&
          (lookahead < 0x2070 || 0x218f < lookahead) &&
          (lookahead < 0x2c00 || 0x2fef < lookahead) &&
          (lookahead < 0x3001 || 0xd7ff < lookahead) &&
          (lookahead < 0xf900 || 0xfdcf < lookahead) &&
          (lookahead < 0xfdf0 || 0xfffd < lookahead) &&
          (lookahead < 0x10000 || 0xeffff < lookahead)) ADVANCE(72);
      if (('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z') ||
          (0xc0 <= lookahead && lookahead <= 0xeffff)) ADVANCE(73);
      END_STATE();
    case 1:
      if (lookahead == '"') ADVANCE(80);
      if (lookahead == '#') ADVANCE(95);
      if (lookahead == '\\') ADVANCE(22);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') ADVANCE(96);
      if (lookahead != 0) ADVANCE(95);
      END_STATE();
    case 2:
      if (lookahead == '"') ADVANCE(93);
      END_STATE();
    case 3:
      if (lookahead == '"') ADVANCE(79);
      if (lookahead == '#') ADVANCE(82);
      if (lookahead == '\\') ADVANCE(22);
      if (lookahead == '\n' ||
          lookahead == '\r') SKIP(3);
      if (('\t' <= lookahead && lookahead <= '\f') ||
          lookahead == ' ') ADVANCE(83);
      if (lookahead != 0) ADVANCE(82);
      END_STATE();
    case 4:
      ADVANCE_MAP(
        '"', 81,
        '#', 60,
        '\'', 85,
        '(', 69,
        ')', 70,
        '.', 36,
        ':', 99,
        '<', 71,
        '@', 97,
        '[', 67,
        '^', 24,
        '_', 20,
        '+', 18,
        '-', 18,
      );
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(4);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(76);
      if (set_contains(sym_pn_prefix_character_set_1, 14, lookahead)) ADVANCE(108);
      END_STATE();
    case 5:
      if (lookahead == '#') ADVANCE(72);
      if (lookahead == '>') ADVANCE(75);
      if (lookahead == '\\') ADVANCE(21);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(5);
      if (lookahead > ' ' &&
          lookahead != '"' &&
          lookahead != '#' &&
          lookahead != '<' &&
          lookahead != '^' &&
          lookahead != '`' &&
          (lookahead < '{' || '}' < lookahead)) ADVANCE(72);
      END_STATE();
    case 6:
      if (lookahead == '#') ADVANCE(60);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == ':') ADVANCE(100);
      if (lookahead == '<') ADVANCE(71);
      if (lookahead == '\\') ADVANCE(38);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(7);
      if (('0' <= lookahead && lookahead <= '9') ||
          lookahead == '_') ADVANCE(113);
      if (set_contains(sym_pn_local_character_set_1, 18, lookahead)) ADVANCE(115);
      END_STATE();
    case 7:
      if (lookahead == '#') ADVANCE(60);
      if (lookahead == ':') ADVANCE(99);
      if (lookahead == '<') ADVANCE(71);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(7);
      if (set_contains(sym_pn_prefix_character_set_1, 14, lookahead)) ADVANCE(108);
      END_STATE();
    case 8:
      if (lookahead == '#') ADVANCE(60);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(8);
      if (('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(104);
      END_STATE();
    case 9:
      if (lookahead == '#') ADVANCE(60);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(9);
      END_STATE();
    case 10:
      if (lookahead == '#') ADVANCE(60);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(9);
      if (set_contains(aux_sym_blank_node_label_token1_character_set_1, 16, lookahead)) ADVANCE(103);
      END_STATE();
    case 11:
      if (lookahead == '#') ADVANCE(91);
      if (lookahead == '\'') ADVANCE(86);
      if (lookahead == '\\') ADVANCE(22);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') ADVANCE(92);
      if (lookahead != 0) ADVANCE(91);
      END_STATE();
    case 12:
      if (lookahead == '#') ADVANCE(87);
      if (lookahead == '\'') ADVANCE(84);
      if (lookahead == '\\') ADVANCE(22);
      if (lookahead == '\n' ||
          lookahead == '\r') SKIP(12);
      if (('\t' <= lookahead && lookahead <= '\f') ||
          lookahead == ' ') ADVANCE(88);
      if (lookahead != 0) ADVANCE(87);
      END_STATE();
    case 13:
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(14);
      if (lookahead == '\\') ADVANCE(38);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(109);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(111);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(113);
      END_STATE();
    case 14:
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(14);
      if (lookahead == '\\') ADVANCE(38);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(113);
      END_STATE();
    case 15:
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(15);
      if (lookahead == ':') ADVANCE(113);
      if (lookahead == '\\') ADVANCE(38);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(115);
      END_STATE();
    case 16:
      if (lookahead == '\'') ADVANCE(89);
      END_STATE();
    case 17:
      if (lookahead == '.') ADVANCE(17);
      if (set_contains(aux_sym_blank_node_label_token1_character_set_2, 18, lookahead)) ADVANCE(108);
      END_STATE();
    case 18:
      if (lookahead == '.') ADVANCE(36);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(76);
      END_STATE();
    case 19:
      if (lookahead == '.') ADVANCE(19);
      if (set_contains(aux_sym_blank_node_label_token1_character_set_2, 18, lookahead)) ADVANCE(103);
      END_STATE();
    case 20:
      if (lookahead == ':') ADVANCE(101);
      END_STATE();
    case 21:
      if (lookahead == 'U') ADVANCE(48);
      if (lookahead == 'u') ADVANCE(44);
      END_STATE();
    case 22:
      ADVANCE_MAP(
        'U', 48,
        'u', 44,
        '"', 106,
        '\'', 106,
        '\\', 106,
        'b', 106,
        'f', 106,
        'n', 106,
        'r', 106,
        't', 106,
      );
      END_STATE();
    case 23:
      if (lookahead == ']') ADVANCE(107);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(23);
      END_STATE();
    case 24:
      if (lookahead == '^') ADVANCE(98);
      END_STATE();
    case 25:
      if (lookahead == 'a') ADVANCE(32);
      END_STATE();
    case 26:
      if (lookahead == 'b') ADVANCE(25);
      if (lookahead == 'p') ADVANCE(31);
      END_STATE();
    case 27:
      if (lookahead == 'e') ADVANCE(29);
      END_STATE();
    case 28:
      if (lookahead == 'e') ADVANCE(64);
      END_STATE();
    case 29:
      if (lookahead == 'f') ADVANCE(30);
      END_STATE();
    case 30:
      if (lookahead == 'i') ADVANCE(33);
      END_STATE();
    case 31:
      if (lookahead == 'r') ADVANCE(27);
      END_STATE();
    case 32:
      if (lookahead == 's') ADVANCE(28);
      END_STATE();
    case 33:
      if (lookahead == 'x') ADVANCE(63);
      END_STATE();
    case 34:
      if (lookahead == '+' ||
          lookahead == '-') ADVANCE(37);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(78);
      END_STATE();
    case 35:
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(34);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(77);
      END_STATE();
    case 36:
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(77);
      END_STATE();
    case 37:
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(78);
      END_STATE();
    case 38:
      if (lookahead == '!' ||
          ('#' <= lookahead && lookahead <= '/') ||
          lookahead == ';' ||
          lookahead == '=' ||
          lookahead == '?' ||
          lookahead == '@' ||
          lookahead == '_' ||
          lookahead == '~') ADVANCE(113);
      END_STATE();
    case 39:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(74);
      END_STATE();
    case 40:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(113);
      END_STATE();
    case 41:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(39);
      END_STATE();
    case 42:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(40);
      END_STATE();
    case 43:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(41);
      END_STATE();
    case 44:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(43);
      END_STATE();
    case 45:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(44);
      END_STATE();
    case 46:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(45);
      END_STATE();
    case 47:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(46);
      END_STATE();
    case 48:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(47);
      END_STATE();
    case 49:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(105);
      END_STATE();
    case 50:
      if (eof) ADVANCE(58);
      ADVANCE_MAP(
        '"', 81,
        '#', 60,
        '%', 42,
        '\'', 85,
        '(', 69,
        ')', 70,
        '.', 36,
        ':', 100,
        '<', 71,
        '[', 67,
        '\\', 38,
        '_', 110,
        '+', 18,
        '-', 18,
      );
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(54);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(114);
      if (set_contains(sym_pn_local_character_set_1, 18, lookahead)) ADVANCE(115);
      END_STATE();
    case 51:
      if (eof) ADVANCE(58);
      ADVANCE_MAP(
        '"', 81,
        '#', 60,
        '\'', 85,
        '(', 69,
        ')', 70,
        ',', 59,
        '.', 36,
        ':', 99,
        '<', 71,
        '@', 26,
        '[', 67,
        ']', 68,
        '_', 20,
        '+', 18,
        '-', 18,
      );
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(51);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(76);
      if (set_contains(sym_pn_prefix_character_set_1, 14, lookahead)) ADVANCE(108);
      END_STATE();
    case 52:
      if (eof) ADVANCE(58);
      ADVANCE_MAP(
        '"', 81,
        '#', 60,
        '\'', 85,
        '(', 69,
        ')', 70,
        ',', 59,
        '.', 62,
        ':', 99,
        ';', 65,
        '<', 71,
        '@', 97,
        '[', 67,
        ']', 68,
        '^', 24,
        '_', 20,
        '+', 18,
        '-', 18,
      );
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(52);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(76);
      if (set_contains(sym_pn_prefix_character_set_1, 14, lookahead)) ADVANCE(108);
      END_STATE();
    case 53:
      if (eof) ADVANCE(58);
      ADVANCE_MAP(
        '"', 81,
        '#', 60,
        '\'', 85,
        '(', 69,
        ')', 70,
        ',', 59,
        '.', 62,
        ':', 99,
        ';', 65,
        '<', 71,
        '@', 26,
        '[', 67,
        ']', 68,
        '_', 20,
        '+', 18,
        '-', 18,
      );
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(53);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(76);
      if (set_contains(sym_pn_prefix_character_set_1, 14, lookahead)) ADVANCE(108);
      END_STATE();
    case 54:
      if (eof) ADVANCE(58);
      ADVANCE_MAP(
        '"', 81,
        '#', 60,
        '\'', 85,
        '(', 69,
        ')', 70,
        '.', 36,
        ':', 99,
        '<', 71,
        '[', 67,
        '_', 20,
        '+', 18,
        '-', 18,
      );
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(54);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(76);
      if (set_contains(sym_pn_prefix_character_set_1, 14, lookahead)) ADVANCE(108);
      END_STATE();
    case 55:
      if (eof) ADVANCE(58);
      if (lookahead == '#') ADVANCE(60);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == ',') ADVANCE(59);
      if (lookahead == '.') ADVANCE(61);
      if (lookahead == ';') ADVANCE(65);
      if (lookahead == '\\') ADVANCE(38);
      if (lookahead == ']') ADVANCE(68);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(57);
      if (set_contains(sym_pn_local_character_set_1, 18, lookahead)) ADVANCE(113);
      END_STATE();
    case 56:
      if (eof) ADVANCE(58);
      ADVANCE_MAP(
        '#', 60,
        ',', 59,
        '.', 61,
        ':', 99,
        ';', 65,
        '<', 71,
        '@', 97,
        ']', 68,
        '^', 24,
      );
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(56);
      if (set_contains(sym_pn_prefix_character_set_1, 14, lookahead)) ADVANCE(108);
      END_STATE();
    case 57:
      if (eof) ADVANCE(58);
      if (lookahead == '#') ADVANCE(60);
      if (lookahead == ',') ADVANCE(59);
      if (lookahead == '.') ADVANCE(61);
      if (lookahead == ';') ADVANCE(65);
      if (lookahead == ']') ADVANCE(68);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(57);
      END_STATE();
    case 58:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 59:
      ACCEPT_TOKEN(anon_sym_COMMA);
      END_STATE();
    case 60:
      ACCEPT_TOKEN(sym_comment);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(60);
      END_STATE();
    case 61:
      ACCEPT_TOKEN(anon_sym_DOT);
      END_STATE();
    case 62:
      ACCEPT_TOKEN(anon_sym_DOT);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(77);
      END_STATE();
    case 63:
      ACCEPT_TOKEN(anon_sym_ATprefix);
      END_STATE();
    case 64:
      ACCEPT_TOKEN(anon_sym_ATbase);
      END_STATE();
    case 65:
      ACCEPT_TOKEN(anon_sym_SEMI);
      END_STATE();
    case 66:
      ACCEPT_TOKEN(anon_sym_LBRACK);
      END_STATE();
    case 67:
      ACCEPT_TOKEN(anon_sym_LBRACK);
      if (lookahead == ']') ADVANCE(107);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(23);
      END_STATE();
    case 68:
      ACCEPT_TOKEN(anon_sym_RBRACK);
      END_STATE();
    case 69:
      ACCEPT_TOKEN(anon_sym_LPAREN);
      END_STATE();
    case 70:
      ACCEPT_TOKEN(anon_sym_RPAREN);
      END_STATE();
    case 71:
      ACCEPT_TOKEN(anon_sym_LT);
      END_STATE();
    case 72:
      ACCEPT_TOKEN(aux_sym_iri_reference_token1);
      END_STATE();
    case 73:
      ACCEPT_TOKEN(aux_sym_iri_reference_token1);
      if (lookahead == '.') ADVANCE(17);
      if (set_contains(aux_sym_blank_node_label_token1_character_set_2, 18, lookahead)) ADVANCE(108);
      END_STATE();
    case 74:
      ACCEPT_TOKEN(aux_sym_iri_reference_token2);
      END_STATE();
    case 75:
      ACCEPT_TOKEN(anon_sym_GT);
      END_STATE();
    case 76:
      ACCEPT_TOKEN(sym_integer);
      if (lookahead == '.') ADVANCE(35);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(34);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(76);
      END_STATE();
    case 77:
      ACCEPT_TOKEN(sym_decimal);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(34);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(77);
      END_STATE();
    case 78:
      ACCEPT_TOKEN(sym_double);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(78);
      END_STATE();
    case 79:
      ACCEPT_TOKEN(anon_sym_DQUOTE);
      END_STATE();
    case 80:
      ACCEPT_TOKEN(anon_sym_DQUOTE);
      if (lookahead == '"') ADVANCE(94);
      END_STATE();
    case 81:
      ACCEPT_TOKEN(anon_sym_DQUOTE);
      if (lookahead == '"') ADVANCE(2);
      END_STATE();
    case 82:
      ACCEPT_TOKEN(aux_sym__string_literal_quote_token1);
      END_STATE();
    case 83:
      ACCEPT_TOKEN(aux_sym__string_literal_quote_token1);
      if (lookahead == '#') ADVANCE(82);
      if (lookahead == '\t' ||
          lookahead == 0x0b ||
          lookahead == '\f' ||
          lookahead == ' ') ADVANCE(83);
      if (lookahead != 0 &&
          (lookahead < '\t' || '\r' < lookahead) &&
          lookahead != '"' &&
          lookahead != '#' &&
          lookahead != '\\') ADVANCE(82);
      END_STATE();
    case 84:
      ACCEPT_TOKEN(anon_sym_SQUOTE);
      END_STATE();
    case 85:
      ACCEPT_TOKEN(anon_sym_SQUOTE);
      if (lookahead == '\'') ADVANCE(16);
      END_STATE();
    case 86:
      ACCEPT_TOKEN(anon_sym_SQUOTE);
      if (lookahead == '\'') ADVANCE(90);
      END_STATE();
    case 87:
      ACCEPT_TOKEN(aux_sym__string_literal_single_quote_token1);
      END_STATE();
    case 88:
      ACCEPT_TOKEN(aux_sym__string_literal_single_quote_token1);
      if (lookahead == '#') ADVANCE(87);
      if (lookahead == '\t' ||
          lookahead == 0x0b ||
          lookahead == '\f' ||
          lookahead == ' ') ADVANCE(88);
      if (lookahead != 0 &&
          (lookahead < '\t' || '\r' < lookahead) &&
          lookahead != '\'' &&
          lookahead != '\\') ADVANCE(87);
      END_STATE();
    case 89:
      ACCEPT_TOKEN(anon_sym_SQUOTE_SQUOTE_SQUOTE);
      END_STATE();
    case 90:
      ACCEPT_TOKEN(anon_sym_SQUOTE_SQUOTE);
      if (lookahead == '\'') ADVANCE(89);
      END_STATE();
    case 91:
      ACCEPT_TOKEN(aux_sym__string_literal_long_single_quote_token1);
      END_STATE();
    case 92:
      ACCEPT_TOKEN(aux_sym__string_literal_long_single_quote_token1);
      if (lookahead == '#') ADVANCE(91);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') ADVANCE(92);
      if (lookahead != 0 &&
          lookahead != '\'' &&
          lookahead != '\\') ADVANCE(91);
      END_STATE();
    case 93:
      ACCEPT_TOKEN(anon_sym_DQUOTE_DQUOTE_DQUOTE);
      END_STATE();
    case 94:
      ACCEPT_TOKEN(anon_sym_DQUOTE_DQUOTE);
      if (lookahead == '"') ADVANCE(93);
      END_STATE();
    case 95:
      ACCEPT_TOKEN(aux_sym__string_literal_long_quote_token1);
      END_STATE();
    case 96:
      ACCEPT_TOKEN(aux_sym__string_literal_long_quote_token1);
      if (lookahead == '#') ADVANCE(95);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') ADVANCE(96);
      if (lookahead != 0 &&
          lookahead != '"' &&
          lookahead != '#' &&
          lookahead != '\\') ADVANCE(95);
      END_STATE();
    case 97:
      ACCEPT_TOKEN(anon_sym_AT);
      END_STATE();
    case 98:
      ACCEPT_TOKEN(anon_sym_CARET_CARET);
      END_STATE();
    case 99:
      ACCEPT_TOKEN(anon_sym_COLON);
      END_STATE();
    case 100:
      ACCEPT_TOKEN(anon_sym_COLON);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(14);
      if (lookahead == '\\') ADVANCE(38);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(113);
      END_STATE();
    case 101:
      ACCEPT_TOKEN(anon_sym__COLON);
      END_STATE();
    case 102:
      ACCEPT_TOKEN(anon_sym__COLON);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(14);
      if (lookahead == '\\') ADVANCE(38);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(113);
      END_STATE();
    case 103:
      ACCEPT_TOKEN(aux_sym_blank_node_label_token1);
      if (lookahead == '.') ADVANCE(19);
      if (set_contains(aux_sym_blank_node_label_token1_character_set_2, 18, lookahead)) ADVANCE(103);
      END_STATE();
    case 104:
      ACCEPT_TOKEN(sym_lang_tag);
      if (lookahead == '-') ADVANCE(49);
      if (('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(104);
      END_STATE();
    case 105:
      ACCEPT_TOKEN(sym_lang_tag);
      if (lookahead == '-') ADVANCE(49);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(105);
      END_STATE();
    case 106:
      ACCEPT_TOKEN(sym_echar);
      END_STATE();
    case 107:
      ACCEPT_TOKEN(sym_anon);
      END_STATE();
    case 108:
      ACCEPT_TOKEN(sym_pn_prefix);
      if (lookahead == '.') ADVANCE(17);
      if (set_contains(aux_sym_blank_node_label_token1_character_set_2, 18, lookahead)) ADVANCE(108);
      END_STATE();
    case 109:
      ACCEPT_TOKEN(sym_pn_local);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '+') ADVANCE(37);
      if (lookahead == '-') ADVANCE(112);
      if (lookahead == '.') ADVANCE(14);
      if (lookahead == '\\') ADVANCE(38);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(112);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(113);
      END_STATE();
    case 110:
      ACCEPT_TOKEN(sym_pn_local);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(14);
      if (lookahead == ':') ADVANCE(102);
      if (lookahead == '\\') ADVANCE(38);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(113);
      END_STATE();
    case 111:
      ACCEPT_TOKEN(sym_pn_local);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(14);
      if (lookahead == '\\') ADVANCE(38);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(109);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(111);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(113);
      END_STATE();
    case 112:
      ACCEPT_TOKEN(sym_pn_local);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(14);
      if (lookahead == '\\') ADVANCE(38);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(112);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(113);
      END_STATE();
    case 113:
      ACCEPT_TOKEN(sym_pn_local);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(14);
      if (lookahead == '\\') ADVANCE(38);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(113);
      END_STATE();
    case 114:
      ACCEPT_TOKEN(sym_pn_local);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(13);
      if (lookahead == '\\') ADVANCE(38);
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(109);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(114);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(113);
      END_STATE();
    case 115:
      ACCEPT_TOKEN(sym_pn_local);
      if (lookahead == '%') ADVANCE(42);
      if (lookahead == '.') ADVANCE(15);
      if (lookahead == ':') ADVANCE(113);
      if (lookahead == '\\') ADVANCE(38);
      if (set_contains(sym_pn_local_character_set_2, 20, lookahead)) ADVANCE(115);
      END_STATE();
    default:
      return false;
  }
}

static bool ts_lex_keywords(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (lookahead == 'a') ADVANCE(1);
      if (lookahead == 'f') ADVANCE(2);
      if (lookahead == 't') ADVANCE(3);
      if (lookahead == 'B' ||
          lookahead == 'b') ADVANCE(4);
      if (lookahead == 'P' ||
          lookahead == 'p') ADVANCE(5);
      if (('\t' <= lookahead && lookahead <= '\r') ||
          lookahead == ' ') SKIP(0);
      END_STATE();
    case 1:
      ACCEPT_TOKEN(anon_sym_a);
      END_STATE();
    case 2:
      if (lookahead == 'a') ADVANCE(6);
      END_STATE();
    case 3:
      if (lookahead == 'r') ADVANCE(7);
      END_STATE();
    case 4:
      if (lookahead == 'A' ||
          lookahead == 'a') ADVANCE(8);
      END_STATE();
    case 5:
      if (lookahead == 'R' ||
          lookahead == 'r') ADVANCE(9);
      END_STATE();
    case 6:
      if (lookahead == 'l') ADVANCE(10);
      END_STATE();
    case 7:
      if (lookahead == 'u') ADVANCE(11);
      END_STATE();
    case 8:
      if (lookahead == 'S' ||
          lookahead == 's') ADVANCE(12);
      END_STATE();
    case 9:
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(13);
      END_STATE();
    case 10:
      if (lookahead == 's') ADVANCE(14);
      END_STATE();
    case 11:
      if (lookahead == 'e') ADVANCE(15);
      END_STATE();
    case 12:
      if (lookahead == 'E' ||
          lookahead == 'e') ADVANCE(16);
      END_STATE();
    case 13:
      if (lookahead == 'F' ||
          lookahead == 'f') ADVANCE(17);
      END_STATE();
    case 14:
      if (lookahead == 'e') ADVANCE(18);
      END_STATE();
    case 15:
      ACCEPT_TOKEN(anon_sym_true);
      END_STATE();
    case 16:
      ACCEPT_TOKEN(aux_sym_sparql_base_token1);
      END_STATE();
    case 17:
      if (lookahead == 'I' ||
          lookahead == 'i') ADVANCE(19);
      END_STATE();
    case 18:
      ACCEPT_TOKEN(anon_sym_false);
      END_STATE();
    case 19:
      if (lookahead == 'X' ||
          lookahead == 'x') ADVANCE(20);
      END_STATE();
    case 20:
      ACCEPT_TOKEN(aux_sym_sparql_prefix_token1);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 51},
  [2] = {.lex_state = 51},
  [3] = {.lex_state = 51},
  [4] = {.lex_state = 51},
  [5] = {.lex_state = 51},
  [6] = {.lex_state = 51},
  [7] = {.lex_state = 51},
  [8] = {.lex_state = 51},
  [9] = {.lex_state = 51},
  [10] = {.lex_state = 51},
  [11] = {.lex_state = 53},
  [12] = {.lex_state = 53},
  [13] = {.lex_state = 52},
  [14] = {.lex_state = 52},
  [15] = {.lex_state = 52},
  [16] = {.lex_state = 52},
  [17] = {.lex_state = 52},
  [18] = {.lex_state = 52},
  [19] = {.lex_state = 52},
  [20] = {.lex_state = 52},
  [21] = {.lex_state = 52},
  [22] = {.lex_state = 53},
  [23] = {.lex_state = 53},
  [24] = {.lex_state = 53},
  [25] = {.lex_state = 53},
  [26] = {.lex_state = 53},
  [27] = {.lex_state = 53},
  [28] = {.lex_state = 53},
  [29] = {.lex_state = 53},
  [30] = {.lex_state = 53},
  [31] = {.lex_state = 53},
  [32] = {.lex_state = 53},
  [33] = {.lex_state = 53},
  [34] = {.lex_state = 53},
  [35] = {.lex_state = 4},
  [36] = {.lex_state = 50},
  [37] = {.lex_state = 50},
  [38] = {.lex_state = 50},
  [39] = {.lex_state = 50},
  [40] = {.lex_state = 50},
  [41] = {.lex_state = 50},
  [42] = {.lex_state = 51},
  [43] = {.lex_state = 51},
  [44] = {.lex_state = 56},
  [45] = {.lex_state = 51},
  [46] = {.lex_state = 51},
  [47] = {.lex_state = 56},
  [48] = {.lex_state = 51},
  [49] = {.lex_state = 51},
  [50] = {.lex_state = 51},
  [51] = {.lex_state = 51},
  [52] = {.lex_state = 51},
  [53] = {.lex_state = 51},
  [54] = {.lex_state = 51},
  [55] = {.lex_state = 11},
  [56] = {.lex_state = 1},
  [57] = {.lex_state = 51},
  [58] = {.lex_state = 11},
  [59] = {.lex_state = 56},
  [60] = {.lex_state = 1},
  [61] = {.lex_state = 1},
  [62] = {.lex_state = 51},
  [63] = {.lex_state = 11},
  [64] = {.lex_state = 11},
  [65] = {.lex_state = 11},
  [66] = {.lex_state = 1},
  [67] = {.lex_state = 1},
  [68] = {.lex_state = 55},
  [69] = {.lex_state = 55},
  [70] = {.lex_state = 55},
  [71] = {.lex_state = 6},
  [72] = {.lex_state = 56},
  [73] = {.lex_state = 12},
  [74] = {.lex_state = 3},
  [75] = {.lex_state = 56},
  [76] = {.lex_state = 3},
  [77] = {.lex_state = 12},
  [78] = {.lex_state = 6},
  [79] = {.lex_state = 6},
  [80] = {.lex_state = 12},
  [81] = {.lex_state = 3},
  [82] = {.lex_state = 56},
  [83] = {.lex_state = 51},
  [84] = {.lex_state = 56},
  [85] = {.lex_state = 51},
  [86] = {.lex_state = 5},
  [87] = {.lex_state = 12},
  [88] = {.lex_state = 56},
  [89] = {.lex_state = 3},
  [90] = {.lex_state = 5},
  [91] = {.lex_state = 5},
  [92] = {.lex_state = 56},
  [93] = {.lex_state = 56},
  [94] = {.lex_state = 51},
  [95] = {.lex_state = 11},
  [96] = {.lex_state = 1},
  [97] = {.lex_state = 51},
  [98] = {.lex_state = 56},
  [99] = {.lex_state = 5},
  [100] = {.lex_state = 51},
  [101] = {.lex_state = 51},
  [102] = {.lex_state = 51},
  [103] = {.lex_state = 56},
  [104] = {.lex_state = 51},
  [105] = {.lex_state = 51},
  [106] = {.lex_state = 51},
  [107] = {.lex_state = 51},
  [108] = {.lex_state = 51},
  [109] = {.lex_state = 51},
  [110] = {.lex_state = 8},
  [111] = {.lex_state = 51},
  [112] = {.lex_state = 56},
  [113] = {.lex_state = 56},
  [114] = {.lex_state = 51},
  [115] = {.lex_state = 10},
  [116] = {.lex_state = 56},
  [117] = {.lex_state = 51},
  [118] = {.lex_state = 51},
  [119] = {.lex_state = 51},
  [120] = {.lex_state = 51},
  [121] = {.lex_state = 56},
  [122] = {.lex_state = 51},
  [123] = {.lex_state = 51},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [sym_pn_prefix] = ACTIONS(1),
    [anon_sym_COMMA] = ACTIONS(1),
    [sym_comment] = ACTIONS(3),
    [anon_sym_DOT] = ACTIONS(1),
    [aux_sym_sparql_base_token1] = ACTIONS(1),
    [aux_sym_sparql_prefix_token1] = ACTIONS(1),
    [anon_sym_SEMI] = ACTIONS(1),
    [anon_sym_a] = ACTIONS(1),
    [anon_sym_LBRACK] = ACTIONS(1),
    [anon_sym_RBRACK] = ACTIONS(1),
    [anon_sym_LPAREN] = ACTIONS(1),
    [anon_sym_RPAREN] = ACTIONS(1),
    [anon_sym_LT] = ACTIONS(1),
    [aux_sym_iri_reference_token1] = ACTIONS(1),
    [aux_sym_iri_reference_token2] = ACTIONS(1),
    [anon_sym_GT] = ACTIONS(1),
    [anon_sym_DQUOTE] = ACTIONS(1),
    [anon_sym_SQUOTE] = ACTIONS(1),
    [anon_sym_DQUOTE_DQUOTE_DQUOTE] = ACTIONS(1),
    [anon_sym_DQUOTE_DQUOTE] = ACTIONS(1),
    [anon_sym_AT] = ACTIONS(1),
    [anon_sym_CARET_CARET] = ACTIONS(1),
    [anon_sym_true] = ACTIONS(1),
    [anon_sym_false] = ACTIONS(1),
    [anon_sym_COLON] = ACTIONS(1),
    [sym_echar] = ACTIONS(1),
  },
  [1] = {
    [sym_rdf_diagram] = STATE(118),
    [sym_term] = STATE(119),
    [sym_property_term] = STATE(119),
    [sym_turtle_doc] = STATE(119),
    [sym_statement] = STATE(10),
    [sym_directive] = STATE(51),
    [sym_prefix_id] = STATE(52),
    [sym_base] = STATE(52),
    [sym_sparql_base] = STATE(52),
    [sym_sparql_prefix] = STATE(52),
    [sym_triples] = STATE(121),
    [sym_predicate] = STATE(7),
    [sym_subject] = STATE(54),
    [sym_literal] = STATE(106),
    [sym_blank_node_property_list] = STATE(47),
    [sym_collection] = STATE(85),
    [sym_numeric_literal] = STATE(28),
    [sym_string] = STATE(59),
    [sym_iri_reference] = STATE(25),
    [sym__string_literal_quote] = STATE(15),
    [sym__string_literal_single_quote] = STATE(15),
    [sym__string_literal_long_single_quote] = STATE(15),
    [sym__string_literal_long_quote] = STATE(15),
    [sym_rdf_literal] = STATE(28),
    [sym_boolean_literal] = STATE(28),
    [sym_iri] = STATE(42),
    [sym_prefixed_name] = STATE(25),
    [sym_blank_node] = STATE(83),
    [sym_namespace] = STATE(40),
    [sym_blank_node_label] = STATE(23),
    [aux_sym_turtle_doc_repeat1] = STATE(10),
    [sym_pn_prefix] = ACTIONS(5),
    [sym_comment] = ACTIONS(7),
    [anon_sym_ATprefix] = ACTIONS(9),
    [anon_sym_ATbase] = ACTIONS(11),
    [aux_sym_sparql_base_token1] = ACTIONS(13),
    [aux_sym_sparql_prefix_token1] = ACTIONS(15),
    [anon_sym_a] = ACTIONS(17),
    [anon_sym_LBRACK] = ACTIONS(19),
    [anon_sym_LPAREN] = ACTIONS(21),
    [anon_sym_LT] = ACTIONS(23),
    [sym_integer] = ACTIONS(25),
    [sym_decimal] = ACTIONS(25),
    [sym_double] = ACTIONS(27),
    [anon_sym_DQUOTE] = ACTIONS(29),
    [anon_sym_SQUOTE] = ACTIONS(31),
    [anon_sym_SQUOTE_SQUOTE_SQUOTE] = ACTIONS(33),
    [anon_sym_DQUOTE_DQUOTE_DQUOTE] = ACTIONS(35),
    [anon_sym_true] = ACTIONS(37),
    [anon_sym_false] = ACTIONS(37),
    [anon_sym_COLON] = ACTIONS(39),
    [anon_sym__COLON] = ACTIONS(41),
    [sym_anon] = ACTIONS(43),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 25,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(19), 1,
      anon_sym_LBRACK,
    ACTIONS(21), 1,
      anon_sym_LPAREN,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(27), 1,
      sym_double,
    ACTIONS(29), 1,
      anon_sym_DQUOTE,
    ACTIONS(31), 1,
      anon_sym_SQUOTE,
    ACTIONS(33), 1,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
    ACTIONS(35), 1,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
    ACTIONS(41), 1,
      anon_sym__COLON,
    ACTIONS(43), 1,
      sym_anon,
    ACTIONS(45), 1,
      sym_pn_prefix,
    ACTIONS(47), 1,
      anon_sym_RPAREN,
    ACTIONS(49), 1,
      anon_sym_COLON,
    STATE(23), 1,
      sym_blank_node_label,
    STATE(35), 1,
      sym_string,
    STATE(37), 1,
      sym_namespace,
    STATE(111), 1,
      sym_object_collection,
    ACTIONS(25), 2,
      sym_integer,
      sym_decimal,
    ACTIONS(37), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(4), 2,
      sym_object,
      aux_sym_object_collection_repeat1,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
    STATE(28), 3,
      sym_numeric_literal,
      sym_rdf_literal,
      sym_boolean_literal,
    STATE(15), 4,
      sym__string_literal_quote,
      sym__string_literal_single_quote,
      sym__string_literal_long_single_quote,
      sym__string_literal_long_quote,
    STATE(34), 5,
      sym_literal,
      sym_blank_node_property_list,
      sym_collection,
      sym_iri,
      sym_blank_node,
  [89] = 24,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(51), 1,
      sym_pn_prefix,
    ACTIONS(54), 1,
      anon_sym_LBRACK,
    ACTIONS(57), 1,
      anon_sym_LPAREN,
    ACTIONS(60), 1,
      anon_sym_RPAREN,
    ACTIONS(62), 1,
      anon_sym_LT,
    ACTIONS(68), 1,
      sym_double,
    ACTIONS(71), 1,
      anon_sym_DQUOTE,
    ACTIONS(74), 1,
      anon_sym_SQUOTE,
    ACTIONS(77), 1,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
    ACTIONS(80), 1,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
    ACTIONS(86), 1,
      anon_sym_COLON,
    ACTIONS(89), 1,
      anon_sym__COLON,
    ACTIONS(92), 1,
      sym_anon,
    STATE(23), 1,
      sym_blank_node_label,
    STATE(35), 1,
      sym_string,
    STATE(37), 1,
      sym_namespace,
    ACTIONS(65), 2,
      sym_integer,
      sym_decimal,
    ACTIONS(83), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(3), 2,
      sym_object,
      aux_sym_object_collection_repeat1,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
    STATE(28), 3,
      sym_numeric_literal,
      sym_rdf_literal,
      sym_boolean_literal,
    STATE(15), 4,
      sym__string_literal_quote,
      sym__string_literal_single_quote,
      sym__string_literal_long_single_quote,
      sym__string_literal_long_quote,
    STATE(34), 5,
      sym_literal,
      sym_blank_node_property_list,
      sym_collection,
      sym_iri,
      sym_blank_node,
  [175] = 24,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(19), 1,
      anon_sym_LBRACK,
    ACTIONS(21), 1,
      anon_sym_LPAREN,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(27), 1,
      sym_double,
    ACTIONS(29), 1,
      anon_sym_DQUOTE,
    ACTIONS(31), 1,
      anon_sym_SQUOTE,
    ACTIONS(33), 1,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
    ACTIONS(35), 1,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
    ACTIONS(41), 1,
      anon_sym__COLON,
    ACTIONS(43), 1,
      sym_anon,
    ACTIONS(45), 1,
      sym_pn_prefix,
    ACTIONS(49), 1,
      anon_sym_COLON,
    ACTIONS(95), 1,
      anon_sym_RPAREN,
    STATE(23), 1,
      sym_blank_node_label,
    STATE(35), 1,
      sym_string,
    STATE(37), 1,
      sym_namespace,
    ACTIONS(25), 2,
      sym_integer,
      sym_decimal,
    ACTIONS(37), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(3), 2,
      sym_object,
      aux_sym_object_collection_repeat1,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
    STATE(28), 3,
      sym_numeric_literal,
      sym_rdf_literal,
      sym_boolean_literal,
    STATE(15), 4,
      sym__string_literal_quote,
      sym__string_literal_single_quote,
      sym__string_literal_long_single_quote,
      sym__string_literal_long_quote,
    STATE(34), 5,
      sym_literal,
      sym_blank_node_property_list,
      sym_collection,
      sym_iri,
      sym_blank_node,
  [261] = 24,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(19), 1,
      anon_sym_LBRACK,
    ACTIONS(21), 1,
      anon_sym_LPAREN,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(27), 1,
      sym_double,
    ACTIONS(29), 1,
      anon_sym_DQUOTE,
    ACTIONS(31), 1,
      anon_sym_SQUOTE,
    ACTIONS(33), 1,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
    ACTIONS(35), 1,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
    ACTIONS(41), 1,
      anon_sym__COLON,
    ACTIONS(43), 1,
      sym_anon,
    ACTIONS(97), 1,
      sym_pn_prefix,
    ACTIONS(99), 1,
      anon_sym_COLON,
    STATE(23), 1,
      sym_blank_node_label,
    STATE(59), 1,
      sym_string,
    STATE(69), 1,
      sym_namespace,
    STATE(82), 1,
      sym_object,
    STATE(98), 1,
      sym_object_list,
    ACTIONS(25), 2,
      sym_integer,
      sym_decimal,
    ACTIONS(37), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
    STATE(28), 3,
      sym_numeric_literal,
      sym_rdf_literal,
      sym_boolean_literal,
    STATE(15), 4,
      sym__string_literal_quote,
      sym__string_literal_single_quote,
      sym__string_literal_long_single_quote,
      sym__string_literal_long_quote,
    STATE(34), 5,
      sym_literal,
      sym_blank_node_property_list,
      sym_collection,
      sym_iri,
      sym_blank_node,
  [346] = 23,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(19), 1,
      anon_sym_LBRACK,
    ACTIONS(21), 1,
      anon_sym_LPAREN,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(27), 1,
      sym_double,
    ACTIONS(29), 1,
      anon_sym_DQUOTE,
    ACTIONS(31), 1,
      anon_sym_SQUOTE,
    ACTIONS(33), 1,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
    ACTIONS(35), 1,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
    ACTIONS(41), 1,
      anon_sym__COLON,
    ACTIONS(43), 1,
      sym_anon,
    ACTIONS(97), 1,
      sym_pn_prefix,
    ACTIONS(99), 1,
      anon_sym_COLON,
    STATE(23), 1,
      sym_blank_node_label,
    STATE(59), 1,
      sym_string,
    STATE(69), 1,
      sym_namespace,
    STATE(92), 1,
      sym_object,
    ACTIONS(25), 2,
      sym_integer,
      sym_decimal,
    ACTIONS(37), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
    STATE(28), 3,
      sym_numeric_literal,
      sym_rdf_literal,
      sym_boolean_literal,
    STATE(15), 4,
      sym__string_literal_quote,
      sym__string_literal_single_quote,
      sym__string_literal_long_single_quote,
      sym__string_literal_long_quote,
    STATE(34), 5,
      sym_literal,
      sym_blank_node_property_list,
      sym_collection,
      sym_iri,
      sym_blank_node,
  [428] = 21,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(27), 1,
      sym_double,
    ACTIONS(29), 1,
      anon_sym_DQUOTE,
    ACTIONS(31), 1,
      anon_sym_SQUOTE,
    ACTIONS(33), 1,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
    ACTIONS(35), 1,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
    ACTIONS(41), 1,
      anon_sym__COLON,
    ACTIONS(43), 1,
      sym_anon,
    ACTIONS(97), 1,
      sym_pn_prefix,
    ACTIONS(99), 1,
      anon_sym_COLON,
    STATE(23), 1,
      sym_blank_node_label,
    STATE(59), 1,
      sym_string,
    STATE(69), 1,
      sym_namespace,
    STATE(100), 1,
      sym_term,
    ACTIONS(25), 2,
      sym_integer,
      sym_decimal,
    ACTIONS(37), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
    STATE(28), 3,
      sym_numeric_literal,
      sym_rdf_literal,
      sym_boolean_literal,
    STATE(106), 3,
      sym_literal,
      sym_iri,
      sym_blank_node,
    STATE(15), 4,
      sym__string_literal_quote,
      sym__string_literal_single_quote,
      sym__string_literal_long_single_quote,
      sym__string_literal_long_quote,
  [502] = 21,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(27), 1,
      sym_double,
    ACTIONS(29), 1,
      anon_sym_DQUOTE,
    ACTIONS(31), 1,
      anon_sym_SQUOTE,
    ACTIONS(33), 1,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
    ACTIONS(35), 1,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
    ACTIONS(41), 1,
      anon_sym__COLON,
    ACTIONS(43), 1,
      sym_anon,
    ACTIONS(97), 1,
      sym_pn_prefix,
    ACTIONS(99), 1,
      anon_sym_COLON,
    STATE(23), 1,
      sym_blank_node_label,
    STATE(59), 1,
      sym_string,
    STATE(69), 1,
      sym_namespace,
    STATE(105), 1,
      sym_term,
    ACTIONS(25), 2,
      sym_integer,
      sym_decimal,
    ACTIONS(37), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
    STATE(28), 3,
      sym_numeric_literal,
      sym_rdf_literal,
      sym_boolean_literal,
    STATE(106), 3,
      sym_literal,
      sym_iri,
      sym_blank_node,
    STATE(15), 4,
      sym__string_literal_quote,
      sym__string_literal_single_quote,
      sym__string_literal_long_single_quote,
      sym__string_literal_long_quote,
  [576] = 23,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(101), 1,
      ts_builtin_sym_end,
    ACTIONS(103), 1,
      sym_pn_prefix,
    ACTIONS(106), 1,
      anon_sym_ATprefix,
    ACTIONS(109), 1,
      anon_sym_ATbase,
    ACTIONS(112), 1,
      aux_sym_sparql_base_token1,
    ACTIONS(115), 1,
      aux_sym_sparql_prefix_token1,
    ACTIONS(118), 1,
      anon_sym_LBRACK,
    ACTIONS(121), 1,
      anon_sym_LPAREN,
    ACTIONS(124), 1,
      anon_sym_LT,
    ACTIONS(127), 1,
      anon_sym_COLON,
    ACTIONS(130), 1,
      anon_sym__COLON,
    ACTIONS(133), 1,
      sym_anon,
    STATE(23), 1,
      sym_blank_node_label,
    STATE(47), 1,
      sym_blank_node_property_list,
    STATE(51), 1,
      sym_directive,
    STATE(54), 1,
      sym_subject,
    STATE(71), 1,
      sym_namespace,
    STATE(121), 1,
      sym_triples,
    STATE(9), 2,
      sym_statement,
      aux_sym_turtle_doc_repeat1,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
    STATE(85), 3,
      sym_collection,
      sym_iri,
      sym_blank_node,
    STATE(52), 4,
      sym_prefix_id,
      sym_base,
      sym_sparql_base,
      sym_sparql_prefix,
  [653] = 23,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(9), 1,
      anon_sym_ATprefix,
    ACTIONS(11), 1,
      anon_sym_ATbase,
    ACTIONS(13), 1,
      aux_sym_sparql_base_token1,
    ACTIONS(15), 1,
      aux_sym_sparql_prefix_token1,
    ACTIONS(19), 1,
      anon_sym_LBRACK,
    ACTIONS(21), 1,
      anon_sym_LPAREN,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(41), 1,
      anon_sym__COLON,
    ACTIONS(43), 1,
      sym_anon,
    ACTIONS(136), 1,
      ts_builtin_sym_end,
    ACTIONS(138), 1,
      sym_pn_prefix,
    ACTIONS(140), 1,
      anon_sym_COLON,
    STATE(23), 1,
      sym_blank_node_label,
    STATE(47), 1,
      sym_blank_node_property_list,
    STATE(51), 1,
      sym_directive,
    STATE(54), 1,
      sym_subject,
    STATE(71), 1,
      sym_namespace,
    STATE(121), 1,
      sym_triples,
    STATE(9), 2,
      sym_statement,
      aux_sym_turtle_doc_repeat1,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
    STATE(85), 3,
      sym_collection,
      sym_iri,
      sym_blank_node,
    STATE(52), 4,
      sym_prefix_id,
      sym_base,
      sym_sparql_base,
      sym_sparql_prefix,
  [730] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(144), 12,
      anon_sym_DOT,
      aux_sym_sparql_base_token1,
      aux_sym_sparql_prefix_token1,
      anon_sym_a,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(142), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_ATprefix,
      anon_sym_ATbase,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [765] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(148), 12,
      anon_sym_DOT,
      aux_sym_sparql_base_token1,
      aux_sym_sparql_prefix_token1,
      anon_sym_a,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(146), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_ATprefix,
      anon_sym_ATbase,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [800] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(152), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(150), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_AT,
      anon_sym_CARET_CARET,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [832] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(156), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(154), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_AT,
      anon_sym_CARET_CARET,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [864] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(160), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(158), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_AT,
      anon_sym_CARET_CARET,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [896] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(164), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(162), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_AT,
      anon_sym_CARET_CARET,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [928] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(168), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(166), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_AT,
      anon_sym_CARET_CARET,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [960] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(172), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(170), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_AT,
      anon_sym_CARET_CARET,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [992] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(176), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(174), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_AT,
      anon_sym_CARET_CARET,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1024] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(180), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(178), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_AT,
      anon_sym_CARET_CARET,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1056] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(184), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(182), 15,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_AT,
      anon_sym_CARET_CARET,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1088] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(188), 10,
      anon_sym_DOT,
      anon_sym_a,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(186), 13,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1119] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(192), 10,
      anon_sym_DOT,
      anon_sym_a,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(190), 13,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1150] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(196), 10,
      anon_sym_DOT,
      anon_sym_a,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(194), 13,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1181] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(200), 10,
      anon_sym_DOT,
      anon_sym_a,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(198), 13,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1212] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(204), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(202), 13,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1242] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(208), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(206), 13,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1272] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(212), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(210), 13,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1302] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(214), 10,
      anon_sym_DOT,
      anon_sym_a,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(216), 12,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1332] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(220), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(218), 13,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1362] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(222), 10,
      anon_sym_DOT,
      anon_sym_a,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(224), 12,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1392] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(226), 10,
      anon_sym_DOT,
      anon_sym_a,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(228), 12,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1422] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(232), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(230), 13,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1452] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(234), 9,
      anon_sym_DOT,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(236), 12,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1481] = 5,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(242), 1,
      anon_sym_AT,
    ACTIONS(244), 1,
      anon_sym_CARET_CARET,
    ACTIONS(238), 8,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(240), 9,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1512] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(248), 6,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      sym_anon,
    ACTIONS(246), 12,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      sym_double,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_pn_prefix,
      sym_pn_local,
  [1538] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(254), 1,
      sym_pn_local,
    ACTIONS(252), 6,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      sym_anon,
    ACTIONS(250), 11,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      sym_double,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_pn_prefix,
  [1566] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(258), 6,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LT,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      sym_anon,
    ACTIONS(256), 12,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      sym_double,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_pn_prefix,
      sym_pn_local,
  [1592] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(248), 5,
      ts_builtin_sym_end,
      anon_sym_LT,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      sym_anon,
    ACTIONS(246), 12,
      anon_sym_a,
      sym_integer,
      sym_decimal,
      sym_double,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_pn_prefix,
      sym_pn_local,
  [1617] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(254), 1,
      sym_pn_local,
    ACTIONS(252), 5,
      ts_builtin_sym_end,
      anon_sym_LT,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      sym_anon,
    ACTIONS(250), 11,
      anon_sym_a,
      sym_integer,
      sym_decimal,
      sym_double,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_pn_prefix,
  [1644] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(258), 5,
      ts_builtin_sym_end,
      anon_sym_LT,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      sym_anon,
    ACTIONS(256), 12,
      anon_sym_a,
      sym_integer,
      sym_decimal,
      sym_double,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_pn_prefix,
      sym_pn_local,
  [1669] = 7,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(260), 1,
      ts_builtin_sym_end,
    ACTIONS(262), 1,
      sym_pn_prefix,
    ACTIONS(265), 1,
      anon_sym_a,
    ACTIONS(267), 2,
      anon_sym_LT,
      anon_sym_COLON,
    ACTIONS(272), 5,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym__COLON,
      sym_anon,
    ACTIONS(270), 6,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
  [1701] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(270), 8,
      anon_sym_LBRACK,
      sym_integer,
      sym_decimal,
      anon_sym_DQUOTE,
      anon_sym_SQUOTE,
      anon_sym_true,
      anon_sym_false,
      sym_pn_prefix,
    ACTIONS(272), 8,
      anon_sym_LPAREN,
      anon_sym_LT,
      sym_double,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1725] = 11,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(17), 1,
      anon_sym_a,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(45), 1,
      sym_pn_prefix,
    ACTIONS(49), 1,
      anon_sym_COLON,
    STATE(5), 1,
      sym_predicate,
    STATE(37), 1,
      sym_namespace,
    STATE(43), 1,
      sym_iri,
    STATE(103), 1,
      sym_property,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
    ACTIONS(274), 3,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
  [1762] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(278), 4,
      aux_sym_sparql_base_token1,
      aux_sym_sparql_prefix_token1,
      anon_sym_LBRACK,
      sym_pn_prefix,
    ACTIONS(276), 8,
      ts_builtin_sym_end,
      anon_sym_ATprefix,
      anon_sym_ATbase,
      anon_sym_LPAREN,
      anon_sym_LT,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1782] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(282), 4,
      aux_sym_sparql_base_token1,
      aux_sym_sparql_prefix_token1,
      anon_sym_LBRACK,
      sym_pn_prefix,
    ACTIONS(280), 8,
      ts_builtin_sym_end,
      anon_sym_ATprefix,
      anon_sym_ATbase,
      anon_sym_LPAREN,
      anon_sym_LT,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1802] = 12,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(17), 1,
      anon_sym_a,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(45), 1,
      sym_pn_prefix,
    ACTIONS(49), 1,
      anon_sym_COLON,
    ACTIONS(284), 1,
      anon_sym_DOT,
    STATE(5), 1,
      sym_predicate,
    STATE(37), 1,
      sym_namespace,
    STATE(43), 1,
      sym_iri,
    STATE(84), 1,
      sym_property,
    STATE(112), 1,
      sym_property_list,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
  [1840] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(288), 4,
      aux_sym_sparql_base_token1,
      aux_sym_sparql_prefix_token1,
      anon_sym_LBRACK,
      sym_pn_prefix,
    ACTIONS(286), 8,
      ts_builtin_sym_end,
      anon_sym_ATprefix,
      anon_sym_ATbase,
      anon_sym_LPAREN,
      anon_sym_LT,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1860] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(292), 4,
      aux_sym_sparql_base_token1,
      aux_sym_sparql_prefix_token1,
      anon_sym_LBRACK,
      sym_pn_prefix,
    ACTIONS(290), 8,
      ts_builtin_sym_end,
      anon_sym_ATprefix,
      anon_sym_ATbase,
      anon_sym_LPAREN,
      anon_sym_LT,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1880] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(296), 4,
      aux_sym_sparql_base_token1,
      aux_sym_sparql_prefix_token1,
      anon_sym_LBRACK,
      sym_pn_prefix,
    ACTIONS(294), 8,
      ts_builtin_sym_end,
      anon_sym_ATprefix,
      anon_sym_ATbase,
      anon_sym_LPAREN,
      anon_sym_LT,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1900] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(300), 4,
      aux_sym_sparql_base_token1,
      aux_sym_sparql_prefix_token1,
      anon_sym_LBRACK,
      sym_pn_prefix,
    ACTIONS(298), 8,
      ts_builtin_sym_end,
      anon_sym_ATprefix,
      anon_sym_ATbase,
      anon_sym_LPAREN,
      anon_sym_LT,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1920] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(304), 4,
      aux_sym_sparql_base_token1,
      aux_sym_sparql_prefix_token1,
      anon_sym_LBRACK,
      sym_pn_prefix,
    ACTIONS(302), 8,
      ts_builtin_sym_end,
      anon_sym_ATprefix,
      anon_sym_ATbase,
      anon_sym_LPAREN,
      anon_sym_LT,
      anon_sym_COLON,
      anon_sym__COLON,
      sym_anon,
  [1940] = 11,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(17), 1,
      anon_sym_a,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(45), 1,
      sym_pn_prefix,
    ACTIONS(49), 1,
      anon_sym_COLON,
    STATE(5), 1,
      sym_predicate,
    STATE(37), 1,
      sym_namespace,
    STATE(43), 1,
      sym_iri,
    STATE(84), 1,
      sym_property,
    STATE(114), 1,
      sym_property_list,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
  [1975] = 11,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(17), 1,
      anon_sym_a,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(45), 1,
      sym_pn_prefix,
    ACTIONS(49), 1,
      anon_sym_COLON,
    STATE(5), 1,
      sym_predicate,
    STATE(37), 1,
      sym_namespace,
    STATE(43), 1,
      sym_iri,
    STATE(84), 1,
      sym_property,
    STATE(112), 1,
      sym_property_list,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
  [2010] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(312), 1,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
    ACTIONS(314), 1,
      aux_sym__string_literal_long_single_quote_token1,
    STATE(55), 1,
      aux_sym__string_literal_long_single_quote_repeat1,
    ACTIONS(306), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
    ACTIONS(309), 2,
      anon_sym_SQUOTE,
      anon_sym_SQUOTE_SQUOTE,
  [2031] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(321), 1,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
    ACTIONS(323), 1,
      aux_sym__string_literal_long_quote_token1,
    STATE(61), 1,
      aux_sym__string_literal_long_quote_repeat1,
    ACTIONS(317), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
    ACTIONS(319), 2,
      anon_sym_DQUOTE,
      anon_sym_DQUOTE_DQUOTE,
  [2052] = 7,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(49), 1,
      anon_sym_COLON,
    ACTIONS(325), 1,
      sym_pn_prefix,
    STATE(26), 1,
      sym_iri,
    STATE(37), 1,
      sym_namespace,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
  [2075] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(331), 1,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
    ACTIONS(333), 1,
      aux_sym__string_literal_long_single_quote_token1,
    STATE(55), 1,
      aux_sym__string_literal_long_single_quote_repeat1,
    ACTIONS(327), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
    ACTIONS(329), 2,
      anon_sym_SQUOTE,
      anon_sym_SQUOTE_SQUOTE,
  [2096] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(242), 1,
      anon_sym_AT,
    ACTIONS(335), 1,
      anon_sym_CARET_CARET,
    ACTIONS(240), 5,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
  [2113] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(343), 1,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
    ACTIONS(345), 1,
      aux_sym__string_literal_long_quote_token1,
    STATE(60), 1,
      aux_sym__string_literal_long_quote_repeat1,
    ACTIONS(337), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
    ACTIONS(340), 2,
      anon_sym_DQUOTE,
      anon_sym_DQUOTE_DQUOTE,
  [2134] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(323), 1,
      aux_sym__string_literal_long_quote_token1,
    ACTIONS(348), 1,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
    STATE(60), 1,
      aux_sym__string_literal_long_quote_repeat1,
    ACTIONS(317), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
    ACTIONS(319), 2,
      anon_sym_DQUOTE,
      anon_sym_DQUOTE_DQUOTE,
  [2155] = 7,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(23), 1,
      anon_sym_LT,
    ACTIONS(99), 1,
      anon_sym_COLON,
    ACTIONS(350), 1,
      sym_pn_prefix,
    STATE(26), 1,
      sym_iri,
    STATE(69), 1,
      sym_namespace,
    STATE(25), 2,
      sym_iri_reference,
      sym_prefixed_name,
  [2178] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(333), 1,
      aux_sym__string_literal_long_single_quote_token1,
    ACTIONS(352), 1,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
    STATE(58), 1,
      aux_sym__string_literal_long_single_quote_repeat1,
    ACTIONS(327), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
    ACTIONS(329), 2,
      anon_sym_SQUOTE,
      anon_sym_SQUOTE_SQUOTE,
  [2199] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(354), 1,
      aux_sym__string_literal_long_single_quote_token1,
    ACTIONS(312), 5,
      aux_sym_iri_reference_token2,
      anon_sym_SQUOTE,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_SQUOTE_SQUOTE,
      sym_echar,
  [2213] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(358), 1,
      aux_sym__string_literal_long_single_quote_token1,
    ACTIONS(356), 5,
      aux_sym_iri_reference_token2,
      anon_sym_SQUOTE,
      anon_sym_SQUOTE_SQUOTE_SQUOTE,
      anon_sym_SQUOTE_SQUOTE,
      sym_echar,
  [2227] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(360), 1,
      aux_sym__string_literal_long_quote_token1,
    ACTIONS(343), 5,
      aux_sym_iri_reference_token2,
      anon_sym_DQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_DQUOTE_DQUOTE,
      sym_echar,
  [2241] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(364), 1,
      aux_sym__string_literal_long_quote_token1,
    ACTIONS(362), 5,
      aux_sym_iri_reference_token2,
      anon_sym_DQUOTE,
      anon_sym_DQUOTE_DQUOTE_DQUOTE,
      anon_sym_DQUOTE_DQUOTE,
      sym_echar,
  [2255] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(248), 6,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      sym_pn_local,
  [2267] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(366), 1,
      sym_pn_local,
    ACTIONS(252), 5,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
  [2281] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(258), 6,
      ts_builtin_sym_end,
      anon_sym_COMMA,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
      sym_pn_local,
  [2293] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(252), 1,
      anon_sym_LT,
    ACTIONS(254), 1,
      sym_pn_local,
    ACTIONS(250), 3,
      anon_sym_a,
      anon_sym_COLON,
      sym_pn_prefix,
  [2308] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(368), 1,
      anon_sym_COMMA,
    STATE(72), 1,
      aux_sym_object_list_repeat1,
    ACTIONS(371), 3,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
  [2323] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(375), 1,
      anon_sym_SQUOTE,
    ACTIONS(377), 1,
      aux_sym__string_literal_single_quote_token1,
    STATE(77), 1,
      aux_sym__string_literal_single_quote_repeat1,
    ACTIONS(373), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
  [2340] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(381), 1,
      anon_sym_DQUOTE,
    ACTIONS(383), 1,
      aux_sym__string_literal_quote_token1,
    STATE(81), 1,
      aux_sym__string_literal_quote_repeat1,
    ACTIONS(379), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
  [2357] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(385), 1,
      anon_sym_COMMA,
    STATE(72), 1,
      aux_sym_object_list_repeat1,
    ACTIONS(387), 3,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
  [2372] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(383), 1,
      aux_sym__string_literal_quote_token1,
    ACTIONS(389), 1,
      anon_sym_DQUOTE,
    STATE(74), 1,
      aux_sym__string_literal_quote_repeat1,
    ACTIONS(379), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
  [2389] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(377), 1,
      aux_sym__string_literal_single_quote_token1,
    ACTIONS(391), 1,
      anon_sym_SQUOTE,
    STATE(80), 1,
      aux_sym__string_literal_single_quote_repeat1,
    ACTIONS(373), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
  [2406] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(258), 1,
      anon_sym_LT,
    ACTIONS(256), 4,
      anon_sym_a,
      anon_sym_COLON,
      sym_pn_prefix,
      sym_pn_local,
  [2419] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(248), 1,
      anon_sym_LT,
    ACTIONS(246), 4,
      anon_sym_a,
      anon_sym_COLON,
      sym_pn_prefix,
      sym_pn_local,
  [2432] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(396), 1,
      anon_sym_SQUOTE,
    ACTIONS(398), 1,
      aux_sym__string_literal_single_quote_token1,
    STATE(80), 1,
      aux_sym__string_literal_single_quote_repeat1,
    ACTIONS(393), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
  [2449] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(404), 1,
      anon_sym_DQUOTE,
    ACTIONS(406), 1,
      aux_sym__string_literal_quote_token1,
    STATE(81), 1,
      aux_sym__string_literal_quote_repeat1,
    ACTIONS(401), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
  [2466] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(385), 1,
      anon_sym_COMMA,
    STATE(75), 1,
      aux_sym_object_list_repeat1,
    ACTIONS(409), 3,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
  [2481] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(260), 1,
      ts_builtin_sym_end,
    ACTIONS(265), 2,
      anon_sym_a,
      sym_pn_prefix,
    ACTIONS(411), 2,
      anon_sym_LT,
      anon_sym_COLON,
  [2496] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(415), 1,
      anon_sym_SEMI,
    STATE(88), 1,
      aux_sym_property_list_repeat1,
    ACTIONS(413), 2,
      anon_sym_DOT,
      anon_sym_RBRACK,
  [2510] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(265), 2,
      anon_sym_a,
      sym_pn_prefix,
    ACTIONS(411), 2,
      anon_sym_LT,
      anon_sym_COLON,
  [2522] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(420), 1,
      anon_sym_GT,
    STATE(86), 1,
      aux_sym_iri_reference_repeat1,
    ACTIONS(417), 2,
      aux_sym_iri_reference_token1,
      aux_sym_iri_reference_token2,
  [2536] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(424), 1,
      aux_sym__string_literal_single_quote_token1,
    ACTIONS(422), 3,
      aux_sym_iri_reference_token2,
      anon_sym_SQUOTE,
      sym_echar,
  [2548] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(415), 1,
      anon_sym_SEMI,
    STATE(93), 1,
      aux_sym_property_list_repeat1,
    ACTIONS(426), 2,
      anon_sym_DOT,
      anon_sym_RBRACK,
  [2562] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(430), 1,
      aux_sym__string_literal_quote_token1,
    ACTIONS(428), 3,
      aux_sym_iri_reference_token2,
      anon_sym_DQUOTE,
      sym_echar,
  [2574] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(434), 1,
      anon_sym_GT,
    STATE(86), 1,
      aux_sym_iri_reference_repeat1,
    ACTIONS(432), 2,
      aux_sym_iri_reference_token1,
      aux_sym_iri_reference_token2,
  [2588] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(436), 1,
      anon_sym_GT,
    STATE(90), 1,
      aux_sym_iri_reference_repeat1,
    ACTIONS(432), 2,
      aux_sym_iri_reference_token1,
      aux_sym_iri_reference_token2,
  [2602] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(371), 4,
      anon_sym_COMMA,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
  [2612] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(440), 1,
      anon_sym_SEMI,
    STATE(93), 1,
      aux_sym_property_list_repeat1,
    ACTIONS(438), 2,
      anon_sym_DOT,
      anon_sym_RBRACK,
  [2626] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(443), 1,
      ts_builtin_sym_end,
    ACTIONS(445), 1,
      anon_sym_COMMA,
    STATE(94), 1,
      aux_sym_property_term_repeat1,
  [2639] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(450), 1,
      aux_sym__string_literal_long_single_quote_token1,
    ACTIONS(448), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
  [2650] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(454), 1,
      aux_sym__string_literal_long_quote_token1,
    ACTIONS(452), 2,
      aux_sym_iri_reference_token2,
      sym_echar,
  [2661] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(39), 1,
      anon_sym_COLON,
    ACTIONS(456), 1,
      sym_pn_prefix,
    STATE(104), 1,
      sym_namespace,
  [2674] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(458), 3,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
  [2683] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(460), 3,
      aux_sym_iri_reference_token1,
      aux_sym_iri_reference_token2,
      anon_sym_GT,
  [2692] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(462), 1,
      ts_builtin_sym_end,
    ACTIONS(464), 1,
      anon_sym_COMMA,
    STATE(101), 1,
      aux_sym_property_term_repeat1,
  [2705] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(464), 1,
      anon_sym_COMMA,
    ACTIONS(466), 1,
      ts_builtin_sym_end,
    STATE(94), 1,
      aux_sym_property_term_repeat1,
  [2718] = 4,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(39), 1,
      anon_sym_COLON,
    ACTIONS(456), 1,
      sym_pn_prefix,
    STATE(108), 1,
      sym_namespace,
  [2731] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(438), 3,
      anon_sym_DOT,
      anon_sym_SEMI,
      anon_sym_RBRACK,
  [2740] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(23), 1,
      anon_sym_LT,
    STATE(113), 1,
      sym_iri_reference,
  [2750] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(443), 2,
      ts_builtin_sym_end,
      anon_sym_COMMA,
  [2758] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(260), 2,
      ts_builtin_sym_end,
      anon_sym_COMMA,
  [2766] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(23), 1,
      anon_sym_LT,
    STATE(48), 1,
      sym_iri_reference,
  [2776] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(23), 1,
      anon_sym_LT,
    STATE(50), 1,
      sym_iri_reference,
  [2786] = 3,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(23), 1,
      anon_sym_LT,
    STATE(116), 1,
      sym_iri_reference,
  [2796] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(468), 1,
      sym_lang_tag,
  [2803] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(470), 1,
      anon_sym_RPAREN,
  [2810] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(472), 1,
      anon_sym_DOT,
  [2817] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(474), 1,
      anon_sym_DOT,
  [2824] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(476), 1,
      anon_sym_RBRACK,
  [2831] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(478), 1,
      aux_sym_blank_node_label_token1,
  [2838] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(480), 1,
      anon_sym_DOT,
  [2845] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(482), 1,
      anon_sym_COLON,
  [2852] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(484), 1,
      ts_builtin_sym_end,
  [2859] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(486), 1,
      ts_builtin_sym_end,
  [2866] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(488), 1,
      anon_sym_COLON,
  [2873] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(490), 1,
      anon_sym_DOT,
  [2880] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(492), 1,
      anon_sym_COLON,
  [2887] = 2,
    ACTIONS(7), 1,
      sym_comment,
    ACTIONS(494), 1,
      anon_sym_COLON,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(2)] = 0,
  [SMALL_STATE(3)] = 89,
  [SMALL_STATE(4)] = 175,
  [SMALL_STATE(5)] = 261,
  [SMALL_STATE(6)] = 346,
  [SMALL_STATE(7)] = 428,
  [SMALL_STATE(8)] = 502,
  [SMALL_STATE(9)] = 576,
  [SMALL_STATE(10)] = 653,
  [SMALL_STATE(11)] = 730,
  [SMALL_STATE(12)] = 765,
  [SMALL_STATE(13)] = 800,
  [SMALL_STATE(14)] = 832,
  [SMALL_STATE(15)] = 864,
  [SMALL_STATE(16)] = 896,
  [SMALL_STATE(17)] = 928,
  [SMALL_STATE(18)] = 960,
  [SMALL_STATE(19)] = 992,
  [SMALL_STATE(20)] = 1024,
  [SMALL_STATE(21)] = 1056,
  [SMALL_STATE(22)] = 1088,
  [SMALL_STATE(23)] = 1119,
  [SMALL_STATE(24)] = 1150,
  [SMALL_STATE(25)] = 1181,
  [SMALL_STATE(26)] = 1212,
  [SMALL_STATE(27)] = 1242,
  [SMALL_STATE(28)] = 1272,
  [SMALL_STATE(29)] = 1302,
  [SMALL_STATE(30)] = 1332,
  [SMALL_STATE(31)] = 1362,
  [SMALL_STATE(32)] = 1392,
  [SMALL_STATE(33)] = 1422,
  [SMALL_STATE(34)] = 1452,
  [SMALL_STATE(35)] = 1481,
  [SMALL_STATE(36)] = 1512,
  [SMALL_STATE(37)] = 1538,
  [SMALL_STATE(38)] = 1566,
  [SMALL_STATE(39)] = 1592,
  [SMALL_STATE(40)] = 1617,
  [SMALL_STATE(41)] = 1644,
  [SMALL_STATE(42)] = 1669,
  [SMALL_STATE(43)] = 1701,
  [SMALL_STATE(44)] = 1725,
  [SMALL_STATE(45)] = 1762,
  [SMALL_STATE(46)] = 1782,
  [SMALL_STATE(47)] = 1802,
  [SMALL_STATE(48)] = 1840,
  [SMALL_STATE(49)] = 1860,
  [SMALL_STATE(50)] = 1880,
  [SMALL_STATE(51)] = 1900,
  [SMALL_STATE(52)] = 1920,
  [SMALL_STATE(53)] = 1940,
  [SMALL_STATE(54)] = 1975,
  [SMALL_STATE(55)] = 2010,
  [SMALL_STATE(56)] = 2031,
  [SMALL_STATE(57)] = 2052,
  [SMALL_STATE(58)] = 2075,
  [SMALL_STATE(59)] = 2096,
  [SMALL_STATE(60)] = 2113,
  [SMALL_STATE(61)] = 2134,
  [SMALL_STATE(62)] = 2155,
  [SMALL_STATE(63)] = 2178,
  [SMALL_STATE(64)] = 2199,
  [SMALL_STATE(65)] = 2213,
  [SMALL_STATE(66)] = 2227,
  [SMALL_STATE(67)] = 2241,
  [SMALL_STATE(68)] = 2255,
  [SMALL_STATE(69)] = 2267,
  [SMALL_STATE(70)] = 2281,
  [SMALL_STATE(71)] = 2293,
  [SMALL_STATE(72)] = 2308,
  [SMALL_STATE(73)] = 2323,
  [SMALL_STATE(74)] = 2340,
  [SMALL_STATE(75)] = 2357,
  [SMALL_STATE(76)] = 2372,
  [SMALL_STATE(77)] = 2389,
  [SMALL_STATE(78)] = 2406,
  [SMALL_STATE(79)] = 2419,
  [SMALL_STATE(80)] = 2432,
  [SMALL_STATE(81)] = 2449,
  [SMALL_STATE(82)] = 2466,
  [SMALL_STATE(83)] = 2481,
  [SMALL_STATE(84)] = 2496,
  [SMALL_STATE(85)] = 2510,
  [SMALL_STATE(86)] = 2522,
  [SMALL_STATE(87)] = 2536,
  [SMALL_STATE(88)] = 2548,
  [SMALL_STATE(89)] = 2562,
  [SMALL_STATE(90)] = 2574,
  [SMALL_STATE(91)] = 2588,
  [SMALL_STATE(92)] = 2602,
  [SMALL_STATE(93)] = 2612,
  [SMALL_STATE(94)] = 2626,
  [SMALL_STATE(95)] = 2639,
  [SMALL_STATE(96)] = 2650,
  [SMALL_STATE(97)] = 2661,
  [SMALL_STATE(98)] = 2674,
  [SMALL_STATE(99)] = 2683,
  [SMALL_STATE(100)] = 2692,
  [SMALL_STATE(101)] = 2705,
  [SMALL_STATE(102)] = 2718,
  [SMALL_STATE(103)] = 2731,
  [SMALL_STATE(104)] = 2740,
  [SMALL_STATE(105)] = 2750,
  [SMALL_STATE(106)] = 2758,
  [SMALL_STATE(107)] = 2766,
  [SMALL_STATE(108)] = 2776,
  [SMALL_STATE(109)] = 2786,
  [SMALL_STATE(110)] = 2796,
  [SMALL_STATE(111)] = 2803,
  [SMALL_STATE(112)] = 2810,
  [SMALL_STATE(113)] = 2817,
  [SMALL_STATE(114)] = 2824,
  [SMALL_STATE(115)] = 2831,
  [SMALL_STATE(116)] = 2838,
  [SMALL_STATE(117)] = 2845,
  [SMALL_STATE(118)] = 2852,
  [SMALL_STATE(119)] = 2859,
  [SMALL_STATE(120)] = 2866,
  [SMALL_STATE(121)] = 2873,
  [SMALL_STATE(122)] = 2880,
  [SMALL_STATE(123)] = 2887,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = false}}, SHIFT_EXTRA(),
  [5] = {.entry = {.count = 1, .reusable = false}}, SHIFT(117),
  [7] = {.entry = {.count = 1, .reusable = true}}, SHIFT_EXTRA(),
  [9] = {.entry = {.count = 1, .reusable = true}}, SHIFT(97),
  [11] = {.entry = {.count = 1, .reusable = true}}, SHIFT(109),
  [13] = {.entry = {.count = 1, .reusable = false}}, SHIFT(107),
  [15] = {.entry = {.count = 1, .reusable = false}}, SHIFT(102),
  [17] = {.entry = {.count = 1, .reusable = false}}, SHIFT(43),
  [19] = {.entry = {.count = 1, .reusable = false}}, SHIFT(53),
  [21] = {.entry = {.count = 1, .reusable = true}}, SHIFT(2),
  [23] = {.entry = {.count = 1, .reusable = true}}, SHIFT(91),
  [25] = {.entry = {.count = 1, .reusable = false}}, SHIFT(30),
  [27] = {.entry = {.count = 1, .reusable = true}}, SHIFT(30),
  [29] = {.entry = {.count = 1, .reusable = false}}, SHIFT(76),
  [31] = {.entry = {.count = 1, .reusable = false}}, SHIFT(73),
  [33] = {.entry = {.count = 1, .reusable = true}}, SHIFT(63),
  [35] = {.entry = {.count = 1, .reusable = true}}, SHIFT(56),
  [37] = {.entry = {.count = 1, .reusable = false}}, SHIFT(33),
  [39] = {.entry = {.count = 1, .reusable = true}}, SHIFT(41),
  [41] = {.entry = {.count = 1, .reusable = true}}, SHIFT(115),
  [43] = {.entry = {.count = 1, .reusable = true}}, SHIFT(23),
  [45] = {.entry = {.count = 1, .reusable = false}}, SHIFT(120),
  [47] = {.entry = {.count = 1, .reusable = true}}, SHIFT(32),
  [49] = {.entry = {.count = 1, .reusable = true}}, SHIFT(38),
  [51] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(120),
  [54] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(53),
  [57] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(2),
  [60] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0),
  [62] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(91),
  [65] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(30),
  [68] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(30),
  [71] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(76),
  [74] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(73),
  [77] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(63),
  [80] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(56),
  [83] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(33),
  [86] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(38),
  [89] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(115),
  [92] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_object_collection_repeat1, 2, 0, 0), SHIFT_REPEAT(23),
  [95] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_object_collection, 1, 0, 0),
  [97] = {.entry = {.count = 1, .reusable = false}}, SHIFT(122),
  [99] = {.entry = {.count = 1, .reusable = true}}, SHIFT(70),
  [101] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0),
  [103] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(123),
  [106] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(97),
  [109] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(109),
  [112] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(107),
  [115] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(102),
  [118] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(53),
  [121] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(2),
  [124] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(91),
  [127] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(78),
  [130] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(115),
  [133] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_turtle_doc_repeat1, 2, 0, 0), SHIFT_REPEAT(23),
  [136] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_turtle_doc, 1, 0, 0),
  [138] = {.entry = {.count = 1, .reusable = false}}, SHIFT(123),
  [140] = {.entry = {.count = 1, .reusable = true}}, SHIFT(78),
  [142] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_iri_reference, 2, 0, 0),
  [144] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_iri_reference, 2, 0, 0),
  [146] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_iri_reference, 3, 0, 1),
  [148] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_iri_reference, 3, 0, 1),
  [150] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__string_literal_long_single_quote, 2, 0, 0),
  [152] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__string_literal_long_single_quote, 2, 0, 0),
  [154] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__string_literal_single_quote, 2, 0, 0),
  [156] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__string_literal_single_quote, 2, 0, 0),
  [158] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_string, 1, 0, 0),
  [160] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_string, 1, 0, 0),
  [162] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__string_literal_long_quote, 3, 0, 1),
  [164] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__string_literal_long_quote, 3, 0, 1),
  [166] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__string_literal_long_single_quote, 3, 0, 1),
  [168] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__string_literal_long_single_quote, 3, 0, 1),
  [170] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__string_literal_single_quote, 3, 0, 1),
  [172] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__string_literal_single_quote, 3, 0, 1),
  [174] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__string_literal_long_quote, 2, 0, 0),
  [176] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__string_literal_long_quote, 2, 0, 0),
  [178] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__string_literal_quote, 3, 0, 1),
  [180] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__string_literal_quote, 3, 0, 1),
  [182] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__string_literal_quote, 2, 0, 0),
  [184] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__string_literal_quote, 2, 0, 0),
  [186] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_prefixed_name, 2, 0, 0),
  [188] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_prefixed_name, 2, 0, 0),
  [190] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_blank_node, 1, 0, 0),
  [192] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_blank_node, 1, 0, 0),
  [194] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_blank_node_label, 2, 0, 0),
  [196] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_blank_node_label, 2, 0, 0),
  [198] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_iri, 1, 0, 0),
  [200] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_iri, 1, 0, 0),
  [202] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_rdf_literal, 3, 0, 2),
  [204] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_rdf_literal, 3, 0, 2),
  [206] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_rdf_literal, 3, 0, 0),
  [208] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_rdf_literal, 3, 0, 0),
  [210] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_literal, 1, 0, 0),
  [212] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_literal, 1, 0, 0),
  [214] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_collection, 3, 0, 0),
  [216] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_collection, 3, 0, 0),
  [218] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_numeric_literal, 1, 0, 0),
  [220] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_numeric_literal, 1, 0, 0),
  [222] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_blank_node_property_list, 3, 0, 0),
  [224] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_blank_node_property_list, 3, 0, 0),
  [226] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_collection, 2, 0, 0),
  [228] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_collection, 2, 0, 0),
  [230] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_boolean_literal, 1, 0, 0),
  [232] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_boolean_literal, 1, 0, 0),
  [234] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_object, 1, 0, 0),
  [236] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_object, 1, 0, 0),
  [238] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_rdf_literal, 1, 0, 0),
  [240] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_rdf_literal, 1, 0, 0),
  [242] = {.entry = {.count = 1, .reusable = true}}, SHIFT(110),
  [244] = {.entry = {.count = 1, .reusable = true}}, SHIFT(57),
  [246] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_namespace, 2, 0, 0),
  [248] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_namespace, 2, 0, 0),
  [250] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_prefixed_name, 1, 0, 0),
  [252] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_prefixed_name, 1, 0, 0),
  [254] = {.entry = {.count = 1, .reusable = false}}, SHIFT(22),
  [256] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_namespace, 1, 0, 0),
  [258] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_namespace, 1, 0, 0),
  [260] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_term, 1, 0, 0),
  [262] = {.entry = {.count = 2, .reusable = false}}, REDUCE(sym_predicate, 1, 0, 0), REDUCE(sym_subject, 1, 0, 0),
  [265] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_subject, 1, 0, 0),
  [267] = {.entry = {.count = 2, .reusable = true}}, REDUCE(sym_predicate, 1, 0, 0), REDUCE(sym_subject, 1, 0, 0),
  [270] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_predicate, 1, 0, 0),
  [272] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_predicate, 1, 0, 0),
  [274] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_property_list_repeat1, 1, 0, 0),
  [276] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_base, 3, 0, 0),
  [278] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_base, 3, 0, 0),
  [280] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_prefix_id, 4, 0, 0),
  [282] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_prefix_id, 4, 0, 0),
  [284] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_triples, 1, 0, 0),
  [286] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_sparql_base, 2, 0, 0),
  [288] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_sparql_base, 2, 0, 0),
  [290] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_statement, 2, 0, 0),
  [292] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_statement, 2, 0, 0),
  [294] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_sparql_prefix, 3, 0, 0),
  [296] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_sparql_prefix, 3, 0, 0),
  [298] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_statement, 1, 0, 0),
  [300] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_statement, 1, 0, 0),
  [302] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_directive, 1, 0, 0),
  [304] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_directive, 1, 0, 0),
  [306] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__string_literal_long_single_quote_repeat1, 2, 0, 0), SHIFT_REPEAT(65),
  [309] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__string_literal_long_single_quote_repeat1, 2, 0, 0), SHIFT_REPEAT(95),
  [312] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym__string_literal_long_single_quote_repeat1, 2, 0, 0),
  [314] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__string_literal_long_single_quote_repeat1, 2, 0, 0), SHIFT_REPEAT(65),
  [317] = {.entry = {.count = 1, .reusable = false}}, SHIFT(67),
  [319] = {.entry = {.count = 1, .reusable = false}}, SHIFT(96),
  [321] = {.entry = {.count = 1, .reusable = false}}, SHIFT(19),
  [323] = {.entry = {.count = 1, .reusable = true}}, SHIFT(67),
  [325] = {.entry = {.count = 1, .reusable = true}}, SHIFT(120),
  [327] = {.entry = {.count = 1, .reusable = false}}, SHIFT(65),
  [329] = {.entry = {.count = 1, .reusable = false}}, SHIFT(95),
  [331] = {.entry = {.count = 1, .reusable = false}}, SHIFT(17),
  [333] = {.entry = {.count = 1, .reusable = true}}, SHIFT(65),
  [335] = {.entry = {.count = 1, .reusable = true}}, SHIFT(62),
  [337] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__string_literal_long_quote_repeat1, 2, 0, 0), SHIFT_REPEAT(67),
  [340] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__string_literal_long_quote_repeat1, 2, 0, 0), SHIFT_REPEAT(96),
  [343] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym__string_literal_long_quote_repeat1, 2, 0, 0),
  [345] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__string_literal_long_quote_repeat1, 2, 0, 0), SHIFT_REPEAT(67),
  [348] = {.entry = {.count = 1, .reusable = false}}, SHIFT(16),
  [350] = {.entry = {.count = 1, .reusable = true}}, SHIFT(122),
  [352] = {.entry = {.count = 1, .reusable = false}}, SHIFT(13),
  [354] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__string_literal_long_single_quote_repeat1, 2, 0, 0),
  [356] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym__string_literal_long_single_quote_repeat1, 1, 0, 0),
  [358] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__string_literal_long_single_quote_repeat1, 1, 0, 0),
  [360] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__string_literal_long_quote_repeat1, 2, 0, 0),
  [362] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym__string_literal_long_quote_repeat1, 1, 0, 0),
  [364] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__string_literal_long_quote_repeat1, 1, 0, 0),
  [366] = {.entry = {.count = 1, .reusable = true}}, SHIFT(22),
  [368] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_object_list_repeat1, 2, 0, 0), SHIFT_REPEAT(6),
  [371] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_object_list_repeat1, 2, 0, 0),
  [373] = {.entry = {.count = 1, .reusable = false}}, SHIFT(87),
  [375] = {.entry = {.count = 1, .reusable = false}}, SHIFT(14),
  [377] = {.entry = {.count = 1, .reusable = true}}, SHIFT(87),
  [379] = {.entry = {.count = 1, .reusable = false}}, SHIFT(89),
  [381] = {.entry = {.count = 1, .reusable = false}}, SHIFT(20),
  [383] = {.entry = {.count = 1, .reusable = true}}, SHIFT(89),
  [385] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
  [387] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_object_list, 2, 0, 0),
  [389] = {.entry = {.count = 1, .reusable = false}}, SHIFT(21),
  [391] = {.entry = {.count = 1, .reusable = false}}, SHIFT(18),
  [393] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__string_literal_single_quote_repeat1, 2, 0, 0), SHIFT_REPEAT(87),
  [396] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym__string_literal_single_quote_repeat1, 2, 0, 0),
  [398] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__string_literal_single_quote_repeat1, 2, 0, 0), SHIFT_REPEAT(87),
  [401] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__string_literal_quote_repeat1, 2, 0, 0), SHIFT_REPEAT(89),
  [404] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym__string_literal_quote_repeat1, 2, 0, 0),
  [406] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__string_literal_quote_repeat1, 2, 0, 0), SHIFT_REPEAT(89),
  [409] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_object_list, 1, 0, 0),
  [411] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_subject, 1, 0, 0),
  [413] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_property_list, 1, 0, 0),
  [415] = {.entry = {.count = 1, .reusable = true}}, SHIFT(44),
  [417] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_iri_reference_repeat1, 2, 0, 0), SHIFT_REPEAT(99),
  [420] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_iri_reference_repeat1, 2, 0, 0),
  [422] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym__string_literal_single_quote_repeat1, 1, 0, 0),
  [424] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__string_literal_single_quote_repeat1, 1, 0, 0),
  [426] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_property_list, 2, 0, 0),
  [428] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym__string_literal_quote_repeat1, 1, 0, 0),
  [430] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__string_literal_quote_repeat1, 1, 0, 0),
  [432] = {.entry = {.count = 1, .reusable = true}}, SHIFT(99),
  [434] = {.entry = {.count = 1, .reusable = true}}, SHIFT(12),
  [436] = {.entry = {.count = 1, .reusable = true}}, SHIFT(11),
  [438] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_property_list_repeat1, 2, 0, 0),
  [440] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_property_list_repeat1, 2, 0, 0), SHIFT_REPEAT(44),
  [443] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_property_term_repeat1, 2, 0, 0),
  [445] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_property_term_repeat1, 2, 0, 0), SHIFT_REPEAT(8),
  [448] = {.entry = {.count = 1, .reusable = false}}, SHIFT(64),
  [450] = {.entry = {.count = 1, .reusable = true}}, SHIFT(64),
  [452] = {.entry = {.count = 1, .reusable = false}}, SHIFT(66),
  [454] = {.entry = {.count = 1, .reusable = true}}, SHIFT(66),
  [456] = {.entry = {.count = 1, .reusable = true}}, SHIFT(117),
  [458] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_property, 2, 0, 0),
  [460] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_iri_reference_repeat1, 1, 0, 0),
  [462] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_property_term, 2, 0, 0),
  [464] = {.entry = {.count = 1, .reusable = true}}, SHIFT(8),
  [466] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_property_term, 3, 0, 0),
  [468] = {.entry = {.count = 1, .reusable = true}}, SHIFT(27),
  [470] = {.entry = {.count = 1, .reusable = true}}, SHIFT(29),
  [472] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_triples, 2, 0, 0),
  [474] = {.entry = {.count = 1, .reusable = true}}, SHIFT(46),
  [476] = {.entry = {.count = 1, .reusable = true}}, SHIFT(31),
  [478] = {.entry = {.count = 1, .reusable = true}}, SHIFT(24),
  [480] = {.entry = {.count = 1, .reusable = true}}, SHIFT(45),
  [482] = {.entry = {.count = 1, .reusable = true}}, SHIFT(39),
  [484] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [486] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_rdf_diagram, 1, 0, 0),
  [488] = {.entry = {.count = 1, .reusable = true}}, SHIFT(36),
  [490] = {.entry = {.count = 1, .reusable = true}}, SHIFT(49),
  [492] = {.entry = {.count = 1, .reusable = true}}, SHIFT(68),
  [494] = {.entry = {.count = 1, .reusable = true}}, SHIFT(79),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef TREE_SITTER_HIDE_SYMBOLS
#define TS_PUBLIC
#elif defined(_WIN32)
#define TS_PUBLIC __declspec(dllexport)
#else
#define TS_PUBLIC __attribute__((visibility("default")))
#endif

TS_PUBLIC const TSLanguage *tree_sitter_rdf_diagram(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .field_names = ts_field_names,
    .field_map_slices = ts_field_map_slices,
    .field_map_entries = ts_field_map_entries,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .keyword_lex_fn = ts_lex_keywords,
    .keyword_capture_token = sym_pn_prefix,
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
