package tree_sitter_rdf_diagram_test

import (
	"testing"

	tree_sitter "github.com/smacker/go-tree-sitter"
	"github.com/tree-sitter/tree-sitter-rdf_diagram"
)

func TestCanLoadGrammar(t *testing.T) {
	language := tree_sitter.NewLanguage(tree_sitter_rdf_diagram.Language())
	if language == nil {
		t.Errorf("Error loading RdfDiagram grammar")
	}
}
