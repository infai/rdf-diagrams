#ifndef TREE_SITTER_RDF_DIAGRAM_H_
#define TREE_SITTER_RDF_DIAGRAM_H_

typedef struct TSLanguage TSLanguage;

#ifdef __cplusplus
extern "C" {
#endif

const TSLanguage *tree_sitter_rdf_diagram(void);

#ifdef __cplusplus
}
#endif

#endif // TREE_SITTER_RDF_DIAGRAM_H_
